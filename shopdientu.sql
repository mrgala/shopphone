USE [ShopDienTu]
GO
/****** Object:  StoredProcedure [dbo].[Benefit]    Script Date: 12/12/2017 12:49:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Benefit]
@fromDate nvarchar(max),
@toDate nvarchar(max)
as
set dateformat dmy
select NgayDat as Date,sum(ctdh.SoLuong * ctdh.DonGia) as Revenue,sum((ctdh.SoLuong * ctdh.DonGia)-(ctdh.SoLuong * ctpn.DonGiaNhap)) as Benefit
from DonDatHang ddh 
	inner join ChiTietDonDatHang ctdh on ddh.MaDDH = ctdh.MaDDH
	inner join ChiTietPhieuNhap ctpn on ctdh.MaSP = ctpn.MaSP
--where ddh.NgayDat >='20/05/2017' and ddh.NgayDat <='24/05/2017'
--SELECT CONVERT(NVARCHAR, NgayDat, 105)+' '+CONVERT(NVARCHAR, NgayDat, 108) FROM DonDatHang
--where ddh.NgayDat <= CAST(@toDate as date) and ddh.NgayDat >= CAST(@fromDate as date) and ddh.DaThanhToan = 1 and ddh.TinhTrangGiaoHang = 1 
where ddh.NgayDat <= @toDate and ddh.NgayDat >= @fromDate and ddh.DaThanhToan = 1 and ddh.TinhTrangGiaoHang = 1 
group by NgayDat

GO
/****** Object:  Table [dbo].[BinhLuan]    Script Date: 12/12/2017 12:49:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BinhLuan](
	[MaBL] [int] IDENTITY(1,1) NOT NULL,
	[NoiDungBL] [nvarchar](max) NULL,
	[MaThanhVien] [int] NULL,
	[MaSP] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[MaBL] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ChiTietDonDatHang]    Script Date: 12/12/2017 12:49:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChiTietDonDatHang](
	[MaChiTietDDH] [int] IDENTITY(1,1) NOT NULL,
	[MaDDH] [int] NULL,
	[MaSP] [int] NULL,
	[TenSP] [nvarchar](255) NULL,
	[SoLuong] [int] NULL,
	[DonGia] [decimal](18, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[MaChiTietDDH] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ChiTietPhieuNhap]    Script Date: 12/12/2017 12:49:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChiTietPhieuNhap](
	[MaChiTietPN] [int] IDENTITY(1,1) NOT NULL,
	[MaPN] [int] NULL,
	[MaSP] [int] NULL,
	[DonGiaNhap] [decimal](18, 0) NOT NULL,
	[SoLuongNhap] [int] NOT NULL,
 CONSTRAINT [PK__ChiTietP__651D0AF7AB1A1891] PRIMARY KEY CLUSTERED 
(
	[MaChiTietPN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DonDatHang]    Script Date: 12/12/2017 12:49:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DonDatHang](
	[MaDDH] [int] IDENTITY(1,1) NOT NULL,
	[NgayDat] [datetime] NOT NULL,
	[TinhTrangGiaoHang] [bit] NULL,
	[NgayGiao] [datetime] NULL,
	[DaThanhToan] [bit] NULL,
	[MaKH] [int] NULL,
	[UuDai] [int] NULL,
	[DaHuy] [bit] NULL,
	[DaXoa] [bit] NULL,
 CONSTRAINT [PK__DonDatHa__3D88C804AD1EA012] PRIMARY KEY CLUSTERED 
(
	[MaDDH] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[KhachHang]    Script Date: 12/12/2017 12:49:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KhachHang](
	[MaKH] [int] IDENTITY(1,1) NOT NULL,
	[TenKH] [nvarchar](100) NULL,
	[DiaChi] [nvarchar](100) NULL,
	[Email] [nvarchar](255) NULL,
	[SoDienThoai] [nvarchar](255) NULL,
	[MaThanhVien] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[MaKH] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoaiSanPham]    Script Date: 12/12/2017 12:49:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiSanPham](
	[MaLoaiSP] [int] IDENTITY(1,1) NOT NULL,
	[TenLoai] [nvarchar](100) NULL,
	[Icon] [nvarchar](max) NULL,
	[BiDanh] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[MaLoaiSP] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoaiThanhVien]    Script Date: 12/12/2017 12:49:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiThanhVien](
	[MaLoaiThanhVien] [int] IDENTITY(1,1) NOT NULL,
	[TenLoaiThanhVien] [nvarchar](50) NULL,
	[UuDai] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[MaLoaiThanhVien] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoaiThanhVien_Quyen]    Script Date: 12/12/2017 12:49:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiThanhVien_Quyen](
	[MaLoaiTV] [int] NOT NULL,
	[MaQuyen] [nvarchar](50) NOT NULL,
	[GhiChu] [nvarchar](50) NULL,
 CONSTRAINT [PK__LoaiThan__43F045ACF33DABBC] PRIMARY KEY CLUSTERED 
(
	[MaLoaiTV] ASC,
	[MaQuyen] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NhaCungCap]    Script Date: 12/12/2017 12:49:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NhaCungCap](
	[MaNCC] [int] IDENTITY(1,1) NOT NULL,
	[TenNCC] [nvarchar](100) NULL,
	[DiaChi] [nvarchar](255) NULL,
	[Email] [nvarchar](255) NULL,
	[SoDienThoai] [varchar](12) NULL,
	[Fax] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[MaNCC] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NhaSanXuat]    Script Date: 12/12/2017 12:49:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NhaSanXuat](
	[MaNSX] [int] IDENTITY(1,1) NOT NULL,
	[TenNSX] [nvarchar](100) NULL,
	[ThongTin] [nvarchar](255) NULL,
	[Logo] [nvarchar](max) NULL,
 CONSTRAINT [PK_Table] PRIMARY KEY CLUSTERED 
(
	[MaNSX] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PhieuNhap]    Script Date: 12/12/2017 12:49:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PhieuNhap](
	[MaPN] [int] IDENTITY(1,1) NOT NULL,
	[MaNCC] [int] NULL,
	[NgayNhap] [datetime] NULL,
	[DaXoa] [bit] NULL,
	[NgaySua] [datetime] NULL,
	[MaTV] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[MaPN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Quyen]    Script Date: 12/12/2017 12:49:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Quyen](
	[MaQuyen] [nvarchar](50) NOT NULL,
	[TenQuyen] [nvarchar](50) NULL,
 CONSTRAINT [PK__Quyen__1D4B7ED4AAA0CFBD] PRIMARY KEY CLUSTERED 
(
	[MaQuyen] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SanPham]    Script Date: 12/12/2017 12:49:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SanPham](
	[MaSP] [int] IDENTITY(1,1) NOT NULL,
	[TenSP] [nvarchar](255) NULL,
	[DonGia] [decimal](18, 0) NOT NULL,
	[NgayCapNhap] [datetime] NULL,
	[CauHinh] [nvarchar](max) NULL,
	[MoTa] [nvarchar](max) NULL,
	[HinhAnh] [nvarchar](max) NULL,
	[SoLuongTon] [int] NULL,
	[LuotXem] [int] NULL,
	[LuotBinhChon] [int] NULL,
	[LuotBinhLuan] [int] NULL,
	[SoLanMua] [int] NULL,
	[Moi] [bit] NOT NULL,
	[MaNCC] [int] NULL,
	[MaNSX] [int] NULL,
	[MaLoaiSP] [int] NULL,
	[DaXoa] [bit] NOT NULL,
	[HinhAnh1] [xml] NULL,
	[Home] [bit] NOT NULL,
 CONSTRAINT [PK__SanPham__2725081CC5A87939] PRIMARY KEY CLUSTERED 
(
	[MaSP] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Slides]    Script Date: 12/12/2017 12:49:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Slides](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](256) NOT NULL,
	[MoTa] [nvarchar](256) NULL,
	[HinhAnh] [nvarchar](256) NULL,
	[Url] [nvarchar](256) NULL,
	[ThuTu] [int] NULL,
	[DaXoa] [bit] NOT NULL,
 CONSTRAINT [PK_Slides] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ThanhVien]    Script Date: 12/12/2017 12:49:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ThanhVien](
	[MaThanhVien] [int] IDENTITY(1,1) NOT NULL,
	[TaiKhoan] [nvarchar](100) NULL,
	[MatKhau] [nvarchar](100) NULL,
	[HoTen] [nvarchar](100) NULL,
	[DiaChi] [nvarchar](255) NULL,
	[Email] [nvarchar](255) NULL,
	[SoDienThoai] [varchar](12) NULL,
	[CauHoi] [nvarchar](max) NULL,
	[CauTraLoi] [nvarchar](max) NULL,
	[MaLoaiTV] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[MaThanhVien] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[ChiTietDonDatHang] ON 

INSERT [dbo].[ChiTietDonDatHang] ([MaChiTietDDH], [MaDDH], [MaSP], [TenSP], [SoLuong], [DonGia]) VALUES (2, 2, 13, N'Samsung Galaxy A5 2017', 1, CAST(8500000 AS Decimal(18, 0)))
INSERT [dbo].[ChiTietDonDatHang] ([MaChiTietDDH], [MaDDH], [MaSP], [TenSP], [SoLuong], [DonGia]) VALUES (3, 2, 21, N'iPad Mini 4 wifi (32GB)', 1, CAST(9700000 AS Decimal(18, 0)))
INSERT [dbo].[ChiTietDonDatHang] ([MaChiTietDDH], [MaDDH], [MaSP], [TenSP], [SoLuong], [DonGia]) VALUES (4, 2, 17, N'Sony Xperia XZs', 1, CAST(13799000 AS Decimal(18, 0)))
INSERT [dbo].[ChiTietDonDatHang] ([MaChiTietDDH], [MaDDH], [MaSP], [TenSP], [SoLuong], [DonGia]) VALUES (5, 3, 13, N'Samsung Galaxy A5 2017', 1, CAST(8500000 AS Decimal(18, 0)))
INSERT [dbo].[ChiTietDonDatHang] ([MaChiTietDDH], [MaDDH], [MaSP], [TenSP], [SoLuong], [DonGia]) VALUES (6, 3, 14, N'Samsung Galaxy A7 2017', 1, CAST(10500000 AS Decimal(18, 0)))
INSERT [dbo].[ChiTietDonDatHang] ([MaChiTietDDH], [MaDDH], [MaSP], [TenSP], [SoLuong], [DonGia]) VALUES (7, 4, 22, N'iPad Mini 4 wifi+4G (32GB)', 2, CAST(11899000 AS Decimal(18, 0)))
INSERT [dbo].[ChiTietDonDatHang] ([MaChiTietDDH], [MaDDH], [MaSP], [TenSP], [SoLuong], [DonGia]) VALUES (8, 4, 17, N'Sony Xperia XZs', 1, CAST(13799000 AS Decimal(18, 0)))
INSERT [dbo].[ChiTietDonDatHang] ([MaChiTietDDH], [MaDDH], [MaSP], [TenSP], [SoLuong], [DonGia]) VALUES (9, 5, 14, N'Samsung Galaxy A7 2017', 2, CAST(10500000 AS Decimal(18, 0)))
INSERT [dbo].[ChiTietDonDatHang] ([MaChiTietDDH], [MaDDH], [MaSP], [TenSP], [SoLuong], [DonGia]) VALUES (10, 5, 17, N'Sony Xperia XZs', 1, CAST(13799000 AS Decimal(18, 0)))
INSERT [dbo].[ChiTietDonDatHang] ([MaChiTietDDH], [MaDDH], [MaSP], [TenSP], [SoLuong], [DonGia]) VALUES (11, 6, 16, N'Samsung Galaxy S8 Plus', 1, CAST(20490000 AS Decimal(18, 0)))
SET IDENTITY_INSERT [dbo].[ChiTietDonDatHang] OFF
SET IDENTITY_INSERT [dbo].[ChiTietPhieuNhap] ON 

INSERT [dbo].[ChiTietPhieuNhap] ([MaChiTietPN], [MaPN], [MaSP], [DonGiaNhap], [SoLuongNhap]) VALUES (49, 40, 13, CAST(5000000 AS Decimal(18, 0)), 15)
INSERT [dbo].[ChiTietPhieuNhap] ([MaChiTietPN], [MaPN], [MaSP], [DonGiaNhap], [SoLuongNhap]) VALUES (50, 40, 17, CAST(10000000 AS Decimal(18, 0)), 15)
INSERT [dbo].[ChiTietPhieuNhap] ([MaChiTietPN], [MaPN], [MaSP], [DonGiaNhap], [SoLuongNhap]) VALUES (51, 1040, 21, CAST(8500000 AS Decimal(18, 0)), 10)
INSERT [dbo].[ChiTietPhieuNhap] ([MaChiTietPN], [MaPN], [MaSP], [DonGiaNhap], [SoLuongNhap]) VALUES (52, 1040, 22, CAST(10000000 AS Decimal(18, 0)), 10)
INSERT [dbo].[ChiTietPhieuNhap] ([MaChiTietPN], [MaPN], [MaSP], [DonGiaNhap], [SoLuongNhap]) VALUES (53, 1040, 14, CAST(9000000 AS Decimal(18, 0)), 10)
INSERT [dbo].[ChiTietPhieuNhap] ([MaChiTietPN], [MaPN], [MaSP], [DonGiaNhap], [SoLuongNhap]) VALUES (54, 1041, 12, CAST(13000000 AS Decimal(18, 0)), 5)
INSERT [dbo].[ChiTietPhieuNhap] ([MaChiTietPN], [MaPN], [MaSP], [DonGiaNhap], [SoLuongNhap]) VALUES (55, 1041, 16, CAST(18000000 AS Decimal(18, 0)), 5)
INSERT [dbo].[ChiTietPhieuNhap] ([MaChiTietPN], [MaPN], [MaSP], [DonGiaNhap], [SoLuongNhap]) VALUES (56, 1042, 9, CAST(3000000 AS Decimal(18, 0)), 5)
INSERT [dbo].[ChiTietPhieuNhap] ([MaChiTietPN], [MaPN], [MaSP], [DonGiaNhap], [SoLuongNhap]) VALUES (57, 1042, 10, CAST(4000000 AS Decimal(18, 0)), 5)
INSERT [dbo].[ChiTietPhieuNhap] ([MaChiTietPN], [MaPN], [MaSP], [DonGiaNhap], [SoLuongNhap]) VALUES (58, 1043, 20, CAST(5000000 AS Decimal(18, 0)), 6)
INSERT [dbo].[ChiTietPhieuNhap] ([MaChiTietPN], [MaPN], [MaSP], [DonGiaNhap], [SoLuongNhap]) VALUES (59, 1043, 19, CAST(6000000 AS Decimal(18, 0)), 5)
INSERT [dbo].[ChiTietPhieuNhap] ([MaChiTietPN], [MaPN], [MaSP], [DonGiaNhap], [SoLuongNhap]) VALUES (60, 1044, 11, CAST(12000000 AS Decimal(18, 0)), 10)
SET IDENTITY_INSERT [dbo].[ChiTietPhieuNhap] OFF
SET IDENTITY_INSERT [dbo].[DonDatHang] ON 

INSERT [dbo].[DonDatHang] ([MaDDH], [NgayDat], [TinhTrangGiaoHang], [NgayGiao], [DaThanhToan], [MaKH], [UuDai], [DaHuy], [DaXoa]) VALUES (2, CAST(0x0000A77A0129EB0C AS DateTime), 1, NULL, 1, 2, 0, 0, 0)
INSERT [dbo].[DonDatHang] ([MaDDH], [NgayDat], [TinhTrangGiaoHang], [NgayGiao], [DaThanhToan], [MaKH], [UuDai], [DaHuy], [DaXoa]) VALUES (3, CAST(0x0000A77C015E778F AS DateTime), 1, NULL, 1, 3, 0, 0, 0)
INSERT [dbo].[DonDatHang] ([MaDDH], [NgayDat], [TinhTrangGiaoHang], [NgayGiao], [DaThanhToan], [MaKH], [UuDai], [DaHuy], [DaXoa]) VALUES (4, CAST(0x0000A77D0166154F AS DateTime), 1, NULL, 1, 4, 0, 0, 0)
INSERT [dbo].[DonDatHang] ([MaDDH], [NgayDat], [TinhTrangGiaoHang], [NgayGiao], [DaThanhToan], [MaKH], [UuDai], [DaHuy], [DaXoa]) VALUES (5, CAST(0x0000A79400304B7B AS DateTime), 1, NULL, 1, 5, 0, 0, 0)
INSERT [dbo].[DonDatHang] ([MaDDH], [NgayDat], [TinhTrangGiaoHang], [NgayGiao], [DaThanhToan], [MaKH], [UuDai], [DaHuy], [DaXoa]) VALUES (6, CAST(0x0000A799010FA736 AS DateTime), 1, NULL, 1, 6, 0, 0, 0)
SET IDENTITY_INSERT [dbo].[DonDatHang] OFF
SET IDENTITY_INSERT [dbo].[KhachHang] ON 

INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [DiaChi], [Email], [SoDienThoai], [MaThanhVien]) VALUES (2, N'Đỗ Xuân Thành', N'15/1b', N'tabimtop@gmail.com', N'01656606281', NULL)
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [DiaChi], [Email], [SoDienThoai], [MaThanhVien]) VALUES (3, N'Nguyễn Võ Thanh Bình', N'16/2 Long Binh Tân', N'binh@lhu.com.vn', N'0909465745', NULL)
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [DiaChi], [Email], [SoDienThoai], [MaThanhVien]) VALUES (4, N'Búng Búng', N'15/1b', N'tabimtop@gmail.com', N'01656606281', NULL)
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [DiaChi], [Email], [SoDienThoai], [MaThanhVien]) VALUES (5, N'Minh Tiên', N'15/1b', N'doxuanthanhcntt@gmail.com', N'01656606281', 10)
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [DiaChi], [Email], [SoDienThoai], [MaThanhVien]) VALUES (6, N'Thanh Quan', N'15/1b', N'thanhquan@gmail.com', N'01656606281', 1011)
SET IDENTITY_INSERT [dbo].[KhachHang] OFF
SET IDENTITY_INSERT [dbo].[LoaiSanPham] ON 

INSERT [dbo].[LoaiSanPham] ([MaLoaiSP], [TenLoai], [Icon], [BiDanh]) VALUES (1, N'Điện thoại', NULL, N'dien-thoai')
INSERT [dbo].[LoaiSanPham] ([MaLoaiSP], [TenLoai], [Icon], [BiDanh]) VALUES (2, N'Laptop', NULL, N'lap-top')
INSERT [dbo].[LoaiSanPham] ([MaLoaiSP], [TenLoai], [Icon], [BiDanh]) VALUES (3, N'Máy tính bảng', NULL, N'may-tinh-bang')
INSERT [dbo].[LoaiSanPham] ([MaLoaiSP], [TenLoai], [Icon], [BiDanh]) VALUES (13, N'Phụ kiện', NULL, N'phu-kien')
SET IDENTITY_INSERT [dbo].[LoaiSanPham] OFF
SET IDENTITY_INSERT [dbo].[LoaiThanhVien] ON 

INSERT [dbo].[LoaiThanhVien] ([MaLoaiThanhVien], [TenLoaiThanhVien], [UuDai]) VALUES (1, N'Super Admin', 0)
INSERT [dbo].[LoaiThanhVien] ([MaLoaiThanhVien], [TenLoaiThanhVien], [UuDai]) VALUES (2, N'Admin San Pham', 0)
INSERT [dbo].[LoaiThanhVien] ([MaLoaiThanhVien], [TenLoaiThanhVien], [UuDai]) VALUES (3, N'Admin Nhap Hang', 0)
INSERT [dbo].[LoaiThanhVien] ([MaLoaiThanhVien], [TenLoaiThanhVien], [UuDai]) VALUES (4, N'Admin Don Hang', 0)
INSERT [dbo].[LoaiThanhVien] ([MaLoaiThanhVien], [TenLoaiThanhVien], [UuDai]) VALUES (5, N'Admin Khach Hang', 0)
INSERT [dbo].[LoaiThanhVien] ([MaLoaiThanhVien], [TenLoaiThanhVien], [UuDai]) VALUES (6, N'Vip', 15)
INSERT [dbo].[LoaiThanhVien] ([MaLoaiThanhVien], [TenLoaiThanhVien], [UuDai]) VALUES (7, N'Member', 0)
SET IDENTITY_INSERT [dbo].[LoaiThanhVien] OFF
INSERT [dbo].[LoaiThanhVien_Quyen] ([MaLoaiTV], [MaQuyen], [GhiChu]) VALUES (1, N'QuanLySanPham', NULL)
INSERT [dbo].[LoaiThanhVien_Quyen] ([MaLoaiTV], [MaQuyen], [GhiChu]) VALUES (1, N'QuanTri', NULL)
INSERT [dbo].[LoaiThanhVien_Quyen] ([MaLoaiTV], [MaQuyen], [GhiChu]) VALUES (2, N'QuanLySanPham', NULL)
INSERT [dbo].[LoaiThanhVien_Quyen] ([MaLoaiTV], [MaQuyen], [GhiChu]) VALUES (3, N'QuanLyNhapHang', NULL)
INSERT [dbo].[LoaiThanhVien_Quyen] ([MaLoaiTV], [MaQuyen], [GhiChu]) VALUES (4, N'QuanLyDonHang', NULL)
INSERT [dbo].[LoaiThanhVien_Quyen] ([MaLoaiTV], [MaQuyen], [GhiChu]) VALUES (4, N'QuanLyNhapHang', NULL)
INSERT [dbo].[LoaiThanhVien_Quyen] ([MaLoaiTV], [MaQuyen], [GhiChu]) VALUES (4, N'QuanLySanPham', NULL)
INSERT [dbo].[LoaiThanhVien_Quyen] ([MaLoaiTV], [MaQuyen], [GhiChu]) VALUES (5, N'QuanLyThanhVien', NULL)
SET IDENTITY_INSERT [dbo].[NhaCungCap] ON 

INSERT [dbo].[NhaCungCap] ([MaNCC], [TenNCC], [DiaChi], [Email], [SoDienThoai], [Fax]) VALUES (1, N'IMobi Biên Hòa', N'15/1 phường Quang Vinh, Biên Hòa, Đồng Nai', N'imobi@gmail.com', N'0909465745', NULL)
INSERT [dbo].[NhaCungCap] ([MaNCC], [TenNCC], [DiaChi], [Email], [SoDienThoai], [Fax]) VALUES (2, N'Mai Phương', N'13/3 phường Trung Dũng, Biên Hòa, Đồng Nai', N'maiphuong@gmail.com', N'0909465745', NULL)
SET IDENTITY_INSERT [dbo].[NhaCungCap] OFF
SET IDENTITY_INSERT [dbo].[NhaSanXuat] ON 

INSERT [dbo].[NhaSanXuat] ([MaNSX], [TenNSX], [ThongTin], [Logo]) VALUES (1, N'Nokia', NULL, NULL)
INSERT [dbo].[NhaSanXuat] ([MaNSX], [TenNSX], [ThongTin], [Logo]) VALUES (2, N'Apple', NULL, NULL)
INSERT [dbo].[NhaSanXuat] ([MaNSX], [TenNSX], [ThongTin], [Logo]) VALUES (3, N'Sony', NULL, NULL)
INSERT [dbo].[NhaSanXuat] ([MaNSX], [TenNSX], [ThongTin], [Logo]) VALUES (4, N'SamSung', NULL, NULL)
INSERT [dbo].[NhaSanXuat] ([MaNSX], [TenNSX], [ThongTin], [Logo]) VALUES (5, N'Xiaomi', NULL, NULL)
SET IDENTITY_INSERT [dbo].[NhaSanXuat] OFF
SET IDENTITY_INSERT [dbo].[PhieuNhap] ON 

INSERT [dbo].[PhieuNhap] ([MaPN], [MaNCC], [NgayNhap], [DaXoa], [NgaySua], [MaTV]) VALUES (40, 1, CAST(0x0000A78E00E274A5 AS DateTime), 0, CAST(0x0000A78E00E9B3C5 AS DateTime), 1)
INSERT [dbo].[PhieuNhap] ([MaPN], [MaNCC], [NgayNhap], [DaXoa], [NgaySua], [MaTV]) VALUES (1040, 1, CAST(0x0000A7930119ECEA AS DateTime), 0, NULL, 1)
INSERT [dbo].[PhieuNhap] ([MaPN], [MaNCC], [NgayNhap], [DaXoa], [NgaySua], [MaTV]) VALUES (1041, 1, CAST(0x0000A79400256C4D AS DateTime), 0, NULL, 1)
INSERT [dbo].[PhieuNhap] ([MaPN], [MaNCC], [NgayNhap], [DaXoa], [NgaySua], [MaTV]) VALUES (1042, 1, CAST(0x0000A7940025C350 AS DateTime), 0, NULL, 1)
INSERT [dbo].[PhieuNhap] ([MaPN], [MaNCC], [NgayNhap], [DaXoa], [NgaySua], [MaTV]) VALUES (1043, 1, CAST(0x0000A79900F1CD48 AS DateTime), 0, CAST(0x0000A79900F9CD09 AS DateTime), 1)
INSERT [dbo].[PhieuNhap] ([MaPN], [MaNCC], [NgayNhap], [DaXoa], [NgaySua], [MaTV]) VALUES (1044, 1, CAST(0x0000A7D200AF420B AS DateTime), 0, NULL, 1)
SET IDENTITY_INSERT [dbo].[PhieuNhap] OFF
INSERT [dbo].[Quyen] ([MaQuyen], [TenQuyen]) VALUES (N'QuanLyDonHang', N'Quản lý đơn hàng')
INSERT [dbo].[Quyen] ([MaQuyen], [TenQuyen]) VALUES (N'QuanLyNhapHang', N'Quản lý nhập hàng')
INSERT [dbo].[Quyen] ([MaQuyen], [TenQuyen]) VALUES (N'QuanLySanPham', N'Quản lý sản phẩm')
INSERT [dbo].[Quyen] ([MaQuyen], [TenQuyen]) VALUES (N'QuanLyThanhVien', N'Quản lý thành viên')
INSERT [dbo].[Quyen] ([MaQuyen], [TenQuyen]) VALUES (N'QuanTri', N'Quản trị')
SET IDENTITY_INSERT [dbo].[SanPham] ON 

INSERT [dbo].[SanPham] ([MaSP], [TenSP], [DonGia], [NgayCapNhap], [CauHinh], [MoTa], [HinhAnh], [SoLuongTon], [LuotXem], [LuotBinhChon], [LuotBinhLuan], [SoLanMua], [Moi], [MaNCC], [MaNSX], [MaLoaiSP], [DaXoa], [HinhAnh1], [Home]) VALUES (9, N'Samsung Galaxy J5 Prime', CAST(4500000 AS Decimal(18, 0)), CAST(0x0000A767012412E9 AS DateTime), N'<div class="detail-col-left53 left">
<h3>Th&ocirc;ng số kỹ thuật</h3>

<ul>
	<li>Th&ocirc;ng số cơ bản</li>
	<li>M&agrave;n h&igrave;nh : 5.0 inch 720 x 1280 Pixels</li>
	<li>Camera : Ch&iacute;nh: 13.0 MP, Phụ: 5.0 MP</li>
	<li>RAM : 2 GB</li>
	<li>Bộ nhớ trong : 16 GB</li>
	<li>Hệ điều h&agrave;nh : Android 6.0 (Marshmallow)</li>
	<li>Chipset : Exynos 7570 4 nh&acirc;n 64-bit</li>
	<li>CPU : Quad-Core 1.4GHz</li>
	<li>GPU : Mali T720</li>
	<li>K&iacute;ch thước : 142.8 x 69.5 x 8.1 mm</li>
</ul>

<ul>
	<li>M&agrave;n h&igrave;nh</li>
	<li>C&ocirc;ng nghệ m&agrave;n h&igrave;nh : PLS</li>
	<li>Chuẩn m&agrave;n h&igrave;nh : HD</li>
	<li>M&agrave;u m&agrave;n h&igrave;nh : 16 Triệu m&agrave;u</li>
	<li>Độ ph&acirc;n giải m&agrave;n h&igrave;nh : 720 x 1280 pixels</li>
	<li>C&ocirc;ng nghệ cảm ứng : Cảm ứng điện dung, đa điểm</li>
	<li>Mặt k&iacute;nh m&agrave;n h&igrave;nh : K&iacute;nh cường lực</li>
	<li>Camera Sau</li>
	<li>Độ ph&acirc;n giải : 13.0 MP</li>
	<li>Quay phim : Full HD 1080p@30fps</li>
	<li>Đ&egrave;n Flash : C&oacute;</li>
	<li>Chụp ảnh n&acirc;ng cao : Tự động lấy n&eacute;t, Chạm lấy n&eacute;t, Nhận diện khu&ocirc;n mặt, HDR, Panorama, Chế độ chụp chuy&ecirc;n nghiệp</li>
	<li>Camera Trước</li>
	<li>Video Call : C&oacute;</li>
	<li>Độ ph&acirc;n giải : 5.0 MP</li>
	<li>Th&ocirc;ng tin kh&aacute;c : Chế độ l&agrave;m đẹp, Quay video Full HD, Camera g&oacute;c rộng</li>
	<li>Cấu h&igrave;nh phần cứng</li>
	<li>Tốc độ CPU : 1.4 GHz</li>
	<li>Số nh&acirc;n : 4 nh&acirc;n</li>
	<li>Chipset : Exynos 7570</li>
	<li>RAM : 2GB</li>
	<li>Chip đồ họa (GPU) : Mali T720</li>
	<li>Cảm biến : V&acirc;n tay,gia tốc,khoảng c&aacute;ch</li>
	<li>Bộ nhớ &amp; Lưu trữ</li>
	<li>Danh bạ lưu trữ : Kh&ocirc;ng giới hạn</li>
	<li>Bộ nhớ trong : 16 GB</li>
	<li>Bộ nhớ c&ograve;n lại : ~10 GB</li>
	<li>Thẻ nhớ ngo&agrave;i : Micro SD</li>
	<li>Hỗ trợ thẻ nhớ tối đa : 256 GB</li>
	<li>Thiết kế &amp; Trọng lượng</li>
	<li>Kiểu d&aacute;ng : Thanh (thẳng) + Cảm ứng</li>
	<li>Chất liệu : Kim loại nguy&ecirc;n khối(mặt k&iacute;nh cong 2,5D)</li>
	<li>K&iacute;ch thước : 142.8 x 69.5 x 8.1 mm</li>
	<li>Trọng lượng : 143 g</li>
	<li>Khả năng chống nước : Kh&ocirc;ng</li>
	<li>Th&ocirc;ng tin pin</li>
	<li>Loại pin : Li-Ion</li>
	<li>Dung lượng pin : 2400 mAh</li>
	<li>Pin c&oacute; thể th&aacute;o rời : Kh&ocirc;ng</li>
	<li>Thời gian đ&agrave;m thoại : 10 giờ</li>
	<li>Chế độ sạc nhanh : Kh&ocirc;ng</li>
	<li>Kết nối &amp; Cổng giao tiếp</li>
	<li>Băng tần 2G : GSM850, GSM900, DCS1800, PCS1900</li>
	<li>Băng tần 3G : B1(2100), B2(1900), B5(850), B8(900</li>
	<li>Băng tần 4G : C&oacute;</li>
	<li>Hỗ trợ SIM : Nano Sim</li>
	<li>Khe cắm sim : 2 Khe</li>
	<li>Wifi : Wi-Fi 802.11 b/g/n, Wi-Fi Direct, Wi-Fi hotspot</li>
	<li>GPS : GPS, Glonass, Beidou</li>
	<li>Bluetooth : Bluetooth v4.2</li>
	<li>GPRS/EDGE : C&oacute;</li>
	<li>NFC : Kh&ocirc;ng</li>
	<li>Kết nối USB : Micro USB</li>
	<li>Cổng kết nối kh&aacute;c : Hỗ trợ OTG</li>
	<li>Cổng sạc : Micro USB</li>
	<li>Jack (Input &amp; Output) : 3.5 mm</li>
	<li>Giải tr&iacute; &amp; Ứng dụng</li>
	<li>Xem phim : MP4, M4V, 3GP, 3G2, WMV, ASF, AVI, FLV, MKV, WEBM</li>
	<li>Nghe nhạc : MP3, M4A, 3GA, AAC, OGG, OGA, WAV, WMA, AMR, AWB, FLAC, MID, MIDI, XMF, MXMF, IMY, RTTTL, RTX, OTA</li>
	<li>Ghi &acirc;m : C&oacute;</li>
	<li>FM radio : Kh&ocirc;ng</li>
	<li>Chức năng kh&aacute;c : - Google Search, Maps, Gmail - YouTube, Google Talk - T&iacute;ch hợp mạng x&atilde; hội - Ghi &acirc;m bằng giọng n&oacute;i - Xem văn bản - Nhập liệu đo&aacute;n trước từ</li>
	<li>Bảo h&agrave;nh</li>
	<li>Thời gian bảo h&agrave;nh : 12 Th&aacute;ng</li>
</ul>
</div>
', N'<h2><strong><a href="https://www.thegioididong.com/dtdd-samsung" target="_blank" title="Điện thoại hãng Samsung">Samsung</a> Galaxy J5 Prime l&agrave; phi&ecirc;n bản r&uacute;t gọn của J7 Prime với k&iacute;ch thước nhỏ hơn nhưng vẫn đầy sang trọng v&agrave; quyến rũ. Đi k&egrave;m với đ&oacute; l&agrave; c&ocirc;ng nghệ bảo mật v&acirc;n tay cao cấp c&ugrave;ng camera chất lượng tốt hứa hẹn sẽ đem đến cho người d&ugrave;ng nhiều trải nghiệm th&uacute; vị.</strong></h2>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/86689/samsung-galaxy-j5-prime-up-den-12.jpg" onclick="return false;"><img alt="Thiết kế sang trọng" class="lazy" src="https://cdn1.tgdd.vn/Products/Images/42/86689/samsung-galaxy-j5-prime-up-den-12.jpg" style="display:block" title="Thiết kế sang trọng" /></a><br />
<em>Thiết nhỏ gọn dễ cầm nắm</em></p>

<h3><strong>Trang bị cảm biến v&acirc;n tay 1 chạm</strong></h3>

<p>L&agrave; một trong số &iacute;t điện thoại của <a href="https://www.thegioididong.com/dtdd-samsung" target="_blank" title="Điện thoại hãng Samsung">h&atilde;ng Samsung</a> c&oacute; gi&aacute; tầm trung nhưng được t&iacute;ch hợp cảm biến v&acirc;n tay.</p>

<p>T&iacute;nh năng n&agrave;y gi&uacute;p mở kh&oacute;a m&agrave;n h&igrave;nh nhanh chỉ với 1 chạm (kh&ocirc;ng cần mở s&aacute;ng m&agrave;n h&igrave;nh) v&agrave; n&acirc;ng cao bảo mật.</p>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/86689/samsung-galaxy-j5-prime-up-den-13.jpg" onclick="return false;"><img alt="Cảm biến vân tay nhạy" class="lazy" src="https://cdn3.tgdd.vn/Products/Images/42/86689/samsung-galaxy-j5-prime-up-den-13.jpg" style="display:block" title="Cảm biến vân tay nhạy" /></a></p>

<h3><strong>T&iacute;nh năng tiết kiệm pin mới S Power Planning</strong></h3>

<p>T&ugrave;y theo nhu cầu sử dụng pin trong khoảng thời gian n&agrave;o đ&oacute; của bạn để từ đ&oacute; chọn ra 1 trong 3 chế độ dưới đ&acirc;y:</p>

<p>- Th&ocirc;ng b&aacute;o cuộc gọi nhỡ ngay cả khi điện thoại đ&atilde; tắt (với những số điện thoại được ưu ti&ecirc;n lựa chọn).</p>

<p>- Tăng số giờ sử dụng thiết bị l&ecirc;n với 3 cấp kh&aacute;c nhau (cấp c&agrave;ng cao, thời gian c&agrave;ng nhiều).</p>

<p>- Chỉ thực hiện cuộc gọi v&agrave; nhắn tin tr&ecirc;n m&aacute;y (tắt hết ứng dụng chạy ngầm v&agrave; c&aacute;c ứng dụng kh&aacute;c).</p>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/86689/samsung-galaxy-j5-prime-9.jpg" onclick="return false;"><img alt="Samsung Galaxy J5 Prime - Tính năng tiết kiệm pin mới S Power Planning" class="lazy" src="https://cdn.tgdd.vn/Products/Images/42/86689/samsung-galaxy-j5-prime-9.jpg" style="display:block" title="Samsung Galaxy J5 Prime - Tính năng tiết kiệm pin mới S Power Planning" /></a></p>

<h3><strong>Cấu h&igrave;nh kh&aacute; tốt trong tầm gi&aacute;</strong></h3>

<p>M&aacute;y t&iacute;ch hợp chip xử l&yacute; 4 nh&acirc;n, RAM 2 GB, với điểm s&aacute;ng l&agrave; hệ điều h&agrave;nh mới&nbsp;<a href="https://www.thegioididong.com/hoi-dap/android-6-marshmallow-co-gi-hot-726146" target="_blank" title="Tìm hiểu về Android 6">Android 6</a>&nbsp;đem đến trải nghiệm tốt hơn khi duyệt web, &ldquo;lướt&rdquo; Facebook.</p>

<p>Bộ nhớ trong 16 GB v&agrave; bạn c&oacute; thể gắn th&ecirc;m thẻ nhớ ngo&agrave;i l&ecirc;n đến 256 GB.</p>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/86689/samsung-galaxy-j5-prime-10.jpg" onclick="return false;"><img alt="Samsung Galaxy J5 Prime - Cấu hình khá tốt trong tầm giá" class="lazy" src="https://cdn2.tgdd.vn/Products/Images/42/86689/samsung-galaxy-j5-prime-10.jpg" style="display:block" title="Samsung Galaxy J5 Prime - Cấu hình khá tốt trong tầm giá" /></a></p>

<h3><strong>Thiết kế nguy&ecirc;n khối kim loại</strong></h3>

<p>L&agrave; một phi&ecirc;n bản thu nhỏ của&nbsp;<a href="https://www.thegioididong.com/dtdd/samsung-galaxy-j7-prime" target="_blank" title="Điện thoại Samsung Galaxy J7 Prime">Galaxy J7 Prime</a>&nbsp;n&ecirc;n J5 Prime thừa hưởng đầy đủ những n&eacute;t đẹp từ đ&agrave;n anh của m&igrave;nh với thiết kế nguy&ecirc;n khối kim loại.</p>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/86689/samsung-galaxy-j5-prime-3.jpg" onclick="return false;"><img alt="Samsung Galaxy J5 Prime - Thiết kế nguyên khối kim loại" class="lazy" src="https://cdn4.tgdd.vn/Products/Images/42/86689/samsung-galaxy-j5-prime-3.jpg" style="display:block" title="Samsung Galaxy J5 Prime - Thiết kế nguyên khối kim loại" /></a></p>

<p style="text-align:center"><em>Thiết kế nguy&ecirc;n khối bo cong ở c&aacute;c cạnh gi&uacute;p cầm nắm thoải m&aacute;i</em></p>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/86689/samsung-galaxy-j5-prime-5.jpg" onclick="return false;"><img alt="Samsung Galaxy J5 Prime - Với 3 khe gắn ở cạnh máy cho bạn sử dụng được cùng lúc 2 sim và 1 thẻ nhớ" class="lazy" src="https://cdn1.tgdd.vn/Products/Images/42/86689/samsung-galaxy-j5-prime-5.jpg" style="display:block" title="Samsung Galaxy J5 Prime - Với 3 khe gắn ở cạnh máy cho bạn sử dụng được cùng lúc 2 sim và 1 thẻ nhớ" /></a></p>

<p style="text-align:center"><em>Với 3 khe gắn ở cạnh m&aacute;y cho bạn sử dụng được c&ugrave;ng l&uacute;c 2 sim v&agrave; 1 thẻ nhớ</em></p>

<p>Ngo&agrave;i ra, m&aacute;y cũng c&oacute; kết nối 4G đang rất hot hiện nay cho bạn truy cập internet với tốc độ rất nhanh.</p>

<h3><strong>M&agrave;n h&igrave;nh rộng 5 inch</strong></h3>

<p>Điểm cộng m&agrave;n h&igrave;nh của J5 Prime đ&oacute; ch&iacute;nh l&agrave; h&igrave;nh ảnh hiển thị n&eacute;t hơn nhờ độ ph&acirc;n giải HD.</p>

<p>Tấm nền TFT gi&uacute;p t&aacute;i tạo m&agrave;u sắc kh&aacute; tốt, tuy nhi&ecirc;n g&oacute;c nh&igrave;n nghi&ecirc;ng bị hạn chế đ&ocirc;i ch&uacute;t nhưng kh&ocirc;ng phải l&agrave; một vấn đề lớn.</p>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/86689/samsung-galaxy-j5-prime-1-1.jpg" onclick="return false;"><img alt="Samsung Galaxy J5 Prime - Màn hình rộng 5 inch" class="lazy" src="https://cdn3.tgdd.vn/Products/Images/42/86689/samsung-galaxy-j5-prime-1-1.jpg" style="display:block" title="Samsung Galaxy J5 Prime - Màn hình rộng 5 inch" /></a></p>

<h3><strong>Cạnh&nbsp;<a href="https://www.thegioididong.com/hoi-dap/man-hinh-cong-25d-chuan-muc-moi-cho-smartphone-837574" target="_blank" title="Tìm hiểu viền màn hình 2.5D">viền m&agrave;n h&igrave;nh 2.5D</a>&nbsp;tạo sự liền mạch cho m&aacute;y</strong></h3>

<p>Ngo&agrave;i ra, mặt k&iacute;nh cũng được gia c&ocirc;ng từ k&iacute;nh cường lực Gorilla 4 gi&uacute;p hạn chế trầy xước v&agrave; nứt vỡ trong qu&aacute; tr&igrave;nh sử dụng.</p>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/86689/samsung-galaxy-j5-prime-7.jpg" onclick="return false;"><img alt="Samsung Galaxy J5 Prime - Cạnh viền màn hình 2.5D tạo sự liền mạch cho máy" class="lazy" src="https://cdn.tgdd.vn/Products/Images/42/86689/samsung-galaxy-j5-prime-7.jpg" style="display:block" title="Samsung Galaxy J5 Prime - Cạnh viền màn hình 2.5D tạo sự liền mạch cho máy" /></a></p>

<div class="boxRtAtc">
<div class="likewied">
<div class="fb-like fb_iframe_widget">&nbsp;</div>
</div>
</div>
', N'/UploadFile/images/SamSung/SonySamsungJ5PrimeB7_lon.png', 6, 0, NULL, NULL, NULL, 1, 1, 4, 1, 0, N'["/UploadFile/images/SamSung/SonySamsungJ5PrimeB2_lon.png","/UploadFile/images/SamSung/SonySamsungJ5PrimeB3_lon.png","/UploadFile/images/SamSung/SonySamsungJ5PrimeB4_lon.png","/UploadFile/images/SamSung/SonySamsungJ5PrimeB5_lon.png","/UploadFile/images/SamSung/SonySamsungJ5PrimeB6_lon.png","/UploadFile/images/SamSung/SonySamsungJ5PrimeB_lon.png","/UploadFile/images/SamSung/SonySamsungJ5PrimeB1_lon.png"]', 1)
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [DonGia], [NgayCapNhap], [CauHinh], [MoTa], [HinhAnh], [SoLuongTon], [LuotXem], [LuotBinhChon], [LuotBinhLuan], [SoLanMua], [Moi], [MaNCC], [MaNSX], [MaLoaiSP], [DaXoa], [HinhAnh1], [Home]) VALUES (10, N'Samsung Galaxy j7 Prime', CAST(5650000 AS Decimal(18, 0)), CAST(0x0000A767012439B8 AS DateTime), N'<div class="detail-col-left53 left">
<h3>Th&ocirc;ng số kỹ thuật</h3>

<ul>
	<li>Th&ocirc;ng số cơ bản</li>
	<li>M&agrave;n h&igrave;nh : 5.5 inch (1080 x 1920 pixels)</li>
	<li>Camera : Ch&iacute;nh: 13.0 MP, Phụ: 8.0 MP</li>
	<li>RAM : 3 GB</li>
	<li>Bộ nhớ trong : 32 GB</li>
	<li>Hệ điều h&agrave;nh : Android Marshmallow 6.0.1</li>
	<li>Chipset : Exynos 7870 8 nh&acirc;n 64-bit</li>
	<li>CPU : Octa-Core 1.60GHz</li>
	<li>K&iacute;ch thước : 151.5 x 74.9 x 8.1 mm</li>
	<li>GPU : Mali-T830</li>
</ul>

<ul>
	<li>M&agrave;n h&igrave;nh</li>
	<li>C&ocirc;ng nghệ m&agrave;n h&igrave;nh : PLS</li>
	<li>M&agrave;u m&agrave;n h&igrave;nh : 16 Triệu m&agrave;u</li>
	<li>Chuẩn m&agrave;n h&igrave;nh : Full HD</li>
	<li>Độ ph&acirc;n giải m&agrave;n h&igrave;nh : 1920 x 1080 pixels</li>
	<li>C&ocirc;ng nghệ cảm ứng : Đa điểm</li>
	<li>Mặt k&iacute;nh m&agrave;n h&igrave;nh : K&iacute;nh cường lực Gorilla Glass 4</li>
	<li>Camera Sau</li>
	<li>Độ ph&acirc;n giải : 13.0 MP</li>
	<li>Quay phim : Full HD 1080p@30fps</li>
	<li>Đ&egrave;n Flash : C&oacute;</li>
	<li>Chụp ảnh n&acirc;ng cao : Tự động lấy n&eacute;t, Chạm lấy n&eacute;t, Nhận diện khu&ocirc;n mặt, HDR, Panorama, Chế độ chụp chuy&ecirc;n nghiệp</li>
	<li>Camera Trước</li>
	<li>Video Call : C&oacute;</li>
	<li>Độ ph&acirc;n giải : 8.0 MP</li>
	<li>Th&ocirc;ng tin kh&aacute;c : Camera g&oacute;c rộng, Chế độ l&agrave;m đẹp, Nhận diện khu&ocirc;n mặt, Selfie bằng cử chỉ</li>
	<li>Cấu h&igrave;nh phần cứng</li>
	<li>Tốc độ CPU : 1.60 GHz</li>
	<li>Số nh&acirc;n : 8 nh&acirc;n</li>
	<li>Chipset : Exynos 7870</li>
	<li>RAM : 3 GB</li>
	<li>Chip đồ họa (GPU) : Mali-T830</li>
	<li>Cảm biến : V&acirc;n tay,gia tốc,khoảng c&aacute;ch</li>
	<li>Bộ nhớ &amp; Lưu trữ</li>
	<li>Danh bạ lưu trữ : Kh&ocirc;ng giới hạn</li>
	<li>Bộ nhớ trong : 32 GB</li>
	<li>Bộ nhớ c&ograve;n lại : ~25 GB</li>
	<li>Thẻ nhớ ngo&agrave;i : MicroSD (T-Flash)</li>
	<li>Hỗ trợ thẻ nhớ tối đa : 256 GB</li>
	<li>Thiết kế &amp; Trọng lượng</li>
	<li>Kiểu d&aacute;ng : Thanh (thẳng) + Cảm ứng</li>
	<li>Chất liệu : Hợp kim nh&ocirc;m nguy&ecirc;n khối (mặt k&iacute;nh cong 2,5D)</li>
	<li>K&iacute;ch thước : 151.5 x 74.9 x 8.1 mm</li>
	<li>Trọng lượng : 167g</li>
	<li>Khả năng chống nước : Kh&ocirc;ng</li>
	<li>Th&ocirc;ng tin pin</li>
	<li>Loại pin : Li-Ion</li>
	<li>Dung lượng pin : 3300 mAh</li>
	<li>Pin c&oacute; thể th&aacute;o rời : Kh&ocirc;ng</li>
	<li>Chế độ sạc nhanh : Kh&ocirc;ng</li>
	<li>Kết nối &amp; Cổng giao tiếp</li>
	<li>Băng tần 2G : GSM 850 / 900 / 1800 / 1900</li>
	<li>Băng tần 3G : WCDMA 850/ 2100 GHz</li>
	<li>Băng tần 4G : LTE Cat 4</li>
	<li>Hỗ trợ SIM : Nano Sim</li>
	<li>Khe cắm sim : 2 Khe</li>
	<li>Wifi : WiFi b/g/n, Wi-Fi Direct</li>
	<li>GPS : A-GPS, GLONASS</li>
	<li>Bluetooth : v4.1</li>
	<li>GPRS/EDGE : C&oacute;</li>
	<li>NFC : Kh&ocirc;ng</li>
	<li>Kết nối USB : Micro USB</li>
	<li>Cổng kết nối kh&aacute;c : Hỗ trợ OTG</li>
	<li>Cổng sạc : Micro USB</li>
	<li>Jack (Input &amp; Output) : 3.5 mm</li>
	<li>Giải tr&iacute; &amp; Ứng dụng</li>
	<li>Xem phim : H.265, 3GP, MP4, AVI, WMV, H.264(MPEG4-AVC), DivX, WMV9, Xvid</li>
	<li>Nghe nhạc : Lossless, MP3, WAV, WMA</li>
	<li>Ghi &acirc;m : C&oacute;</li>
	<li>FM radio : C&oacute;</li>
	<li>Bảo h&agrave;nh</li>
	<li>Thời gian bảo h&agrave;nh : 12 th&aacute;ng</li>
</ul>
</div>
', N'<h2 style="text-align:justify"><strong>Galaxy J7 Prime xứng đ&aacute;ng l&agrave; c&aacute;i t&ecirc;n h&agrave;ng đầu trong danh s&aacute;ch chọn smartphone phổ th&ocirc;ng của giới trẻ khi nhận được nhiều đ&aacute;nh gi&aacute; t&iacute;ch cực về thiết kế, thời lượng pin l&acirc;u d&agrave;i v&agrave; camera chụp h&igrave;nh chất lượng tốt.</strong></h2>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/84798/samsung-galaxy-j7-prime--2.jpg" onclick="return false;"><img alt="Thiết kế sang trọng" class="lazy" src="https://cdn.tgdd.vn/Products/Images/42/84798/samsung-galaxy-j7-prime--2.jpg" style="display:block" title="Thiết kế sang trọng" /></a></p>

<h3 style="text-align:justify"><strong>Thiết kế thời thượng</strong></h3>

<p>Vẻ ngo&agrave;i của Galaxy J7 Prime hết sức trẻ trung nhờ sự kết hợp giữa lớp vỏ kim loại sang trọng, mặt k&iacute;nh cường lực <a href="https://www.thegioididong.com/tin-tuc/kinh-cuong-luc-la-gi--596319#gorillaglass4" target="_blank" title="Gorilla Glass 4" type="Gorilla Glass 4">Gorilla Glass 4</a> được v&aacute;t cong 2.5D ở c&aacute;c cạnh.</p>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/84798/samsung-galaxy-j7-prime-den-12.jpg" onclick="return false;"><img alt="Thiết kế thời thượng" class="lazy" src="https://cdn2.tgdd.vn/Products/Images/42/84798/samsung-galaxy-j7-prime-den-12.jpg" style="display:block" title="Thiết kế thời thượng" /></a><br />
<em>Phi&ecirc;n bản m&agrave;u đen được đ&aacute;nh gi&aacute; đẹp nhất từ nhiều người d&ugrave;ng.</em></p>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/84798/samsung-galaxy-j7-prime-den-up-5-1.jpg" onclick="return false;"><img alt="Cạnh cong 2.5D" class="lazy" src="https://cdn4.tgdd.vn/Products/Images/42/84798/samsung-galaxy-j7-prime-den-up-5-1.jpg" style="display:block" title="Cạnh cong 2.5D" /></a><br />
<em>C&aacute;c cạnh cong 2.5D tạo cảm gi&aacute;c thoải m&aacute;i khi thao t&aacute;c tr&ecirc;n m&agrave;n h&igrave;nh.</em></p>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/84798/samsung-galaxy-j7-prime-mat-truoc.jpg" onclick="return false;"><img alt="Mặt trước hài hòa" class="lazy" src="https://cdn1.tgdd.vn/Products/Images/42/84798/samsung-galaxy-j7-prime-mat-truoc.jpg" style="display:block" title="Mặt trước hài hòa" /></a><br />
<em>Mặt trước được bố tr&iacute; h&agrave;i h&ograve;a.</em></p>

<h3 style="text-align:justify"><strong>Camera th&aacute;ch thức b&oacute;ng đ&ecirc;m với khẩu độ f/1.9</strong></h3>

<p>Bộ đ&ocirc;i camera tr&ecirc;n J7 Prime c&ugrave;ng sở hữu khẩu độ lớn f/1.9 - thường chỉ xuất hiện tr&ecirc;n c&aacute;c smartphone cao cấp, gi&uacute;p chụp thiếu s&aacute;ng tốt hơn, tốc độ lấy n&eacute;t v&agrave; lưu ảnh chụp rất nhanh. Gần như chỉ cần ấn chụp l&agrave; ảnh đ&atilde; được lưu.</p>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/84798/samsung-galaxy-j7-prime-hong-up-11.jpg" onclick="return false;"><img alt="Camera chất lượng tốt" class="lazy" src="https://cdn3.tgdd.vn/Products/Images/42/84798/samsung-galaxy-j7-prime-hong-up-11.jpg" style="display:block" title="Camera chất lượng tốt" /></a></p>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/84798/samsung-galaxy-j7-prime-chup-thieu-sang.jpg" onclick="return false;"><img alt="Ảnh chụp thiếu sáng từ J7 Prime" class="lazy" src="https://cdn.tgdd.vn/Products/Images/42/84798/samsung-galaxy-j7-prime-chup-thieu-sang.jpg" style="display:block" title="Ảnh chụp thiếu sáng từ J7 Prime" /></a><br />
<em>Ảnh chụp trong ph&ograve;ng với &aacute;nh s&aacute;ng yếu vẫn rất chi tiết</em></p>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/84798/samsung-galaxy-j7-prime-chup-thieu-sang-2.jpg" onclick="return false;"><img alt="Ảnh chụp khi trời tối với nước ảnh chi tiết" class="lazy" src="https://cdn2.tgdd.vn/Products/Images/42/84798/samsung-galaxy-j7-prime-chup-thieu-sang-2.jpg" style="display:block" title="Ảnh chụp khi trời tối với nước ảnh chi tiết" /></a><br />
<em>Ảnh chụp l&uacute;c trời chuẩn bị tối.</em></p>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/84798/samsung-galaxy-j7-prime-camera-truoc.jpg" onclick="return false;"><img alt="Camera trước chụp ảnh độ mịn cao" class="lazy" src="https://cdn4.tgdd.vn/Products/Images/42/84798/samsung-galaxy-j7-prime-camera-truoc.jpg" style="display:block" title="Camera trước chụp ảnh độ mịn cao" /></a><br />
Camera trước chụp ảnh s&aacute;ng v&agrave; độ mịn cao, gi&uacute;p m&agrave;u da tr&ocirc;ng đẹp hơn.</p>

<h3>Cấu h&igrave;nh ổn định cho sức mạnh tốt</h3>

<p>Galaxy J7 Prime nổi bật với RAM 3 GB, bộ nhớ trong 32 GB gi&uacute;p bạn c&oacute; thể chạy đa nhiệm nhiều ứng dụng, game trơn tru hơn, khả năng lưu trữ dữ liệu nhiều hơn.&nbsp;Chip Exynos 7870 tr&ecirc;n J7 Prime c&oacute; hiệu năng kh&aacute; cao, gi&uacute;p bạn thoải m&aacute;i chơi game đồ họa cao. Bạn c&oacute; thể chơi c&aacute;c game đồ họa cao như Asphalt 8 với m&uacute;c đồ họa trung b&igrave;nh m&agrave; kh&ocirc;ng hề c&oacute; cảm gi&aacute;c giật lag khi chơi.</p>

<p style="text-align:center">&nbsp;</p>

<div class="video"><iframe frameborder="0" src="https://www.youtube.com/embed/wWiL8sxAOZM?rel=0"></iframe></div>

<p>&nbsp;</p>

<h3>Cảm biến v&acirc;n tay một chạm</h3>

<p>J7 Prime l&agrave; smartphone đầu ti&ecirc;n thuộc d&ograve;ng smartphone phổ th&ocirc;ng của Samsung sở hữu cảm biến v&acirc;n tay một chạm. Tuy độ nhạy chưa thật sự so s&aacute;nh được với d&ograve;ng sản phẩm cao cấp nhưng một chạm gi&uacute;p bạn mở m&aacute;y m&agrave; kh&ocirc;ng cần ấn bất k&igrave; ph&iacute;m n&agrave;o cũng l&agrave; một điều rất thuận tiện.</p>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/84798/samsung-galaxy-j7-prime-up-vang-dong-13-1.jpg" onclick="return false;"><img alt="Cảm biến vân tay 1 chạm nhạy" class="lazy" src="https://cdn1.tgdd.vn/Products/Images/42/84798/samsung-galaxy-j7-prime-up-vang-dong-13-1.jpg" style="display:block" title="Cảm biến vân tay 1 chạm nhạy" /></a></p>

<h3>Thời lượng pin khủng</h3>

<p>J7 Prime sở hữu vi&ecirc;n pin dung lượng 3300 mAh, thời gian sử dụng pin được nhiều người d&ugrave;ng đ&aacute;nh gi&aacute; rất cao, với cường độ sử dụng nhiều v&agrave; li&ecirc;n tục, J7 Prime vẫn trụ vững được đến tận cuối ng&agrave;y.&nbsp;</p>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/84798/samsung-galaxy-j7-prime-pin-1.jpg" onclick="return false;"><img alt="Thời gian sử dụng pin" class="lazy" src="https://cdn3.tgdd.vn/Products/Images/42/84798/samsung-galaxy-j7-prime-pin-1.jpg" style="display:block" title="Thời gian sử dụng pin" /></a><br />
<em>Thời gian sử dụng pin của Galaxy J7 Prime với m&agrave;n h&igrave;nh được đặt ở độ s&aacute;ng 75%.</em></p>

<h3 style="text-align:justify">Với gi&aacute; tiền chưa tới 6 triệu, J7 Prime đang thực sự tạo ra một cơn sốt với thiết kế thu h&uacute;t, thời lượng pin d&agrave;i c&ugrave;ng nhiều c&ocirc;ng nghệ cao cấp như camera khẩu độ f/1.9, cảm biến v&acirc;n tay 1 chạm hứa hẹn sẽ l&agrave;m h&agrave;i l&ograve;ng mọi đối tượng người d&ugrave;ng.</h3>
', N'/UploadFile/images/SamSung/SonysamsungJ7PrimeB8_lon.png', 6, 0, NULL, NULL, NULL, 1, 1, 4, 1, 0, N'["/UploadFile/images/SamSung/SonysamsungJ7PrimeB2_lon.png","/UploadFile/images/SamSung/SonysamsungJ7PrimeB3_lon.png","/UploadFile/images/SamSung/SonysamsungJ7PrimeB4_lon.png","/UploadFile/images/SamSung/SonysamsungJ7PrimeB1_lon.png","/UploadFile/images/SamSung/SonysamsungJ7PrimeB5_lon.png","/UploadFile/images/SamSung/SonysamsungJ7PrimeB6_lon.png","/UploadFile/images/SamSung/SonysamsungJ7PrimeB7_lon.png","/UploadFile/images/SamSung/SonysamsungJ7PrimeB_lon.png"]', 1)
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [DonGia], [NgayCapNhap], [CauHinh], [MoTa], [HinhAnh], [SoLuongTon], [LuotXem], [LuotBinhChon], [LuotBinhLuan], [SoLanMua], [Moi], [MaNCC], [MaNSX], [MaLoaiSP], [DaXoa], [HinhAnh1], [Home]) VALUES (11, N'Samsung Galaxy S7', CAST(14299000 AS Decimal(18, 0)), CAST(0x0000A767012EC7EE AS DateTime), N'<div class="detail-col-left53 left">
<h3>Th&ocirc;ng số kỹ thuật</h3>

<ul>
	<li>Th&ocirc;ng số cơ bản</li>
	<li>M&agrave;n h&igrave;nh : 5.1 inch (1440 x 2560 pixels)</li>
	<li>Camera : Ch&iacute;nh: 12.0 MP, Phụ: 5.0MP</li>
	<li>RAM : 4 GB</li>
	<li>Bộ nhớ trong : 32 GB</li>
	<li>Hệ điều h&agrave;nh : Android 6.0 (Marshmallow)</li>
	<li>Chipset : Exynos 8890</li>
	<li>CPU : 4 nh&acirc;n 2.3 GHz v&agrave; 4 nh&acirc;n 1.6 GHz</li>
	<li>GPU : Mali-T880 MP12</li>
	<li>K&iacute;ch thước : 142.4 x 69.6 x 7.9 mm</li>
</ul>

<ul>
	<li>M&agrave;n h&igrave;nh</li>
	<li>C&ocirc;ng nghệ m&agrave;n h&igrave;nh : Super AMOLED</li>
	<li>M&agrave;u m&agrave;n h&igrave;nh : 16 Triệu m&agrave;u</li>
	<li>Chuẩn m&agrave;n h&igrave;nh : Quad HD</li>
	<li>Độ ph&acirc;n giải m&agrave;n h&igrave;nh : 1440 x 2560 pixels</li>
	<li>C&ocirc;ng nghệ cảm ứng : Điện dung đa điểm</li>
	<li>Mặt k&iacute;nh m&agrave;n h&igrave;nh : K&iacute;nh cường lực Gorilla Glass 4</li>
	<li>Camera Sau</li>
	<li>Độ ph&acirc;n giải : 12.0 MP</li>
	<li>Quay phim : Quay phim 4K 2160p@30fps</li>
	<li>Đ&egrave;n Flash : C&oacute;</li>
	<li>Chụp ảnh n&acirc;ng cao : Ảnh Raw, Tự động lấy n&eacute;t, Chạm lấy n&eacute;t, Nhận diện khu&ocirc;n mặt, HDR, Panorama, Chống rung quang học</li>
	<li>Camera Trước</li>
	<li>Video Call : C&oacute;</li>
	<li>Độ ph&acirc;n giải : 5.0 MP</li>
	<li>Th&ocirc;ng tin kh&aacute;c : Selfie bằng cử chỉ,Nhận diện khu&ocirc;n mặt, Chế độ l&agrave;m đẹp, Tự động lấy n&eacute;t, Camera g&oacute;c rộng, Selfie ngược s&aacute;ng HDR</li>
	<li>Cấu h&igrave;nh phần cứng</li>
	<li>Tốc độ CPU : 4 nh&acirc;n 2.3 GHz v&agrave; 4 nh&acirc;n 1.6 GHz</li>
	<li>Số nh&acirc;n : 8 Nh&acirc;n</li>
	<li>Chipset : Exynos 8890</li>
	<li>RAM : 4 GB</li>
	<li>Chip đồ họa (GPU) : Mali-T880 MP12</li>
	<li>Cảm biến : V&acirc;n tay, gia tốc kế, con quay hồi chuyển, khoảng c&aacute;ch, la b&agrave;n, phong vũ biểu, nhịp tim, SpO2</li>
	<li>Bộ nhớ &amp; Lưu trữ</li>
	<li>Danh bạ lưu trữ : Kh&ocirc;ng giới hạn</li>
	<li>Bộ nhớ trong : 32 GB</li>
	<li>Bộ nhớ c&ograve;n lại : ~24 GB</li>
	<li>Thẻ nhớ ngo&agrave;i : MicroSD (T-Flash)</li>
	<li>Hỗ trợ thẻ nhớ tối đa : 200 GB</li>
	<li>Thiết kế &amp; Trọng lượng</li>
	<li>Kiểu d&aacute;ng : Thanh (thẳng) + Cảm ứng</li>
	<li>K&iacute;ch thước : 142.4 x 69.6 x 7.9 mm</li>
	<li>Trọng lượng : 152g</li>
	<li>Khả năng chống nước : Chuẩn IP68</li>
	<li>Th&ocirc;ng tin pin</li>
	<li>Loại pin : Li-ion</li>
	<li>Dung lượng pin : 3000 mAh</li>
	<li>Pin c&oacute; thể th&aacute;o rời : Kh&ocirc;ng</li>
	<li>Chế độ sạc nhanh : Sạc nhanh Quick Charge 2.0</li>
	<li>Kết nối &amp; Cổng giao tiếp</li>
	<li>Băng tần 2G : GSM 850/ 900/ 1800/ 1900 MHz</li>
	<li>Băng tần 3G : HSDPA 850/ 900/ 1900/ 2100 GHz</li>
	<li>Băng tần 4G : LTE</li>
	<li>Hỗ trợ SIM : Nano SIM</li>
	<li>Khe cắm sim : 2 SIM Nano (SIM 2 chung khe thẻ nhớ)</li>
	<li>Wifi : Wi-Fi 802.11 a/b/g/n/ac, dual-band, DLNA, Wi-Fi Direct, Wi-Fi hotspot</li>
	<li>GPS : A-GPS, GLONASS, BDS</li>
	<li>Bluetooth : v4.2, A2DP, LE, apt-X</li>
	<li>GPRS/EDGE : C&oacute;</li>
	<li>NFC : C&oacute;</li>
	<li>Kết nối USB : MicroUSB</li>
	<li>Cổng kết nối kh&aacute;c : OTG</li>
	<li>Cổng sạc : MicroUSB</li>
	<li>Jack (Input &amp; Output) : 3.5 mm</li>
	<li>Giải tr&iacute; &amp; Ứng dụng</li>
	<li>Xem phim : MP4/ DivX/ XviD/ WMV/ H.264</li>
	<li>Nghe nhạc : MP3/ WAV/ WMA/ eAAC+/ FLAC</li>
	<li>Ghi &acirc;m : C&oacute;</li>
	<li>FM radio : Kh&ocirc;ng</li>
	<li>Bảo h&agrave;nh</li>
	<li>Thời gian bảo h&agrave;nh : 12 Th&aacute;ng</li>
</ul>
</div>
', N'<div class="boxArticle">
<h2 style="text-align:justify"><strong>Với những n&acirc;ng cấp tuyệt vời m&agrave; <a href="https://www.thegioididong.com/dtdd-samsung" target="_blank" title="Các dòng điện thoại thương hiệu Samsung">Samsung</a> mang lại cho người d&ugrave;ng như khả năng chống nước, thiết kế ho&agrave;n hảo, Galaxy S7 sẽ khiến bạn kh&ocirc;ng thể rời mắt.</strong></h2>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/74113/samsung-galaxy-s75-3.jpg" onclick="return false;"><img alt="Thiết kế hoàn hảo" class="lazy" src="https://cdn4.tgdd.vn/Products/Images/42/74113/samsung-galaxy-s75-3.jpg" style="display:block" title="Thiết kế hoàn hảo" /></a></p>

<h3 style="text-align:justify"><strong>Khả năng chống nước của Gaxaly S7</strong></h3>

<p style="text-align:justify"><a href="https://www.thegioididong.com/dtdd/samsung-galaxy-s7" title="Samsung Galaxy S7" type="Samsung Galaxy S7">Galaxy S7</a> đạt chuẩn chống nước IP68 gi&uacute;p bảo vệ m&aacute;y vẫn an to&agrave;n khi v&ocirc; t&igrave;nh l&agrave;m đổ nước hay d&iacute;nh nước mưa.&nbsp;</p>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/74113/samsung-galaxy-s72-2.jpg" onclick="return false;"><img alt="Đạt chuẩn chống nước IP68" class="lazy" src="https://cdn1.tgdd.vn/Products/Images/42/74113/samsung-galaxy-s72-2.jpg" style="display:block" title="Đạt chuẩn chống nước IP68" /></a></p>

<p>Chuẩn IP68 gi&uacute;p m&aacute;y c&oacute; thể ng&acirc;m dưới độ s&acirc;u 1,5m trong tối đa 30 ph&uacute;t, tuy nhi&ecirc;n bạn kh&ocirc;ng n&ecirc;n thử t&iacute;nh năng n&agrave;y v&igrave; nếu c&oacute; trục trặc dẫn tới v&agrave;o nước th&igrave; m&aacute;y bạn sẽ kh&ocirc;ng được hưởng chế độ bảo h&agrave;nh.</p>

<h3 style="text-align:justify"><strong>Thiết kế bo cong mềm mại</strong></h3>

<p>Galaxy S7 với c&aacute;c cạnh m&aacute;y được bo cong một c&aacute;ch mềm mại, gi&uacute;p m&aacute;y cầm gọn trong l&ograve;ng b&agrave;n tay v&agrave; kh&ocirc;ng hề c&oacute; cảm gi&aacute;c cấn. C&aacute;c g&oacute;c cạnh ở mặt trước được m&agrave;i cong 2.5D tạo cảm gi&aacute;c thoải m&aacute;i khi thao t&aacute;c tr&ecirc;n m&agrave;n h&igrave;nh.</p>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/74113/samsung-galaxy-s73-3.jpg" onclick="return false;"><img alt="Thiết kế cong mềm mại" class="lazy" src="https://cdn3.tgdd.vn/Products/Images/42/74113/samsung-galaxy-s73-3.jpg" style="display:block" title="Thiết kế cong mềm mại" /></a></p>

<p style="text-align:center"><em>Mặt lưng v&agrave; mặt trước l&agrave;m từ k&iacute;nh Corning Gorilla Glass 4 chống trầy xước tốt</em></p>

<div style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/74113/samsung-galaxy-s76-2.jpg" onclick="return false;"><img alt="Cầm tay thoải mái" class="lazy" src="https://cdn.tgdd.vn/Products/Images/42/74113/samsung-galaxy-s76-2.jpg" style="display:block" title="Cầm tay thoải mái" /></a></div>

<p style="text-align:center"><em>Mặt lưng m&aacute;y được l&agrave;m cong 2 b&ecirc;n để tăng sự thoải m&aacute;i v&agrave; chắc chắn hơn khi cầm tay</em></p>

<h3 style="text-align:justify"><strong>Camera ấn tượng</strong></h3>

<p style="text-align:justify">Camera Galaxy S7 c&oacute; độ ph&acirc;n giải Dual Pixel&nbsp;12 MP với 2 đi-ốt quang&nbsp;gi&uacute;p cho bức ảnh chụp trong điều kiện thiếu s&aacute;ng rất tốt, chất lượng ảnh được đ&aacute;nh gi&aacute; rất cao trong mọi điều kiện chụp.</p>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/74113/samsung-galaxy-s74-3.jpg" onclick="return false;"><img alt="Camera cực tốt" class="lazy" src="https://cdn2.tgdd.vn/Products/Images/42/74113/samsung-galaxy-s74-3.jpg" style="display:block" title="Camera cực tốt" /></a></p>

<p style="text-align:center"><em>C&ocirc;ng nghệ 2 đi-ốt quang gi&uacute;p lấy n&eacute;t cực nhanh v&agrave; ch&iacute;nh x&aacute;c</em></p>

<p style="text-align:center">&nbsp;</p>

<div class="twentytwenty-horizontal twentytwenty-wrapper">
<div class="imgCprW twentytwenty-container" id="imgCpr_1" style="height:299px"><img class="twentytwenty-before" src="https://cdn.tgdd.vn/Products/Images/42/74113/samsung-galaxy-s74-2.jpg" style="clip:rect(0px 320px 299px 0px)" /> <img class="twentytwenty-after" src="https://cdn.tgdd.vn/Products/Images/42/74113/samsung-galaxy-s73-2.jpg" />
<div class="twentytwenty-overlay">
<div class="twentytwenty-before-label">&nbsp;</div>

<div class="twentytwenty-after-label">&nbsp;</div>
</div>

<div class="twentytwenty-handle" style="left:320px">&nbsp;</div>
</div>
</div>

<p>&nbsp;</p>

<p style="text-align:center"><em>Khả năng lấy n&eacute;t cực tốt</em></p>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/74113/samsung-galaxy-s78.jpg" onclick="return false;"><img alt="Hình ảnh rất xuất sắc" class="lazy" src="https://cdn4.tgdd.vn/Products/Images/42/74113/samsung-galaxy-s78.jpg" style="display:block" title="Hình ảnh rất xuất sắc" /></a></p>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/74113/samsung-galaxy-s79.jpg" onclick="return false;"><img alt="Hình ảnh rất xuất sắc" class="lazy" src="https://cdn1.tgdd.vn/Products/Images/42/74113/samsung-galaxy-s79.jpg" style="display:block" title="Hình ảnh rất xuất sắc" /></a></p>

<p style="text-align:center"><em>H&igrave;nh ảnh rất xuất sắc</em></p>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/74113/samsung-galaxy-s710.jpg" onclick="return false;"><img alt="Camera trước với độ phân giải 5 MP, tính năng nhận diện bàn tay chụp hình thông minh và nhanh nhạy" class="lazy" src="https://cdn3.tgdd.vn/Products/Images/42/74113/samsung-galaxy-s710.jpg" style="display:block" title="Camera trước với độ phân giải 5 MP, tính năng nhận diện bàn tay chụp hình thông minh và nhanh nhạy" /></a></p>

<p style="text-align:center"><em>Camera trước với độ ph&acirc;n giải 5 MP, t&iacute;nh năng nhận diện b&agrave;n tay chụp h&igrave;nh th&ocirc;ng minh v&agrave; nhanh nhạy</em></p>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/74113/samsung-galaxy-s7100.gif" onclick="return false;"><img alt="Cách hoạt động của nhận diện bàn tay bằng camera trước" class="lazy" src="https://cdn.tgdd.vn/Products/Images/42/74113/samsung-galaxy-s7100.gif" style="display:block" title="Cách hoạt động của nhận diện bàn tay bằng camera trước" /></a></p>

<p style="text-align:center"><em>C&aacute;ch hoạt động của nhận diện b&agrave;n tay bằng camera trước</em></p>

<h3 style="text-align:justify"><strong>Cấu h&igrave;nh mạnh mẽ</strong></h3>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/74113/samsung-galaxy-s717.jpg" onclick="return false;"><img alt="Vận hành mạnh mẽ" class="lazy" src="https://cdn2.tgdd.vn/Products/Images/42/74113/samsung-galaxy-s717.jpg" style="display:block" title="Vận hành mạnh mẽ" /></a></p>

<p style="text-align:center"><em>M&aacute;y được trang bị chip Exynos mới nhất&nbsp;8890, 8 nh&acirc;n, đặc biệt RAM 4 GB gi&uacute;p tốc độ xử l&yacute; tr&ecirc;n m&aacute;y tuyệt vời, tốc độ CPU nhanh hơn 30%, đặc biệt S7 trang bị chất lỏng tản nhiệt gi&uacute;p m&aacute;y kh&ocirc;ng bị n&oacute;ng trong qu&aacute; tr&igrave;nh sử dụng</em></p>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/74113/samsung-galaxy-s71-3.jpg" onclick="return false;"><img alt="Game Laucher" class="lazy" src="https://cdn4.tgdd.vn/Products/Images/42/74113/samsung-galaxy-s71-3.jpg" style="display:block" title="Game Laucher" /></a></p>

<p style="text-align:center"><em>T&iacute;nh năng ho&agrave;n to&agrave;n mới t&ecirc;n Game Laucher cho ph&eacute;p t&ugrave;y chỉnh tiết kiệm năng lượng hay kh&ocirc;ng l&agrave;m phiền khi chơi game</em></p>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/74113/samsung-galaxy-s715.jpg" onclick="return false;"><img alt="Nếu bộ nhớ trong 32 GB vẫn ít, bạn vẫn có thể lựa chọn mở rộng thẻ nhớ lên đến 128 GB" class="lazy" src="https://cdn1.tgdd.vn/Products/Images/42/74113/samsung-galaxy-s715.jpg" style="display:block" title="Nếu bộ nhớ trong 32 GB vẫn ít, bạn vẫn có thể lựa chọn mở rộng thẻ nhớ lên đến 128 GB" /></a></p>

<p style="text-align:center"><em>Nếu bộ nhớ trong 32 GB vẫn &iacute;t, bạn vẫn c&oacute; thể lựa chọn mở rộng <a href="https://www.thegioididong.com/the-nho-dien-thoai" target="_blank" title="Thẻ nhớ cho điện thoại">thẻ nhớ</a> l&ecirc;n đến <a href="https://www.thegioididong.com/tin-tuc/the-nho-microsd-200gb-dau-tien-tren-the-gioi-trinh-616608" target="_blank" title="thẻ nhớ 200 GB">200 GB</a></em></p>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/74113/samsung-galaxy-s716.jpg" onclick="return false;"><img alt="Tính năng sạc nhanh" class="lazy" src="https://cdn3.tgdd.vn/Products/Images/42/74113/samsung-galaxy-s716.jpg" style="display:block" title="Tính năng sạc nhanh" /></a></p>

<p style="text-align:center"><em>Sạc nhanh bằng d&acirc;y cho 1 tiếng 30 ph&uacute;t đ&atilde; đầy 100% cho dung lượng pin 3000 mAh, ngo&agrave;i ra c&ograve;n c&oacute; t&iacute;nh năng sạc kh&ocirc;ng d&acirc;y tiện lợi nhưng bạn cần mua ri&ecirc;ng phụ kiện</em></p>

<p style="text-align:justify">Với những thay đổi ngoạn mục như khả năng chống nước, bụi tốt, camera với rất nhiều cải tiến th&ocirc;ng minh v&agrave; cấu h&igrave;nh mạnh mẽ đầy xuất sắc chắc chắn Galaxy S7 sẽ khiến bạn kh&ocirc;ng thể rời mắt.</p>
</div>
', N'/UploadFile/images/SamSung/Sonysamsungs7B5_lon.png', 11, 0, NULL, NULL, NULL, 1, 1, 4, 1, 0, N'["/UploadFile/images/SamSung/Sonysamsungs7B1_lon.png","/UploadFile/images/SamSung/Sonysamsungs7B2_lon.png","/UploadFile/images/SamSung/Sonysamsungs7B3_lon.png","/UploadFile/images/SamSung/Sonysamsungs7B6_lon.png","/UploadFile/images/SamSung/Sonysamsungs7B_lon.png"]', 1)
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [DonGia], [NgayCapNhap], [CauHinh], [MoTa], [HinhAnh], [SoLuongTon], [LuotXem], [LuotBinhChon], [LuotBinhLuan], [SoLanMua], [Moi], [MaNCC], [MaNSX], [MaLoaiSP], [DaXoa], [HinhAnh1], [Home]) VALUES (12, N'Samsung Galaxy S7 Edge', CAST(15300000 AS Decimal(18, 0)), CAST(0x0000A7670131D53F AS DateTime), N'<div class="detail-col-left53 left">
<h3>Th&ocirc;ng số kỹ thuật</h3>

<ul>
	<li>Th&ocirc;ng số cơ bản</li>
	<li>M&agrave;n h&igrave;nh : 5.5 inch (1440 x 2560 pixels)</li>
	<li>Camera : Ch&iacute;nh: 12.0 MP, Phụ 5.0 MP</li>
	<li>RAM : 4 GB</li>
	<li>Bộ nhớ trong : 32 GB</li>
	<li>Hệ điều h&agrave;nh : Android 6.0 (Marshmallow)</li>
	<li>Chipset : Exynos 8890</li>
	<li>CPU : Octa-core</li>
	<li>GPU : Mali-T880 MP12</li>
	<li>K&iacute;ch thước : 150.9 x 72.6 x 7.7 mm</li>
</ul>

<ul>
	<li>M&agrave;n h&igrave;nh</li>
	<li>C&ocirc;ng nghệ m&agrave;n h&igrave;nh : Super AMOLED</li>
	<li>M&agrave;u m&agrave;n h&igrave;nh : 16 Triệu m&agrave;u</li>
	<li>Chuẩn m&agrave;n h&igrave;nh : Quad HD</li>
	<li>Độ ph&acirc;n giải m&agrave;n h&igrave;nh : 1440 x 2560 pixels</li>
	<li>C&ocirc;ng nghệ cảm ứng : Điện dung đa điểm</li>
	<li>Mặt k&iacute;nh m&agrave;n h&igrave;nh : K&iacute;nh cường lực Gorilla Glass 4</li>
	<li>Camera Sau</li>
	<li>Độ ph&acirc;n giải : 12.0 MP</li>
	<li>Quay phim : Quay phim 4K 2160p@30fps</li>
	<li>Đ&egrave;n Flash : C&oacute;</li>
	<li>Chụp ảnh n&acirc;ng cao : Ảnh Raw, Tự động lấy n&eacute;t, Chạm lấy n&eacute;t, Nhận diện khu&ocirc;n mặt, HDR, Panorama, Chống rung quang học</li>
	<li>Camera Trước</li>
	<li>Video Call : C&oacute;</li>
	<li>Độ ph&acirc;n giải : 5.0 MP</li>
	<li>Th&ocirc;ng tin kh&aacute;c : HDR, Camera g&oacute;c rộng, Chế độ l&agrave;m đẹp, Nhận diện khu&ocirc;n mặt, Selfie bằng cử chỉ</li>
	<li>Cấu h&igrave;nh phần cứng</li>
	<li>Tốc độ CPU : Octa-core</li>
	<li>Số nh&acirc;n : 8 Nh&acirc;n</li>
	<li>Chipset : Exynos 8890</li>
	<li>RAM : 4 GB</li>
	<li>Chip đồ họa (GPU) : Mali-T880 MP12</li>
	<li>Cảm biến : V&acirc;n tay, gia tốc kế, con quay hồi chuyển, khoảng c&aacute;ch, la b&agrave;n, phong vũ biểu, nhịp tim, SpO2</li>
	<li>Bộ nhớ &amp; Lưu trữ</li>
	<li>Danh bạ lưu trữ : Kh&ocirc;ng giới hạn</li>
	<li>Bộ nhớ trong : 32 GB</li>
	<li>Bộ nhớ c&ograve;n lại : ~24 GB</li>
	<li>Thẻ nhớ ngo&agrave;i : MicroSD (T-Flash)</li>
	<li>Hỗ trợ thẻ nhớ tối đa : 200 GB</li>
	<li>Thiết kế &amp; Trọng lượng</li>
	<li>Kiểu d&aacute;ng : Thanh (thẳng) + Cảm ứng</li>
	<li>Chất liệu : Khung kim loại + mặt k&iacute;nh cường lực</li>
	<li>K&iacute;ch thước : 150.9 x 72.6 x 7.7 mm</li>
	<li>Trọng lượng : 157g</li>
	<li>Khả năng chống nước : Chuẩn IP68</li>
	<li>Th&ocirc;ng tin pin</li>
	<li>Loại pin : Li-ion</li>
	<li>Dung lượng pin : 3600mAh</li>
	<li>Pin c&oacute; thể th&aacute;o rời : Kh&ocirc;ng</li>
	<li>Chế độ sạc nhanh : Sạc nhanh Quick Charge 2.0</li>
	<li>Kết nối &amp; Cổng giao tiếp</li>
	<li>Băng tần 2G : GSM 850/ 900/ 1800/ 1900 MHz</li>
	<li>Băng tần 3G : HSDPA 850/ 900/ 1900/ 2100 GHz</li>
	<li>Băng tần 4G : LTE</li>
	<li>Hỗ trợ SIM : Nano SIM</li>
	<li>Khe cắm sim : 2 SIM</li>
	<li>Wifi : Wi-Fi 802.11 a/b/g/n/ac, dual-band, DLNA, Wi-Fi Direct, Wi-Fi hotspot</li>
	<li>GPS : A-GPS, GLONASS, BDS</li>
	<li>Bluetooth : v4.2, A2DP, LE, apt-X</li>
	<li>GPRS/EDGE : C&oacute;</li>
	<li>NFC : C&oacute;</li>
	<li>Kết nối USB : MicroUSB</li>
	<li>Cổng kết nối kh&aacute;c : Hỗ trợ OTG</li>
	<li>Cổng sạc : MicroUSB</li>
	<li>Jack (Input &amp; Output) : 3.5 mm</li>
	<li>Giải tr&iacute; &amp; Ứng dụng</li>
	<li>Xem phim : MP4/ DivX/ XviD/ WMV/ H.264</li>
	<li>Nghe nhạc : Midi, Lossless, MP3, WAV, WMA, eAAC+, AC3, FLAC</li>
	<li>Ghi &acirc;m : C&oacute;</li>
	<li>FM radio : Kh&ocirc;ng</li>
	<li>Bảo h&agrave;nh</li>
	<li>Thời gian bảo h&agrave;nh : 12 Th&aacute;ng</li>
</ul>
</div>
', N'<div class="short_intro txt_666" style="color:#444444; float:left; font-family:arial; font-size:14px; font-stretch:normal; font-weight:700; line-height:18px; padding:0px 0px 10px; text-rendering:geometricPrecision; width:660.016px">Galaxy S7 gọn v&agrave; nhỏ hơn với m&agrave;n h&igrave;nh 5,1 inch trong khi Galaxy S7 edge m&agrave;n h&igrave;nh cong tr&agrave;n viền c&oacute; k&iacute;ch thước nhỉnh hơn.</div>

<div class="block_content_slide_showdetail" style="padding:0px">
<div id="article_content" style="padding:0px">
<div class="item_slide_show" style="clear:both; margin-bottom:0px; margin-left:10px; margin-right:10px; margin-top:0px; padding:0px; width:660.016px">
<div class="block_thumb_slide_show" style="float:left; font-size:0px; line-height:0; padding:0px; position:relative; text-align:center; width:660.016px"><img alt="Bộ đôi Galaxy S7 và Galaxy S7 edge so dáng" class="left" id="vne_slide_image_0" src="http://img.f8.sohoa.vnecdn.net/2016/02/21/12-Samsung-Galaxy-S7-S7edge-VnE-7928-1456036781_660x0.jpg" style="max-width:580px; opacity:1" />&nbsp;</div>

<div class="desc_cation" style="background:#f5f5f5; float:left; padding:10px 13.1875px; width:633.609px">
<p>&nbsp;</p>

<p>Galaxy S7 v&agrave; Galaxy S7 edge l&agrave; hai smartphone Android cao cấp được Samsung giới thiệu c&ugrave;ng l&uacute;c tại sự kiện Unpacked 2016. Kh&ocirc;ng như năm ngo&aacute;i, Galaxy S7 edge sở hữu m&agrave;n h&igrave;nh cong tr&agrave;n viền 5,5 inch nhưng lớn hơn Galaxy S6 edge, đem lại trải nghiệm h&igrave;nh ảnh th&uacute; vị khi xem phim, xem ảnh. Trong khi đ&oacute;, S7 ph&ugrave; hợp với số đ&ocirc;ng người d&ugrave;ng smartphone hiện giờ v&igrave; kiểu d&aacute;ng gọn g&agrave;ng hơn.</p>

<p>&nbsp;</p>
</div>

<div class="clear" style="clear:both; font-size:0px; line-height:0; padding:0px">&nbsp;</div>
</div>

<div class="item_slide_show" style="clear:both; margin-bottom:0px; margin-left:10px; margin-right:10px; margin-top:0px; padding:0px; width:660.016px">
<div class="block_thumb_slide_show" style="float:left; font-size:0px; line-height:0; padding:0px; position:relative; text-align:center; width:660.016px"><img alt="Bộ đôi Galaxy S7 và Galaxy S7 edge so dáng" class="left" id="vne_slide_image_1" src="http://img.f6.sohoa.vnecdn.net/2016/02/21/12-Samsung-Galaxy-S7-S7edge-VnE-7930-1456036837_660x0.jpg" style="max-width:580px; opacity:1" />&nbsp;</div>

<div class="desc_cation" style="background:#f5f5f5; float:left; padding:10px 13.1875px; width:633.609px">
<p>&nbsp;</p>

<p>Cả hai c&oacute; chung ng&ocirc;n ngữ thiết kế lẫn m&agrave;u sắc. Phần lưng của Galaxy S7 lẫn S7 edge đều được l&agrave;m cong nhẹ về hai viền, nhờ vậy m&aacute;y cầm đều gọn v&agrave; thoải m&aacute;i hơn thế hệ cũ. Samsung sử dụng chất liệu k&iacute;nh cường lực v&agrave; kim loại.</p>

<p>&nbsp;</p>
</div>

<div class="clear" style="clear:both; font-size:0px; line-height:0; padding:0px">&nbsp;</div>
</div>

<div class="item_slide_show" style="clear:both; margin-bottom:0px; margin-left:10px; margin-right:10px; margin-top:0px; padding:0px; width:660.016px">
<div class="block_thumb_slide_show" style="float:left; font-size:0px; line-height:0; padding:0px; position:relative; text-align:center; width:660.016px"><img alt="Bộ đôi Galaxy S7 và Galaxy S7 edge so dáng" class="left" id="vne_slide_image_2" src="http://img.f7.sohoa.vnecdn.net/2016/02/21/12-Samsung-Galaxy-S7-S7edge-VnE-7933-1456036857_660x0.jpg" style="max-width:580px; opacity:1" />&nbsp;</div>

<div class="desc_cation" style="background:#f5f5f5; float:left; padding:10px 13.1875px; width:633.609px">
<p>&nbsp;</p>

<p>Khung viền của Galaxy S7 cũng được l&agrave;m cong, v&aacute;t v&agrave;o nhưng kh&ocirc;ng nhiều như Galaxy S7 edge. C&aacute;ch bố tr&iacute; ph&iacute;m bấm tr&ecirc;n cả hai model đều giống nhau, dễ thao t&aacute;c.</p>

<p>&nbsp;</p>
</div>

<div class="clear" style="clear:both; font-size:0px; line-height:0; padding:0px">&nbsp;</div>
</div>

<div class="item_slide_show" style="clear:both; margin-bottom:0px; margin-left:10px; margin-right:10px; margin-top:0px; padding:0px; width:660.016px">
<div class="block_thumb_slide_show" style="float:left; font-size:0px; line-height:0; padding:0px; position:relative; text-align:center; width:660.016px"><img alt="Bộ đôi Galaxy S7 và Galaxy S7 edge so dáng" class="left" id="vne_slide_image_3" src="http://img.f6.sohoa.vnecdn.net/2016/02/21/12-Samsung-Galaxy-S7-S7edge-VnE-7934-1456036885_660x0.jpg" style="max-width:580px; opacity:1" />&nbsp;</div>

<div class="desc_cation" style="background:#f5f5f5; float:left; padding:10px 13.1875px; width:633.609px">
<p>&nbsp;</p>

<p>Kh&ocirc;ng c&ograve;n mắt hồng ngoại d&ugrave;ng l&agrave;m điều khiển từ xa, nhưng khe cắm thẻ nhớ đ&atilde; được Samsung mang trở lại bộ đ&ocirc;i Galaxy S7 v&agrave; S7 edge.</p>

<p>&nbsp;</p>
</div>

<div class="clear" style="clear:both; font-size:0px; line-height:0; padding:0px">&nbsp;</div>
</div>

<div class="item_slide_show" style="clear:both; margin-bottom:0px; margin-left:10px; margin-right:10px; margin-top:0px; padding:0px; width:660.016px">
<div class="block_thumb_slide_show" style="float:left; font-size:0px; line-height:0; padding:0px; position:relative; text-align:center; width:660.016px"><img alt="Bộ đôi Galaxy S7 và Galaxy S7 edge so dáng" class="left" id="vne_slide_image_4" src="http://img.f5.sohoa.vnecdn.net/2016/02/21/12-Samsung-Galaxy-S7-S7edge-VnE-7935-1456036908_660x0.jpg" style="max-width:580px; opacity:1" />&nbsp;</div>

<div class="desc_cation" style="background:#f5f5f5; float:left; padding:10px 13.1875px; width:633.609px">
<p>&nbsp;</p>

<p>Cạnh đ&aacute;y l&agrave; loa ngo&agrave;i, giắc tai nghe v&agrave; cổng sạc microUSB hỗ trợ sạc nhanh. Cả Galaxy S7 v&agrave; S7 edge đều c&oacute; th&ecirc;m khả năng chống nước v&agrave; chống bụi với ti&ecirc;u chuẩn IP68, c&oacute; thể an to&agrave;n ở độ s&acirc;u 1,5 dưới nước v&agrave; trong v&ograve;ng 30 ph&uacute;t.</p>

<p>&nbsp;</p>
</div>

<div class="clear" style="clear:both; font-size:0px; line-height:0; padding:0px">&nbsp;</div>
</div>

<div class="item_slide_show" style="clear:both; margin-bottom:0px; margin-left:10px; margin-right:10px; margin-top:0px; padding:0px; width:660.016px">
<div class="block_thumb_slide_show" style="float:left; font-size:0px; line-height:0; padding:0px; position:relative; text-align:center; width:660.016px"><img alt="Bộ đôi Galaxy S7 và Galaxy S7 edge so dáng" class="left" id="vne_slide_image_5" src="http://img.f5.sohoa.vnecdn.net/2016/02/21/12-Samsung-Galaxy-S7-S7edge-VnE-7937-1456036931_660x0.jpg" style="max-width:580px; opacity:1" />&nbsp;</div>

<div class="desc_cation" style="background:#f5f5f5; float:left; padding:10px 13.1875px; width:633.609px">
<p>&nbsp;</p>

<p>Vẫn giữ được thiết kế mỏng nhưng pin của Galaxy S7 v&agrave; S7 edge được n&acirc;ng l&ecirc;n cao, lần lượt 3.000 v&agrave; 3.600 mAh. B&ecirc;n trong m&aacute;y c&ograve;n c&oacute; hệ thống tản nhiệt bằng chất lỏng gi&uacute;p bớt n&oacute;ng khi hoạt động với t&aacute;c vụ nặng.</p>

<p>&nbsp;</p>
</div>

<div class="clear" style="clear:both; font-size:0px; line-height:0; padding:0px">&nbsp;</div>
</div>

<div class="item_slide_show" style="clear:both; margin-bottom:0px; margin-left:10px; margin-right:10px; margin-top:0px; padding:0px; width:660.016px">
<div class="block_thumb_slide_show" style="float:left; font-size:0px; line-height:0; padding:0px; position:relative; text-align:center; width:660.016px"><img alt="Bộ đôi Galaxy S7 và Galaxy S7 edge so dáng" class="left" id="vne_slide_image_6" src="http://img.f6.sohoa.vnecdn.net/2016/02/21/12-Samsung-Galaxy-S7-S7edge-VnE-7938-1456036985_660x0.jpg" style="max-width:580px; opacity:1" />&nbsp;</div>

<div class="desc_cation" style="background:#f5f5f5; float:left; padding:10px 13.1875px; width:633.609px">
<p>&nbsp;</p>

<p>Bộ đ&ocirc;i Galaxy S7 c&oacute; ba m&agrave;u sắc kh&aacute;c nhau, trong khi phi&ecirc;n bản m&agrave;u v&agrave;ng giống năm ngo&aacute;i th&igrave; hai m&agrave;u c&ograve;n lại tr&ocirc;ng mới lạ hơn. Bản m&agrave;u đen tối v&agrave; sẫm hơn, trong khi m&agrave;u trắng ngọc trai được thay bằng m&agrave;u bạc. Với m&agrave;n h&igrave;nh cong, Galaxy S7 edge tr&ocirc;ng c&aacute; t&iacute;nh hơn đ&agrave;n em.</p>

<p>&nbsp;</p>
</div>

<div class="clear" style="clear:both; font-size:0px; line-height:0; padding:0px">&nbsp;</div>
</div>

<div class="item_slide_show" style="clear:both; margin-bottom:0px; margin-left:10px; margin-right:10px; margin-top:0px; padding:0px; width:660.016px">
<div class="block_thumb_slide_show" style="float:left; font-size:0px; line-height:0; padding:0px; position:relative; text-align:center; width:660.016px"><img alt="Bộ đôi Galaxy S7 và Galaxy S7 edge so dáng" class="left" id="vne_slide_image_7" src="http://img.f7.sohoa.vnecdn.net/2016/02/21/12-Samsung-Galaxy-S7-S7edge-VnE-7940-1456037038_660x0.jpg" style="max-width:580px; opacity:1" />&nbsp;</div>

<div class="desc_cation" style="background:#f5f5f5; float:left; padding:10px 13.1875px; width:633.609px">
<p>&nbsp;</p>

<p>Cả hai model đều được trang bị m&agrave;n h&igrave;nh QuadHD với độ ph&acirc;n giải 2K Pixel c&oacute; th&ecirc;m t&iacute;nh năng mới l&agrave; Always Display On, m&agrave;n h&igrave;nh lu&ocirc;n hiển thị cho ph&eacute;p xem nhanh giờ, lịch hay th&ocirc;ng b&aacute;o.&nbsp;</p>
</div>
</div>
</div>
</div>
', N'/UploadFile/images/SamSung/Sonysamsungs7EgdeB10_lon.png', 6, 0, NULL, NULL, NULL, 1, 1, 4, 1, 0, N'["/UploadFile/images/SamSung/Sonysamsungs7EgdeB1_lon.png","/UploadFile/images/SamSung/Sonysamsungs7EgdeB2_lon.png","/UploadFile/images/SamSung/Sonysamsungs7EgdeB3_lon.png","/UploadFile/images/SamSung/Sonysamsungs7EgdeB4_lon.png","/UploadFile/images/SamSung/Sonysamsungs7EgdeB5_lon.png","/UploadFile/images/SamSung/Sonysamsungs7EgdeB6_lon.png","/UploadFile/images/SamSung/Sonysamsungs7EgdeB_lon.png"]', 1)
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [DonGia], [NgayCapNhap], [CauHinh], [MoTa], [HinhAnh], [SoLuongTon], [LuotXem], [LuotBinhChon], [LuotBinhLuan], [SoLanMua], [Moi], [MaNCC], [MaNSX], [MaLoaiSP], [DaXoa], [HinhAnh1], [Home]) VALUES (13, N'Samsung Galaxy A5 2017', CAST(8500000 AS Decimal(18, 0)), CAST(0x0000A7670188C774 AS DateTime), N'<div class="detail-col-left53 left">
<h3>Th&ocirc;ng số kỹ thuật</h3>

<ul>
	<li>Th&ocirc;ng số cơ bản</li>
	<li>M&agrave;n h&igrave;nh : 5.2 inch (1080 x 1920 pixels )</li>
	<li>Camera : Ch&iacute;nh: 16.0 MP; Phụ: 16.0 MP</li>
	<li>RAM : 3 GB</li>
	<li>Bộ nhớ trong : 32 GB</li>
	<li>Hệ điều h&agrave;nh : Android 6.0 (Marshmallow)</li>
	<li>Chipset : Exynos 7880</li>
	<li>CPU : 1.9 GHz</li>
	<li>K&iacute;ch thước : 146,1 x 71,4 x 7.9mm</li>
	<li>GPU : Mali-T830MP2</li>
</ul>

<ul>
	<li>M&agrave;n h&igrave;nh</li>
	<li>C&ocirc;ng nghệ m&agrave;n h&igrave;nh : Super AMOLED</li>
	<li>M&agrave;u m&agrave;n h&igrave;nh : 16 Triệu m&agrave;u</li>
	<li>Chuẩn m&agrave;n h&igrave;nh : Full HD</li>
	<li>Độ ph&acirc;n giải m&agrave;n h&igrave;nh : 1080 x 1920 pixels</li>
	<li>C&ocirc;ng nghệ cảm ứng : Cảm ứng điện dung, đa điểm</li>
	<li>Mặt k&iacute;nh m&agrave;n h&igrave;nh : K&iacute;nh cường lực</li>
	<li>Camera Sau</li>
	<li>Độ ph&acirc;n giải : 16.0 MP</li>
	<li>Quay phim : Full HD 1080p@30fps</li>
	<li>Đ&egrave;n Flash : C&oacute;</li>
	<li>Chụp ảnh n&acirc;ng cao : Tự động lấy n&eacute;t, Chạm lấy n&eacute;t, Nhận diện khu&ocirc;n mặt, HDR, Panorama</li>
	<li>Camera Trước</li>
	<li>Video Call : C&oacute;</li>
	<li>Độ ph&acirc;n giải : 16.0 MP</li>
	<li>Th&ocirc;ng tin kh&aacute;c : Quay video Full HD, Nhận diện khu&ocirc;n mặt</li>
	<li>Cấu h&igrave;nh phần cứng</li>
	<li>Tốc độ CPU : 1.9 GHz</li>
	<li>Số nh&acirc;n : 8 nh&acirc;n</li>
	<li>Chipset : Exynos 7880</li>
	<li>RAM : 3 GB</li>
	<li>Chip đồ họa (GPU) : Mali-T830MP2</li>
	<li>Cảm biến : V&acirc;n tay,gia tốc, khoảng c&aacute;ch,&aacute;p kế,la b&agrave;n</li>
	<li>Bộ nhớ &amp; Lưu trữ</li>
	<li>Danh bạ lưu trữ : Kh&ocirc;ng giới hạn</li>
	<li>Bộ nhớ trong : 32 GB</li>
	<li>Bộ nhớ c&ograve;n lại : ~23 GB</li>
	<li>Thẻ nhớ ngo&agrave;i : MicroSD (T-Flash)</li>
	<li>Hỗ trợ thẻ nhớ tối đa : 256 GB</li>
	<li>Thiết kế &amp; Trọng lượng</li>
	<li>Kiểu d&aacute;ng : Thanh (thẳng) + Cảm ứng</li>
	<li>Chất liệu : Khung kim loại + mặt k&iacute;nh cường lực (Cong 2,5D)</li>
	<li>K&iacute;ch thước : 146,1 x 71,4 x 7.9mm</li>
	<li>Trọng lượng : 157g</li>
	<li>Khả năng chống nước : Chuẩn IP68</li>
	<li>Th&ocirc;ng tin pin</li>
	<li>Loại pin : Pin chuẩn Li-Ion</li>
	<li>Dung lượng pin : 3000 mAh</li>
	<li>Pin c&oacute; thể th&aacute;o rời : Kh&ocirc;ng</li>
	<li>Chế độ sạc nhanh : C&oacute;</li>
	<li>Kết nối &amp; Cổng giao tiếp</li>
	<li>Băng tần 2G : GSM 850/900/1800/1900</li>
	<li>Băng tần 3G : HSDPA</li>
	<li>Băng tần 4G : 4G LTE Cat 6</li>
	<li>Hỗ trợ SIM : Nano Sim</li>
	<li>Khe cắm sim : 2 SIM</li>
	<li>Wifi : Wi-Fi 802.11 a/b/g/n/ac, Dual-band, Wi-Fi Direct, Wi-Fi hotspot</li>
	<li>GPS : BDS, A-GPS, GLONASS</li>
	<li>Bluetooth : v4.2, A2DP, LE, EDR</li>
	<li>GPRS/EDGE : C&oacute;</li>
	<li>NFC : C&oacute;</li>
	<li>Kết nối USB : USB Type-C</li>
	<li>Cổng kết nối kh&aacute;c : OTG</li>
	<li>Cổng sạc : USB Type-C</li>
	<li>Jack (Input &amp; Output) : 3.5 mm</li>
	<li>Giải tr&iacute; &amp; Ứng dụng</li>
	<li>Xem phim : 3GP, MP4, AVI, WMV, H.263, H.264(MPEG4-AVC), DivX, WMV9, Xvid</li>
	<li>Nghe nhạc : Lossless, Midi, MP3, WAV, WMA, WMA9, AAC, AAC+, AAC++, eAAC+, OGG, AC3, FLAC</li>
	<li>Ghi &acirc;m : C&oacute;</li>
	<li>FM radio : C&oacute;</li>
	<li>Bảo h&agrave;nh</li>
	<li>Thời gian bảo h&agrave;nh : 12 th&aacute;ng</li>
</ul>
</div>
', N'<h2><strong>Đẳng cấp, sang trọng, dẫn đầu xu thế l&agrave; những từ ngữ chuẩn x&aacute;c để mi&ecirc;u tả về điện thoại Samsung Galaxy A5(2017).</strong></h2>

<p>&nbsp;<a class="preventdefault" href="https://www.thegioididong.com/images/42/88268/galaxy-a5-2017-1.jpg" onclick="return false;"><img alt="Thiết kế đẳng cấp, sang trọng" class="lazy" src="https://cdn1.tgdd.vn/Products/Images/42/88268/galaxy-a5-2017-1.jpg" style="display:block" title="Thiết kế đẳng cấp, sang trọng" /></a><br />
<strong>Tinh tế, cao cấp từ thiết kế: </strong>Samsung Galaxy A5 (2017) mang tr&ecirc;n m&igrave;nh vẻ ngo&agrave;i kế thừa từ si&ecirc;u phẩm <a href="https://www.thegioididong.com/dtdd/samsung-galaxy-s7" target="_blank" title="Galaxy S7" type="Galaxy S7">Galaxy S7</a>, sự h&agrave;i ho&agrave; của kim loại cao cấp v&agrave; 2 mặt k&iacute;nh cong tạo ra một thiết kế sang trọng, tinh tế, to&aacute;t l&ecirc;n gi&aacute; trị của sản phẩm v&agrave; người sử dụng.<br />
&nbsp;<a class="preventdefault" href="https://www.thegioididong.com/images/42/88268/galaxy-a5-2017-2.jpg" onclick="return false;"><img alt="Mặt lưng cong 3D độc đáo" class="lazy" src="https://cdn3.tgdd.vn/Products/Images/42/88268/galaxy-a5-2017-2.jpg" style="display:block" title="Mặt lưng cong 3D độc đáo" /></a><br />
C&aacute;c ph&iacute;m tăng giảm &acirc;m lượng được thiết kế tỉ mỉ nằm gọn b&ecirc;n cạnh tr&ecirc;n b&ecirc;n tr&aacute;i.</p>

<p><a class="preventdefault" href="https://www.thegioididong.com/images/42/88268/galaxy-a5-2017-3.jpg" onclick="return false;"><img alt="Cạnh trái của máy" class="lazy" src="https://cdn.tgdd.vn/Products/Images/42/88268/galaxy-a5-2017-3.jpg" style="display:block" title="Cạnh trái của máy" /></a><br />
&nbsp;<br />
Ph&iacute;m nguồn cũng được thiết kế dễ bấm, độ nảy cao, loa được thiết kế s&aacute;ng tạo gi&uacute;p việc giải tr&iacute; được tốt hơn.<br />
&nbsp;<a class="preventdefault" href="https://www.thegioididong.com/images/42/88268/galaxy-a5-2017-4.jpg" onclick="return false;"><img alt="Cạnh phải của máy" class="lazy" src="https://cdn2.tgdd.vn/Products/Images/42/88268/galaxy-a5-2017-4.jpg" style="display:block" title="Cạnh phải của máy" /></a><br />
M&agrave;n h&igrave;nh Super AMOLED với m&agrave;u sắc tươi s&aacute;ng, k&iacute;ch thước 5.2 inch gi&uacute;p thiết bị c&oacute; k&iacute;ch thước vừa tay với hầu hết đối tượng người d&ugrave;ng, độ ph&acirc;n giải 1080 x 1920 pixels hiển thị sắc n&eacute;t. M&agrave;n k&iacute;nh cong 2.5D thời thượng gi&uacute;p thao t&aacute;c chạm lướt sẽ thoải m&aacute;i v&agrave; mượt m&agrave;.<br />
&nbsp;<a class="preventdefault" href="https://www.thegioididong.com/images/42/88268/galaxy-a5-2017-5.jpg" onclick="return false;"><img alt="Màn hình Full HD sắc nét" class="lazy" src="https://cdn4.tgdd.vn/Products/Images/42/88268/galaxy-a5-2017-5.jpg" style="display:block" title="Màn hình Full HD sắc nét" /></a><br />
<strong>Hiệu năng tốt với CPU 8 nh&acirc;n</strong><br />
Cấu h&igrave;nh Exynos 7880 cho tốc độ 1.9 GHz cho trải nghiệm lướt web, chơi game kh&aacute; tốt.<br />
&nbsp;<a class="preventdefault" href="https://www.thegioididong.com/images/42/88268/galaxy-a5-2017-6.jpg" onclick="return false;"><img alt="Cấu hình mạnh mẽ" class="lazy" src="https://cdn1.tgdd.vn/Products/Images/42/88268/galaxy-a5-2017-6.jpg" style="display:block" title="Cấu hình mạnh mẽ" /></a><br />
RAM 3 GB đi c&ugrave;ng hệ điều h&agrave;nh <a href="https://www.thegioididong.com/hoi-dap/android-6-marshmallow-co-gi-hot-726146" target="_blank" title="Android 6.0 (Marshmallow)" type="Android 6.0 (Marshmallow)">Android 6.0 (Marshmallow)</a> cho việc đa nhiệm cực tốt, ROM 32 GB đi k&egrave;m thẻ nhớ ngo&agrave;i l&ecirc;n đến 256 GB.</p>

<p><br />
<strong>Nhiều t&iacute;nh năng ấn tượng</strong><br />
Cổng <a href="https://www.thegioididong.com/hoi-dap/usb-type-c-chua-n-mu-c-co-ng-ke-t-no-i-mo-i-723760" target="_blank" title="USB Type-C" type="USB Type-C">USB Type-C</a> được trang bị sẵn tr&ecirc;n A5(2017) cho tốc độ truyền tải si&ecirc;u nhanh, đi k&egrave;m n&oacute; l&agrave; c&ocirc;ng nghệ <a href="https://www.thegioididong.com/hoi-dap/cong-nghe-sac-nhanh-tren-smartphone-755549" target="_blank" title="sạc pin nhanh" type="sạc pin nhanh">sạc pin nhanh</a> rất hữu &iacute;ch.<br />
&nbsp;<a class="preventdefault" href="https://www.thegioididong.com/images/42/88268/galaxy-a5-2017-7.jpg" onclick="return false;"><img alt="Cổng USB Type-C tiện dụng" class="lazy" src="https://cdn3.tgdd.vn/Products/Images/42/88268/galaxy-a5-2017-7.jpg" style="display:block" title="Cổng USB Type-C tiện dụng" /></a><br />
Cảm biến v&acirc;n tay một chạm, vừa mở m&aacute;y nhanh ch&oacute;ng vừa bảo mật dữ liệu.<br />
&nbsp;<a class="preventdefault" href="https://www.thegioididong.com/images/42/88268/galaxy-a5-2017-8.jpg" onclick="return false;"><img alt="Cảm biến vân tay 1 chạm" class="lazy" src="https://cdn.tgdd.vn/Products/Images/42/88268/galaxy-a5-2017-8.jpg" style="display:block" title="Cảm biến vân tay 1 chạm" /></a><br />
Galaxy A5 (2017) được t&iacute;ch hợp khả năng chống bụi v&agrave; nước chuẩn IP68 cho ph&eacute;p ng&acirc;m điện thoại dưới nước ở độ s&acirc;u 1.5 m&eacute;t trong 30 ph&uacute;t.<br />
&nbsp;<a class="preventdefault" href="https://www.thegioididong.com/images/42/88268/galaxy-a5-2017-9.jpg" onclick="return false;"><img alt="Công nghệ chống nước đạt chuẩn IP68" class="lazy" src="https://cdn2.tgdd.vn/Products/Images/42/88268/galaxy-a5-2017-9.jpg" style="display:block" title="Công nghệ chống nước đạt chuẩn IP68" /></a><br />
<strong>Camera ấn tượng với khẩu độ f1.9</strong><br />
Camera sau xuất hiện một điều đặc biệt l&agrave; n&oacute; kh&ocirc;ng bị lồi l&ecirc;n so với mặt lưng, một cải tiến cực k&igrave; gi&aacute; trị. Vừa tạo t&iacute;nh thẩm mỹ vừa bảo vệ tốt mặt k&iacute;nh camera.<br />
&nbsp;<a class="preventdefault" href="https://www.thegioididong.com/images/42/88268/galaxy-a5-2017-10.jpg" onclick="return false;"><img alt="Camera ấn tượng với khẩu độ f1.9" class="lazy" src="https://cdn4.tgdd.vn/Products/Images/42/88268/galaxy-a5-2017-10.jpg" style="display:block" title="Camera ấn tượng với khẩu độ F1.9" /></a><br />
C&ugrave;ng một độ ph&acirc;n giải 16 MP v&agrave; khẩu độ f1.9 bộ đ&ocirc;i camera của A5(2017) cho chất lượng h&igrave;nh ảnh thực sự nổi bật.<br />
&nbsp;<a class="preventdefault" href="https://www.thegioididong.com/images/42/88268/galaxy-a5-2017-11.jpg" onclick="return false;"><img alt="Camera có cùng độ phân giải 16 MP" class="lazy" src="https://cdn1.tgdd.vn/Products/Images/42/88268/galaxy-a5-2017-11.jpg" style="display:block" title="Camera có cùng độ phân giải 16 MP" /></a><br />
Nhiều t&iacute;nh năng th&uacute; vị cũng c&oacute; sẵn tr&ecirc;n bộ đ&ocirc;i camera n&agrave;y như: tự động lấy n&eacute;t, gắn thẻ địa lý, chạm lấy n&eacute;t... cho bạn thỏa sức s&aacute;ng tạo những g&igrave; m&igrave;nh th&iacute;ch.<br />
&nbsp;<a class="preventdefault" href="https://www.thegioididong.com/images/42/88268/galaxy-a5-2017-12.jpg" onclick="return false;"><img alt="Nhiều tính năng thú vị trên camera" class="lazy" src="https://cdn3.tgdd.vn/Products/Images/42/88268/galaxy-a5-2017-12.jpg" style="display:block" title="Nhiều tính năng thú vị trên camera" /></a><br />
Sự cải tiến mạnh mẽ so với thế hệ trước, đi k&egrave;m nhiều c&ocirc;ng nghệ mới mẻ, hiện đại, chắc chắn Samsung sẽ th&agrave;nh c&ocirc;ng với con b&agrave;i A5(2017).</p>
', N'/UploadFile/images/SamSung/SonySSA52017B1_lon150217110531.png', 16, 0, NULL, NULL, NULL, 1, 1, 4, 1, 0, N'["/UploadFile/images/SamSung/SonySSA52017B2_lon150217110524.png","/UploadFile/images/SamSung/SonySSA52017B3_lon.png","/UploadFile/images/SamSung/SonySSA52017B4_lon150217110509.png","/UploadFile/images/SamSung/SonySSA52017B5_lon150217110501.png","/UploadFile/images/SamSung/SonySSA52017B6_lon.png","/UploadFile/images/SamSung/SonySSA52017B7_lon.png","/UploadFile/images/SamSung/SonySSA52017B8_lon150217110440.png"]', 1)
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [DonGia], [NgayCapNhap], [CauHinh], [MoTa], [HinhAnh], [SoLuongTon], [LuotXem], [LuotBinhChon], [LuotBinhLuan], [SoLanMua], [Moi], [MaNCC], [MaNSX], [MaLoaiSP], [DaXoa], [HinhAnh1], [Home]) VALUES (14, N'Samsung Galaxy A7 2017', CAST(10500000 AS Decimal(18, 0)), CAST(0x0000A76C00B2CE10 AS DateTime), N'<div class="detail-col-left53 left">
<h3>Th&ocirc;ng số kỹ thuật</h3>

<ul>
	<li>Th&ocirc;ng số cơ bản</li>
	<li>M&agrave;n h&igrave;nh : Super AMOLED, 5.7&quot;, Full HD</li>
	<li>Camera : Ch&iacute;nh: 16.0 MP; Phụ: 16.0 MP</li>
	<li>RAM : 3 GB</li>
	<li>Bộ nhớ trong : 32 GB</li>
	<li>Hệ điều h&agrave;nh : Android 6.0 (Marshmallow)</li>
	<li>Chipset : Exynos 7880</li>
	<li>CPU : 1.9 GHz</li>
	<li>GPU : Mali-T830MP2</li>
	<li>K&iacute;ch thước : 156.8 x 77.6 x 7.9 mm</li>
</ul>

<ul>
	<li>M&agrave;n h&igrave;nh</li>
	<li>C&ocirc;ng nghệ m&agrave;n h&igrave;nh : Super AMOLED</li>
	<li>M&agrave;u m&agrave;n h&igrave;nh : 16 Triệu m&agrave;u</li>
	<li>Chuẩn m&agrave;n h&igrave;nh : Full HD</li>
	<li>Độ ph&acirc;n giải m&agrave;n h&igrave;nh : 1080 x 1920 pixels</li>
	<li>C&ocirc;ng nghệ cảm ứng : Cảm ứng điện dung đa điểm</li>
	<li>Mặt k&iacute;nh m&agrave;n h&igrave;nh : K&iacute;nh cường lực Gorilla Glass 4</li>
	<li>Camera Sau</li>
	<li>Độ ph&acirc;n giải : 16.0 MP</li>
	<li>Quay phim : Full HD 1080p@30fps</li>
	<li>Đ&egrave;n Flash : C&oacute;</li>
	<li>Chụp ảnh n&acirc;ng cao : Gắn thẻ địa lý, Tự động lấy n&eacute;t, Chạm lấy n&eacute;t, Nhận diện khu&ocirc;n mặt, HDR, Panorama</li>
	<li>Camera Trước</li>
	<li>Video Call : C&oacute;</li>
	<li>Độ ph&acirc;n giải : 16.0 MP</li>
	<li>Th&ocirc;ng tin kh&aacute;c : Quay Video FullHD</li>
	<li>Cấu h&igrave;nh phần cứng</li>
	<li>Tốc độ CPU : 1.9 GHz</li>
	<li>Số nh&acirc;n : 8 nh&acirc;n</li>
	<li>Chipset : Exynos</li>
	<li>RAM : 3 GB</li>
	<li>Chip đồ họa (GPU) : Mali-T830MP2</li>
	<li>Bộ nhớ &amp; Lưu trữ</li>
	<li>Danh bạ lưu trữ : Kh&ocirc;ng giới hạn</li>
	<li>Bộ nhớ trong : 32 GB</li>
	<li>Bộ nhớ c&ograve;n lại : Đang cập nhật</li>
	<li>Thẻ nhớ ngo&agrave;i : Micro SD</li>
	<li>Hỗ trợ thẻ nhớ tối đa : 256 GB</li>
	<li>Thiết kế &amp; Trọng lượng</li>
	<li>Chất liệu : Kim loại</li>
	<li>Kiểu d&aacute;ng : Thanh + Cảm ứng</li>
	<li>K&iacute;ch thước : 156.8 x 77.6 x 7.9 mm</li>
	<li>Trọng lượng : Đang cập nhật</li>
	<li>Khả năng chống nước : Chuẩn IP68</li>
	<li>Th&ocirc;ng tin pin</li>
	<li>Loại pin : Li-Ion</li>
	<li>Dung lượng pin : 3600 mAh</li>
	<li>Pin c&oacute; thể th&aacute;o rời : Kh&ocirc;ng</li>
	<li>Kết nối &amp; Cổng giao tiếp</li>
	<li>Băng tần 2G : GSM 850/900/1800/1900</li>
	<li>Băng tần 3G : HSDPA 850/900/1900/2100</li>
	<li>Băng tần 4G : 4G LTE Cat 6</li>
	<li>Hỗ trợ SIM : 2 SIM 2 s&oacute;ng</li>
	<li>Khe cắm sim : Nano SIM</li>
	<li>Wifi : Wi-Fi 802.11 a/b/g/n/ac, Dual-band, Wi-Fi Direct, Wi-Fi hotspot</li>
	<li>GPS : BDS, A-GPS, GLONASS</li>
	<li>Bluetooth : v4.2, A2DP, LE, EDR</li>
	<li>GPRS/EDGE : C&oacute;</li>
	<li>NFC : C&oacute;</li>
	<li>Kết nối USB : USB Type-C</li>
	<li>Cổng kết nối kh&aacute;c : OTG</li>
	<li>Cổng sạc : USB Type-C</li>
	<li>Jack (Input &amp; Output) : 3.5 mm</li>
	<li>Giải tr&iacute; &amp; Ứng dụng</li>
	<li>Xem phim : 3GP, MP4, AVI, WMV, H.264(MPEG4-AVC), Xvid</li>
	<li>Nghe nhạc : MP3, WAV, WMA, AAC, OGG, FLAC</li>
	<li>Ghi &acirc;m : C&oacute;</li>
	<li>FM radio : C&oacute;</li>
	<li>Chức năng kh&aacute;c : Mặt k&iacute;nh 2.5D Mở khóa nhanh bằng v&acirc;n tay Chống nước, chống bụi Sạc pin nhanh</li>
	<li>Bảo h&agrave;nh</li>
	<li>Thời gian bảo h&agrave;nh : 12 th&aacute;ng</li>
</ul>
</div>
', N'<p><strong>Samsung Galaxy A7 (2017) tạo bước đột ph&aacute; cho d&ograve;ng A với thiết kế sang trọng v&agrave; đẳng cấp, cấu h&igrave;nh mạnh mẽ, nhiều tiện &iacute;ch cao cấp.</strong></p>

<p><a class="preventdefault" href="https://www.thegioididong.com/images/42/88537/samsung-galaxy-a7-2017-1-2.jpg" onclick="return false;"><img alt="Thiết kế tinh tế và cao cấp" class="lazy" src="https://cdn.tgdd.vn/Products/Images/42/88537/samsung-galaxy-a7-2017-1-2.jpg" style="display:block" title="Thiết kế tinh tế và cao cấp" /></a><br />
&nbsp;<br />
<strong>Sang trọng, tinh tế:</strong> Galaxy A7 (2017) l&agrave; sự kết hợp h&agrave;i ho&agrave; của kim loại cao cấp v&agrave; 2 mặt k&iacute;nh cong kế thừa từ thiết kế của smartphone cao cấp <a href="https://www.thegioididong.com/dtdd/samsung-galaxy-s7" target="_blank" title="Galaxy S7" type="Galaxy S7">Galaxy S7</a>. C&aacute;c ph&iacute;m tăng giảm &acirc;m lượng được thiết kế tỉ mỉ nằm gọn b&ecirc;n cạnh tr&ecirc;n b&ecirc;n tr&aacute;i.</p>

<p><a class="preventdefault" href="https://www.thegioididong.com/images/42/88537/samsung-galaxy-a7-2017-15.jpg" onclick="return false;"><img alt="Cạnh trái của máy" class="lazy" src="https://cdn2.tgdd.vn/Products/Images/42/88537/samsung-galaxy-a7-2017-15.jpg" style="display:block" title="Cạnh trái của máy" /></a></p>

<p>Ph&iacute;m nguồn cũng được l&agrave;m sao cho dễ sử dụng, loa được thiết kế s&aacute;ng tạo gi&uacute;p trải nghiệm &acirc;m thanh tốt hơn.</p>

<p><a class="preventdefault" href="https://www.thegioididong.com/images/42/88537/samsung-galaxy-a7-20172.jpg" onclick="return false;"><img alt="Cạnh phải của máy" class="lazy" src="https://cdn4.tgdd.vn/Products/Images/42/88537/samsung-galaxy-a7-20172.jpg" style="display:block" title="Cạnh phải của máy" /></a></p>

<p>M&agrave;n h&igrave;nh <a href="https://www.thegioididong.com/hoi-dap/man-hinh-super-amoled-la-gi-905770" target="_blank" title="Super AMOLED" type="Super AMOLED">Super AMOLED</a>, 5.7 inch, độ ph&acirc;n giải 1080 x 1920 pixels hiển thị sắc n&eacute;t. Mặt k&iacute;nh cong 2.5D thời thượng, thao t&aacute;c chạm lướt sẽ thoải m&aacute;i v&agrave; mượt m&agrave; hơn.</p>

<p><a class="preventdefault" href="https://www.thegioididong.com/images/42/88537/samsung-galaxy-a7-20175.jpg" onclick="return false;"><img alt="Mặt trước " class="lazy" src="https://cdn1.tgdd.vn/Products/Images/42/88537/samsung-galaxy-a7-20175.jpg" style="display:block" title="Mặt trước " /></a></p>

<p><strong>Hiệu năng mạnh mẽ:&nbsp;</strong>Cấu h&igrave;nh Exynos 7880 với tốc độ 1.9 GHz cho trải nghiệm lướt web, chơi game kh&aacute; tốt.</p>

<p><a class="preventdefault" href="https://www.thegioididong.com/images/42/88537/samsung-galaxy-a7-2017-5-1.jpg" onclick="return false;"><img alt="Hiệu năng khá tốt" class="lazy" src="https://cdn3.tgdd.vn/Products/Images/42/88537/samsung-galaxy-a7-2017-5-1.jpg" style="display:block" title="Hiệu năng khá tốt" /></a><br />
&nbsp;<br />
RAM 3 GB đi c&ugrave;ng hệ điều h&agrave;nh <a href="https://www.thegioididong.com/hoi-dap/android-6-marshmallow-co-gi-hot-726146" target="_blank" title="Android 6.0 (Marshmallow)" type="Android 6.0 (Marshmallow)">Android 6.0 (Marshmallow)</a> cho việc đa nhiệm cực tốt, bộ nhớ lưu trữ của m&aacute;y 32 GB v&agrave; hỗ trợ thẻ nhớ ngo&agrave;i l&ecirc;n đến 256 GB.</p>

<p><br />
<strong>Nhiều t&iacute;nh năng hữu &iacute;ch</strong><br />
Cổng <a href="https://www.thegioididong.com/hoi-dap/usb-type-c-chua-n-mu-c-co-ng-ke-t-no-i-mo-i-723760" target="_blank" title="USB Type-C" type="USB Type-C">USB Type-C</a> được trang bị sẵn tr&ecirc;n A7 (2017) đi k&egrave;m n&oacute; l&agrave; c&ocirc;ng nghệ <a href="https://www.thegioididong.com/hoi-dap/cong-nghe-sac-nhanh-tren-smartphone-755549" target="_blank" title="sạc pin nhanh" type="sạc pin nhanh">sạc pin nhanh</a>. Ngo&agrave;i ra chuẩn Type-C c&oacute; tốc độ truyền tải dữ liệu của chuẩn 3.1 với tốc độ tối đa l&ecirc;n đến 10 Gbps, gấp đ&ocirc;i 3.0 v&agrave; tương đương chuẩn Thunderbolt.</p>

<p><a class="preventdefault" href="https://www.thegioididong.com/images/42/88537/samsung-galaxy-a7-20174.jpg" onclick="return false;"><img alt="Cổng Type-c" class="lazy" src="https://cdn.tgdd.vn/Products/Images/42/88537/samsung-galaxy-a7-20174.jpg" style="display:block" title="Cổng Type-c" /></a></p>

<p>&nbsp;<br />
Cảm biến v&acirc;n tay một chạm, vừa mở m&aacute;y nhanh ch&oacute;ng vừa bảo mật dữ liệu.</p>

<p><a class="preventdefault" href="https://www.thegioididong.com/images/42/88537/samsung-galaxy-a7-2017-7-1.jpg" onclick="return false;"><img alt="Cảm biến vân tay 1 chạm hiện đại" class="lazy" src="https://cdn2.tgdd.vn/Products/Images/42/88537/samsung-galaxy-a7-2017-7-1.jpg" style="display:block" title="Cảm biến vân tay 1 chạm hiện đại" /></a><br />
&nbsp;<br />
Galaxy A7 (2017) được t&iacute;ch hợp khả năng <a href="https://www.thegioididong.com/hoi-dap/chong-nuoc-va-chong-bui-tren-smart-phone-771130" target="_blank" title="chống nước và bụi" type="chống nước và bụi">chống nước v&agrave; bụi</a> chuẩn IP68 cho ph&eacute;p ng&acirc;m điện thoại dưới nước ở độ s&acirc;u 1.5 m&eacute;t trong 30 ph&uacute;t.</p>

<p><a class="preventdefault" href="https://www.thegioididong.com/images/42/88537/samsung-galaxy-a7-2017-8-1.jpg" onclick="return false;"><img alt="Chuẩn chống nước IP68" class="lazy" src="https://cdn4.tgdd.vn/Products/Images/42/88537/samsung-galaxy-a7-2017-8-1.jpg" style="display:block" title="Chuẩn chống nước IP68" /></a><br />
&nbsp;<br />
<strong>Camera khẩu độ f1.9</strong></p>

<p>Camera sau xuất hiện một điều đặc biệt l&agrave; n&oacute; kh&ocirc;ng bị lồi l&ecirc;n so với mặt lưng, một cải tiến cực k&igrave; gi&aacute; trị, vừa tạo t&iacute;nh thẩm mỹ m&agrave; vừa bảo vệ tốt mặt k&iacute;nh camera.</p>

<p><a class="preventdefault" href="https://www.thegioididong.com/images/42/88537/samsung-galaxy-a7-20173.jpg" onclick="return false;"><img alt="Camera không lồi" class="lazy" src="https://cdn1.tgdd.vn/Products/Images/42/88537/samsung-galaxy-a7-20173.jpg" style="display:block" title="Camera không lồi" /></a></p>

<p>Bộ đ&ocirc;i camera của A7 (2017) thực sự nổi bật với c&ugrave;ng một độ ph&acirc;n giải 16 MP v&agrave; khẩu độ f1.9 ở camera trước v&agrave; sau.</p>

<p><a class="preventdefault" href="https://www.thegioididong.com/images/42/88537/samsung-galaxy-a7-2017-10-1.jpg" onclick="return false;"><img alt="Camera với nhiều tính năng tuyệt vời" class="lazy" src="https://cdn3.tgdd.vn/Products/Images/42/88537/samsung-galaxy-a7-2017-10-1.jpg" style="display:block" title="Camera với nhiều tính năng tuyệt vời" /></a><br />
&nbsp;<br />
Bộ đ&ocirc;i camera n&agrave;y được trang bị nhiều t&iacute;nh năng th&uacute; vị như: tự động lấy n&eacute;t, gắn thẻ địa lý, chạm lấy n&eacute;t... cho bạn thỏa sức lưu giữ những khoảnh khắc đẹp theo nhiều phong c&aacute;ch kh&aacute;c nhau.</p>

<p><br />
Galaxy A7 (2017) hứa hẹn sẽ l&agrave; một chiếc m&aacute;y ấn tượng nhiều chức năng mới, cấu h&igrave;nh mạnh mẽ sẽ v&agrave; lu&ocirc;n l&agrave; sự lựa chọn tốt cho bạn ở ph&acirc;n kh&uacute;c tầm trung.</p>
', N'/UploadFile/images/SamSung/SonySSA52017B8_lon150217110440.png', 11, 0, NULL, NULL, NULL, 1, 1, 4, 1, 0, N'["/UploadFile/images/SamSung/SonySSA52017B7_lon150217110446.png","/UploadFile/images/SamSung/SonySSA52017B3_lon.png","/UploadFile/images/SamSung/SonySSA52017B6_lon150217110452.png","/UploadFile/images/SamSung/SonySSA52017B_lon.png","/UploadFile/images/SamSung/SonySSA52017B2_lon.png"]', 1)
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [DonGia], [NgayCapNhap], [CauHinh], [MoTa], [HinhAnh], [SoLuongTon], [LuotXem], [LuotBinhChon], [LuotBinhLuan], [SoLanMua], [Moi], [MaNCC], [MaNSX], [MaLoaiSP], [DaXoa], [HinhAnh1], [Home]) VALUES (15, N'Samsung Galaxy S8', CAST(18490000 AS Decimal(18, 0)), CAST(0x0000A7730015FAB4 AS DateTime), N'<div class="detail-col-left53 left">
<h3>Th&ocirc;ng số kỹ thuật</h3>

<ul>
	<li>Th&ocirc;ng số cơ bản</li>
	<li>M&agrave;n h&igrave;nh : 5.8 inch Super AMOLED (2560 x 1440 pixel)</li>
	<li>Camera : Ch&iacute;nh: 12.0 MP, Phụ: 8.0 MP</li>
	<li>RAM : 4 GB</li>
	<li>Bộ nhớ trong : 64 GB</li>
	<li>Hệ điều h&agrave;nh : Android 7.0 (Nougat)</li>
	<li>CPU : L&otilde;i T&aacute;m (l&otilde;i Tứ 2.3GHz + l&otilde;i Tứ 1.7GHz), 64 bit, vi xử l&yacute; 10nm</li>
	<li>K&iacute;ch thước : 148.9 x 68.1 x 8.0mm</li>
</ul>

<ul>
	<li>M&agrave;n h&igrave;nh</li>
	<li>C&ocirc;ng nghệ m&agrave;n h&igrave;nh : Super AMOLED</li>
	<li>M&agrave;u m&agrave;n h&igrave;nh : 16 Triệu m&agrave;u</li>
	<li>Chuẩn m&agrave;n h&igrave;nh : 2k</li>
	<li>Độ ph&acirc;n giải m&agrave;n h&igrave;nh : 2560 x 1440 pixels</li>
	<li>C&ocirc;ng nghệ cảm ứng : Đa điểm</li>
	<li>Mặt k&iacute;nh m&agrave;n h&igrave;nh : Corning Gorilla Glass 5</li>
	<li>Camera Sau</li>
	<li>Độ ph&acirc;n giải : 12.0 MP</li>
	<li>Quay phim : Quay phim 4K 2160p@60fps</li>
	<li>Đ&egrave;n Flash : C&oacute;</li>
	<li>Chụp ảnh n&acirc;ng cao : Ảnh Raw, Lấy n&eacute;t theo pha (EIS), Tự động lấy n&eacute;t, Chạm lấy n&eacute;t, Nhận diện khu&ocirc;n mặt, HDR, Panorama, Chống rung quang học (OIS), Chế độ chụp chuy&ecirc;n nghiệp</li>
	<li>Camera Trước</li>
	<li>Video Call : C&oacute;</li>
	<li>Độ ph&acirc;n giải : 8.0 MP</li>
	<li>Th&ocirc;ng tin kh&aacute;c : Selfie bằng cử chỉ, Chụp bằng giọng n&oacute;i, Nhận diện khu&ocirc;n mặt, Chế độ l&agrave;m đẹp, Quay video Full HD, Tự động lấy n&eacute;t, Selfie ngược s&aacute;ng HDR</li>
	<li>Cấu h&igrave;nh phần cứng</li>
	<li>Tốc độ CPU : 4 nh&acirc;n 2.3GHz + 4 nh&acirc;n 1.7GHz</li>
	<li>Số nh&acirc;n : 8 nh&acirc;n</li>
	<li>RAM : 4 GB</li>
	<li>Cảm biến : Qu&eacute;t v&otilde;ng mạc, v&acirc;n tay (gắn ph&iacute;a sau), gia tốc, con quay hồi chuyển, khoảng c&aacute;ch, la b&agrave;n, đo &aacute;p suất kh&iacute; quyển, nhịp tim, SpO2</li>
	<li>Bộ nhớ &amp; Lưu trữ</li>
	<li>Danh bạ lưu trữ : Kh&ocirc;ng giới hạn</li>
	<li>Bộ nhớ trong : 64 GB</li>
	<li>Bộ nhớ c&ograve;n lại : Đang cập nhật</li>
	<li>Thẻ nhớ ngo&agrave;i : MicroSD</li>
	<li>Hỗ trợ thẻ nhớ tối đa : 256 GB</li>
	<li>Thiết kế &amp; Trọng lượng</li>
	<li>Kiểu d&aacute;ng : Thanh (thẳng) + Cảm ứng</li>
	<li>Chất liệu : Viền kim loại nguy&ecirc;n khối</li>
	<li>K&iacute;ch thước : 148.9 x 68.1 x 8.0mm</li>
	<li>Trọng lượng : 155 g</li>
	<li>Khả năng chống nước : IP68 - bụi / chống nước hơn 1,5 m&eacute;t v&agrave; 30 ph&uacute;t</li>
	<li>Th&ocirc;ng tin pin</li>
	<li>Loại pin : Li-Ion</li>
	<li>Dung lượng pin : 3000 mAh</li>
	<li>Pin c&oacute; thể th&aacute;o rời : Kh&ocirc;ng</li>
	<li>Chế độ sạc nhanh : C&oacute;</li>
	<li>Kết nối &amp; Cổng giao tiếp</li>
	<li>Băng tần 4G : C&oacute;</li>
	<li>Hỗ trợ SIM : Dual SIM</li>
	<li>Khe cắm sim : Nano-SIM</li>
	<li>Wifi : Wi-Fi 802.11 a/b/g/n/ac</li>
	<li>GPS : A-GPS, GLONASS, BDS</li>
	<li>Bluetooth : v5.0</li>
	<li>GPRS/EDGE : C&oacute;</li>
	<li>NFC : C&oacute;</li>
	<li>Kết nối USB : USB Type-C</li>
	<li>Cổng kết nối kh&aacute;c : Hỗ trợ OTG</li>
	<li>Cổng sạc : USB Type-C</li>
	<li>Jack (Input &amp; Output) : 3.5 mm</li>
	<li>Giải tr&iacute; &amp; Ứng dụng</li>
	<li>Xem phim : MP4/DivX/XviD/H.265 player</li>
	<li>Nghe nhạc : MP3/WAV/eAAC+/Flac player</li>
	<li>Ghi &acirc;m : C&oacute;</li>
	<li>FM radio : C&oacute;</li>
	<li>Đ&egrave;n pin : C&oacute;</li>
	<li>Chức năng kh&aacute;c : Photo/video editor, Document viewer</li>
	<li>Bảo h&agrave;nh</li>
	<li>Thời gian bảo h&agrave;nh : 12 th&aacute;ng</li>
</ul>
</div>
', N'<h2><strong>Galaxy S8 l&agrave; si&ecirc;u phẩm mới nhất được <a href="https://www.thegioididong.com/dtdd-samsung" target="_blank" title="Samsung" type="Samsung">Samsung</a> ra mắt v&agrave;o ng&agrave;y 29/3 với thiết kế nhỏ gọn ho&agrave;n to&agrave;n mới, m&agrave;n h&igrave;nh v&ocirc; cực viền si&ecirc;u mỏng.</strong></h2>

<p style="text-align:center"><a class="preventdefault" href="https://cdn.tgdd.vn/Files/2017/04/09/970347/img_1680_1280x719-800-resize.jpg" onclick="return false;"><img alt="Trên tay Galaxy S8" class="lazy" src="https://cdn.tgdd.vn/Files/2017/04/09/970347/img_1680_1280x719-800-resize.jpg" style="display:block" title="Trên tay Galaxy S8" /></a></p>

<p>Lần đầu ti&ecirc;n cầm chiếc m&aacute;y tr&ecirc;n tay, cảm gi&aacute;c kh&ocirc;ng c&ograve;n l&agrave; chiếc smartphone truyền thống nữa.</p>

<p>M&aacute;y d&agrave;i v&agrave; ốm, gọn trong l&ograve;ng b&agrave;n tay khiến m&igrave;nh nhớ đến thời LG c&oacute; chiếc Chocolate d&agrave;i d&agrave;i v&agrave; thon gọn kiểu n&agrave;y, tất nhi&ecirc;n Galaxy S8 cao cấp hơn nhiều.</p>

<p style="text-align:center"><a class="preventdefault" href="https://cdn.tgdd.vn/Files/2017/04/09/970347/img_1692_1280x719-800-resize.jpg" onclick="return false;"><img alt="Trên tay Galaxy S8" class="lazy" src="https://cdn2.tgdd.vn/Files/2017/04/09/970347/img_1692_1280x719-800-resize.jpg" style="display:block" title="Trên tay Galaxy S8" /></a></p>

<p>Ph&iacute;a trước l&agrave; m&agrave;n h&igrave;nh 5.8 inch độ ph&acirc;n giải QHD+ 2960 x 1440 pixels, tỉ lệ 18,5:9 c&ugrave;ng m&agrave;n h&igrave;nh cạnh căng tr&agrave;n. Sử dụng c&ocirc;ng nghệ Super AMOLED cho khả năng hiển thị tươi tắn, độ s&aacute;ng tuyệt vời.</p>

<p>Quả kh&ocirc;ng ngoa khi Samsung gọi đ&acirc;y l&agrave; Infinity Display (M&agrave;n h&igrave;nh v&ocirc; cực).</p>

<p style="text-align:center"><a class="preventdefault" href="https://cdn.tgdd.vn/Files/2017/04/09/970347/img_1720_1280x720-800-resize.jpg" onclick="return false;"><img alt="Trên tay Galaxy S8" class="lazy" src="https://cdn4.tgdd.vn/Files/2017/04/09/970347/img_1720_1280x720-800-resize.jpg" style="display:block" title="Trên tay Galaxy S8" /></a></p>

<p>Tr&ecirc;n Galaxy S8, Samsung đ&atilde; quyết định mang ph&iacute;m điều hướng v&agrave;o trong m&agrave;n h&igrave;nh, k&egrave;m theo đ&oacute; l&agrave; n&uacute;t home hỗ trợ cảm ứng lực.</p>

<p style="text-align:center"><a class="preventdefault" href="https://cdn.tgdd.vn/Files/2017/04/09/970347/img_1695_1280x720-800-resize.jpg" onclick="return false;"><img alt="trên tay nhanh Galaxy S8" class="lazy" src="https://cdn1.tgdd.vn/Files/2017/04/09/970347/img_1695_1280x720-800-resize.jpg" style="display:block" title="trên tay nhanh Galaxy S8" /></a></p>

<p>Phần tr&ecirc;n l&agrave; camera phụ 8 MP hỗ trợ t&iacute;nh năng lấy n&eacute;t tự động đầu ti&ecirc;n tr&ecirc;n thế giới, c&aacute;c cảm biến, loa thoại v&agrave; quan trọng nhất l&agrave; cảm biến qu&eacute;t mống mắt. M&igrave;nh c&oacute; sử dụng thử th&igrave; m&aacute;y nhận dạng v&agrave; mở kh&oacute;a cực k&igrave; nhanh.</p>

<p style="text-align:center"><a class="preventdefault" href="https://cdn.tgdd.vn/Files/2017/04/09/970347/img_1732_1280x720-800-resize.jpg" onclick="return false;"><img alt="trên tay nhanh Galaxy S8" class="lazy" src="https://cdn3.tgdd.vn/Files/2017/04/09/970347/img_1732_1280x720-800-resize.jpg" style="display:block" title="trên tay nhanh Galaxy S8" /></a></p>

<p>Khung kim loại được l&agrave;m d&agrave;y hơn, cho cảm gi&aacute;c tựa ng&oacute;n tay chắc chắn hơn Galaxy S7 Edge rất nhiều.&nbsp;</p>

<p style="text-align:center"><a class="preventdefault" href="https://cdn.tgdd.vn/Files/2017/04/09/970347/img_1693_1280x720-800-resize.jpg" onclick="return false;"><img alt="Trên tay Galaxy S8" class="lazy" src="https://cdn.tgdd.vn/Files/2017/04/09/970347/img_1693_1280x720-800-resize.jpg" style="display:block" title="Trên tay Galaxy S8" /></a></p>

<p>M&igrave;nh c&oacute; mang theo một chiếc Galaxy S7 Edge để cho c&aacute;c bạn tiện theo d&otilde;i những thay đổi tr&ecirc;n S8. Đầu ti&ecirc;n, m&agrave;n h&igrave;nh tăng l&ecirc;n nhưng k&iacute;ch thước hầu như kh&ocirc;ng đổi, thật tuyệt vời.</p>

<p style="text-align:center"><a class="preventdefault" href="https://cdn.tgdd.vn/Files/2017/04/09/970347/img_1722_1280x719-800-resize.jpg" onclick="return false;"><img alt="trên tay nhanh Galaxy S8" class="lazy" src="https://cdn2.tgdd.vn/Files/2017/04/09/970347/img_1722_1280x719-800-resize.jpg" style="display:block" title="trên tay nhanh Galaxy S8" /></a></p>

<p>C&aacute;c vị tr&iacute; đặt ph&iacute;m nguồn, volume, cổng sạc đều kh&aacute; giống nhau. Ri&ecirc;ng tr&ecirc;n Galaxy S8 trang bị cổng USB Type-C, đồng thời xuất hiện th&ecirc;m ph&iacute;m tắt k&iacute;ch hoạt trợ l&yacute; ảo Bixby.</p>

<p>Mặt lưng của Galaxy S8 vẫn l&agrave;m bằng k&iacute;nh bo cong mềm mại, bảo vệ bởi cường lực Gorilla Glass 5.</p>

<p style="text-align:center"><a class="preventdefault" href="https://cdn.tgdd.vn/Files/2017/04/09/970347/img_1696_1280x720-800-resize.jpg" onclick="return false;"><img alt="trên tay nhanh Galaxy S8" class="lazy" src="https://cdn4.tgdd.vn/Files/2017/04/09/970347/img_1696_1280x720-800-resize.jpg" style="display:block" title="trên tay nhanh Galaxy S8" /></a></p>

<p>Vị tr&iacute; ph&iacute;a tr&ecirc;n gồm camera ch&iacute;nh 12 MP, khẩu độ F/1.7 c&ugrave;ng c&ocirc;ng nghệ Dual Pixel, đ&egrave;n flash LED v&agrave; cảm biến v&acirc;n tay.</p>

<p>Tuy nhi&ecirc;n nh&igrave;n kĩ sẽ thấy Samsung chăm ch&uacute;t camera hơn với chi tiết v&ograve;ng tr&ograve;n nhỏ bao quanh.</p>

<p style="text-align:center"><a class="preventdefault" href="https://cdn.tgdd.vn/Files/2017/04/09/970347/img_1697_1280x719-800-resize.jpg" onclick="return false;"><img alt="trên tay nhanh Galaxy S8" class="lazy" src="https://cdn1.tgdd.vn/Files/2017/04/09/970347/img_1697_1280x719-800-resize.jpg" style="display:block" title="trên tay nhanh Galaxy S8" /></a></p>

<p>C&ograve;n về chất lượng ảnh v&agrave; t&iacute;nh năng, m&igrave;nh sẽ c&oacute; b&agrave;i đ&aacute;nh gi&aacute; khi sử dụng m&aacute;y l&acirc;u hơn.</p>

<p style="text-align:center"><a class="preventdefault" href="https://cdn.tgdd.vn/Files/2017/04/09/970347/img_1686_1280x720-800-resize.jpg" onclick="return false;"><img alt="trên tay nhanh Galaxy S8" class="lazy" src="https://cdn3.tgdd.vn/Files/2017/04/09/970347/img_1686_1280x720-800-resize.jpg" style="display:block" title="trên tay nhanh Galaxy S8" /></a></p>

<p>Về cấu h&igrave;nh, chiếc Galaxy S8 chạy con chip Exynos 8895 c&ugrave;ng 4 GB RAM, 64 GB bộ nhớ trong cho trải nghiệm mượt m&agrave; v&agrave; đủ sức chiến tất cả game 3D nặng.</p>

<p style="text-align:center"><a class="preventdefault" href="https://cdn.tgdd.vn/Files/2017/04/09/970347/img_1715_1280x719-800-resize.jpg" onclick="return false;"><img alt="Trên tay nhanh Galaxy S8" class="lazy" src="https://cdn.tgdd.vn/Files/2017/04/09/970347/img_1715_1280x719-800-resize.jpg" style="display:block" title="Trên tay nhanh Galaxy S8" /></a></p>

<p>M&aacute;y chạy tr&ecirc;n nền Android 7.0.1 Nougat với giao diện tuỳ biến Grace UX 8.1, vi&ecirc;n pin đi k&egrave;m 3.000 mAh được t&iacute;ch hợp c&ocirc;ng nghệ sạc nhanh Fast Charge.</p>

<p style="text-align:center"><a class="preventdefault" href="https://cdn.tgdd.vn/Files/2017/04/09/970347/img_1716_1280x720-800-resize.jpg" onclick="return false;"><img alt="Trên tay nhanh Galaxy S8" class="lazy" src="https://cdn2.tgdd.vn/Files/2017/04/09/970347/img_1716_1280x720-800-resize.jpg" style="display:block" title="Trên tay nhanh Galaxy S8" /></a></p>

<p>Điểm nhấn tr&ecirc;n&nbsp;<a href="https://www.thegioididong.com/dtdd/samsung-galaxy-s8" target="_blank" title="Galaxy S8" type="Galaxy S8">Galaxy S8</a>&nbsp;c&ograve;n c&oacute; t&iacute;nh năng trợ l&yacute; ảo Bixby, tuy nhi&ecirc;n trợ l&yacute; ảo n&agrave;y chưa hỗ trợ tiếng việt.</p>

<p style="text-align:center"><a class="preventdefault" href="https://cdn.tgdd.vn/Files/2017/04/09/970347/img_1723_1280x720-800-resize.jpg" onclick="return false;"><img alt="Trên tay nhanh Galaxy S8" class="lazy" src="https://cdn4.tgdd.vn/Files/2017/04/09/970347/img_1723_1280x720-800-resize.jpg" style="display:block" title="Trên tay nhanh Galaxy S8" /></a></p>

<p>Với nhiều ưu điểm như m&agrave;n h&igrave;nh v&ocirc; cực viền si&ecirc;u mỏng, thiết kế định h&igrave;nh smartphone trong tương lai, cấu h&igrave;nh cực đỉnh v&agrave; camera chụp đ&ecirc;m rất tốt, Galaxy S8 sẽ l&agrave; một si&ecirc;u phẩm l&agrave;m vừa l&ograve;ng rất nhiều người d&ugrave;ng.</p>
', N'/UploadFile/images/SamSung/SonySSS8B10_lon.png', 1, 0, NULL, NULL, NULL, 1, 1, 4, 1, 0, N'["/UploadFile/images/SamSung/SonySSS8B1_lon.png","/UploadFile/images/SamSung/SonySSS8B2_lon.png","/UploadFile/images/SamSung/SonySSS8B3_lon.png","/UploadFile/images/SamSung/SonySSS8B4_lon.png","/UploadFile/images/SamSung/SonySSS8B5_lon.png","/UploadFile/images/SamSung/SonySSS8B6_lon.png","/UploadFile/images/SamSung/SonySSS8B7_lon.png","/UploadFile/images/SamSung/SonySSS8B8_lon.png","/UploadFile/images/SamSung/SonySSS8B9_lon.png","/UploadFile/images/SamSung/SonySSS8B_lon.png"]', 1)
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [DonGia], [NgayCapNhap], [CauHinh], [MoTa], [HinhAnh], [SoLuongTon], [LuotXem], [LuotBinhChon], [LuotBinhLuan], [SoLanMua], [Moi], [MaNCC], [MaNSX], [MaLoaiSP], [DaXoa], [HinhAnh1], [Home]) VALUES (16, N'Samsung Galaxy S8 Plus', CAST(20490000 AS Decimal(18, 0)), CAST(0x0000A7730015BB8C AS DateTime), N'<div class="detail-col-left53 left">
<h3>Th&ocirc;ng số kỹ thuật</h3>

<ul>
	<li>Th&ocirc;ng số cơ bản</li>
	<li>M&agrave;n h&igrave;nh : 6.2 inch Super AMOLED (2560 x 1440 pixel)</li>
	<li>Camera : Ch&iacute;nh: 12.0 MP, Phụ: 8.0 MP</li>
	<li>RAM : 4 GB</li>
	<li>Bộ nhớ trong : 64 GB</li>
	<li>Hệ điều h&agrave;nh : Android 7.0 (Nougat)</li>
	<li>CPU : L&otilde;i T&aacute;m (l&otilde;i Tứ 2.3GHz + l&otilde;i Tứ 1.7GHz), 64 bit, vi xử l&yacute; 10nm</li>
	<li>K&iacute;ch thước : 159.5 x 73.4 x 8.1mm</li>
</ul>

<ul>
	<li>M&agrave;n h&igrave;nh</li>
	<li>C&ocirc;ng nghệ m&agrave;n h&igrave;nh : Super AMOLED</li>
	<li>M&agrave;u m&agrave;n h&igrave;nh : 16 Triệu m&agrave;u</li>
	<li>Chuẩn m&agrave;n h&igrave;nh : 2k</li>
	<li>Độ ph&acirc;n giải m&agrave;n h&igrave;nh : 2560 x 1440 pixels</li>
	<li>C&ocirc;ng nghệ cảm ứng : Đa điểm</li>
	<li>Mặt k&iacute;nh m&agrave;n h&igrave;nh : Corning Gorilla Glass 5</li>
	<li>Camera Sau</li>
	<li>Độ ph&acirc;n giải : Dual 12.0 MP</li>
	<li>Quay phim : Quay phim 4K 2160p@60fps</li>
	<li>Đ&egrave;n Flash : C&oacute;</li>
	<li>Chụp ảnh n&acirc;ng cao : Chụp trước lấy n&eacute;t sau, Chụp ảnh x&oacute;a ph&ocirc;ng, Chụp phơi s&aacute;ng, Tự động lấy n&eacute;t, Chạm lấy n&eacute;t, Nhận diện khu&ocirc;n mặt, HDR, Panorama, Chống rung quang học (OIS), Chế độ chụp chuy&ecirc;n nghiệp</li>
	<li>Camera Trước</li>
	<li>Video Call : C&oacute;</li>
	<li>Độ ph&acirc;n giải : 8.0 MP</li>
	<li>Cấu h&igrave;nh phần cứng</li>
	<li>Số nh&acirc;n : 8 nh&acirc;n</li>
	<li>RAM : 4 GB</li>
	<li>Cảm biến : Qu&eacute;t v&otilde;ng mạc, v&acirc;n tay (gắn ph&iacute;a sau), gia tốc, con quay hồi chuyển, khoảng c&aacute;ch, la b&agrave;n, đo &aacute;p suất kh&iacute; quyển, nhịp tim, SpO2</li>
	<li>Bộ nhớ &amp; Lưu trữ</li>
	<li>Danh bạ lưu trữ : Kh&ocirc;ng giới hạn</li>
	<li>Bộ nhớ trong : 64 GB</li>
	<li>Thẻ nhớ ngo&agrave;i : MicroSD</li>
	<li>Hỗ trợ thẻ nhớ tối đa : 256 GB</li>
	<li>Thiết kế &amp; Trọng lượng</li>
	<li>Chất liệu : Nguy&ecirc;n khối viền kim loại</li>
	<li>Kiểu d&aacute;ng : Thanh (thẳng) + Cảm ứng</li>
	<li>K&iacute;ch thước : 159.5 x 73.4 x 8.1mm</li>
	<li>Trọng lượng : 173 g</li>
	<li>Khả năng chống nước : IP68 - bụi / chống nước hơn 1,5 m&eacute;t v&agrave; 30 ph&uacute;t</li>
	<li>Th&ocirc;ng tin pin</li>
	<li>Loại pin : Li-Ion</li>
	<li>Dung lượng pin : 3500 mAh</li>
	<li>Pin c&oacute; thể th&aacute;o rời : Kh&ocirc;ng</li>
	<li>Chế độ sạc nhanh : C&oacute;</li>
	<li>Kết nối &amp; Cổng giao tiếp</li>
	<li>Băng tần 4G : C&oacute;</li>
	<li>Hỗ trợ SIM : Dual SIM</li>
	<li>Khe cắm sim : Nano-SIM</li>
	<li>Wifi : Wi-Fi 802.11 a/b/g/n/ac</li>
	<li>GPS : A-GPS, GLONASS, BDS</li>
	<li>Bluetooth : v5.0</li>
	<li>NFC : C&oacute;</li>
	<li>Kết nối USB : USB Type-C</li>
	<li>Cổng sạc : USB Type-C</li>
	<li>Jack (Input &amp; Output) : 3.5 mm</li>
	<li>Giải tr&iacute; &amp; Ứng dụng</li>
	<li>Xem phim : MP4/DivX/XviD/H.265 player</li>
	<li>Nghe nhạc : MP3/WAV/eAAC+/Flac player</li>
	<li>Ghi &acirc;m : C&oacute;</li>
	<li>FM radio : C&oacute;</li>
	<li>Đ&egrave;n pin : C&oacute;</li>
	<li>Chức năng kh&aacute;c : Photo/video editor, Document viewer</li>
	<li>Bảo h&agrave;nh</li>
	<li>Thời gian bảo h&agrave;nh : 12 th&aacute;ng</li>
</ul>
</div>
', N'<h2><strong>Galaxy S8 v&agrave; S8 Plus hiện đang l&agrave; chuẩn mực smartphone về thiết kế trong tương lai. Sau nhiều ng&agrave;y được sử dụng, m&igrave;nh xin chia sẻ những cảm nhận chi tiết nhất về chiếc&nbsp;<a href="https://www.thegioididong.com/dtdd/samsung-galaxy-s8-plus" target="_blank" title="Galaxy S8 Plus" type="Galaxy S8 Plus">Galaxy S8 Plus</a>&nbsp;- thiết bị đang c&oacute; doanh số đặt h&agrave;ng khủng nhất hiện tại.</strong></h2>

<p style="text-align:center"><a class="preventdefault" href="https://cdn.tgdd.vn/Files/2017/05/01/978135/galaxys8plus_1280x720-800-resize.jpg" onclick="return false;"><img alt="Đánh giá chi tiết Galaxy S8 Plus" class="lazy" src="https://cdn4.tgdd.vn/Files/2017/05/01/978135/galaxys8plus_1280x720-800-resize.jpg" style="display:block" title="Đánh giá chi tiết Galaxy S8 Plus" /></a></p>

<h3><strong>Trải nghiệm m&agrave;n h&igrave;nh lớn, chứ kh&ocirc;ng phải l&agrave; một thiết bị k&iacute;ch thước lớn</strong></h3>

<p>Năm nay, c&aacute;c nh&agrave; sản xuất H&agrave;n Quốc như LG v&agrave; Samsung đ&atilde; đi ti&ecirc;n phong với m&agrave;n h&igrave;nh tỉ lệ 18:9 hay 18,5:9, tối ưu hết mức viền m&agrave;n h&igrave;nh. V&agrave; Galaxy S8 Plus cũng vậy, người d&ugrave;ng sẽ c&oacute; một trải nghiệm m&agrave;n h&igrave;nh lớn chứ kh&ocirc;ng phải l&agrave; một thiết bị k&iacute;ch thước lớn.&nbsp;</p>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/91131/samsung-galaxy-s8-plus-man-hinh.jpg" onclick="return false;"><img alt="Màn hình" class="lazy" src="https://cdn1.tgdd.vn/Products/Images/42/91131/samsung-galaxy-s8-plus-man-hinh.jpg" style="display:block" title="Màn hình" /></a></p>

<p>Kh&ocirc;ng những mặt trước bo cong m&agrave; mặt k&iacute;nh sau cũng mềm mại, &ocirc;m gọn trong b&agrave;n tay. Cảm gi&aacute;c cầm nắm S8 Plus thật sự rất dễ chịu, dường như kh&ocirc;ng thấy ch&uacute;t cấn n&agrave;o ngay cả với chi tiết nhỏ nhất. Tuy nhi&ecirc;n, k&iacute;ch thước 6.2 inch khiến cho việc d&ugrave;ng v&acirc;n tay của m&igrave;nh kh&aacute; l&agrave; kh&oacute; khăn.</p>

<p style="text-align:center"><a class="preventdefault" href="https://cdn.tgdd.vn/Files/2017/05/01/978135/img_0203_1280x720-800-resize.jpg" onclick="return false;"><img alt="Đánh giá chi tiết Galaxy S8 plus" class="lazy" src="https://cdn3.tgdd.vn/Files/2017/05/01/978135/img_0203_1280x720-800-resize.jpg" style="display:block" title="Đánh giá chi tiết Galaxy S8 plus" /></a></p>

<p>Trong qu&aacute; tr&igrave;nh sử dụng, đ&ocirc;i khi m&igrave;nh hay ấn nhầm n&uacute;t volume v&agrave; Bixby trong những ng&agrave;y đầu, do Samsung thiết kế vị tr&iacute; kh&aacute; gần nhau.</p>

<p style="text-align:center"><a class="preventdefault" href="https://cdn.tgdd.vn/Files/2017/05/01/978135/img_1839_1280x720-800-resize.jpg" onclick="return false;"><img alt="Đánh giá chi tiết Galaxy S8 Plus" class="lazy" src="https://cdn.tgdd.vn/Files/2017/05/01/978135/img_1839_1280x720-800-resize.jpg" style="display:block" title="Đánh giá chi tiết Galaxy S8 Plus" /></a></p>

<p><strong>M&agrave;n h&igrave;nh v&ocirc; cực, trải nghiệm thật đ&atilde;</strong></p>

<p>Galaxy S8 Plus được trang bị m&agrave;n h&igrave;nh 6.2 inch độ ph&acirc;n giải 2K+, tỉ lệ 18,5:9 tr&ecirc;n tấm nền Super AMOLED. Với nhiều c&ocirc;ng nghệ đi k&egrave;m đặc biệt l&agrave; HDR10 khiến cho g&oacute;c nh&igrave;n rộng, độ s&aacute;ng cao, m&agrave;u đen s&acirc;u c&ugrave;ng tương phản m&agrave;u sắc đỉnh, kh&ocirc;ng c&oacute; g&igrave; đề ph&agrave;n n&agrave;n về m&agrave;n h&igrave;nh tr&ecirc;n S8 Plus.</p>

<p style="text-align:center"><a class="preventdefault" href="https://cdn.tgdd.vn/Files/2017/05/01/978135/img_0206_1280x720-800-resize.jpg" onclick="return false;"><img alt="Đánh giá chi tiết Galaxy S8 plus" class="lazy" src="https://cdn2.tgdd.vn/Files/2017/05/01/978135/img_0206_1280x720-800-resize.jpg" style="display:block" title="Đánh giá chi tiết Galaxy S8 plus" /></a></p>

<p>Việc thay thế n&uacute;t home vật l&yacute; bằng ph&iacute;m điều hướng m&agrave;n h&igrave;nh tr&ecirc;n S8 Plus m&igrave;nh thấy b&igrave;nh thường kh&ocirc;ng g&acirc;y kh&oacute; &nbsp;khăn g&igrave; trong qu&aacute; tr&igrave;nh sử dụng.</p>

<p style="text-align:center"><a class="preventdefault" href="https://cdn.tgdd.vn/Files/2017/05/01/978135/img_0207_1280x720-800-resize.jpg" onclick="return false;"><img alt="Đánh giá chi tiết Galaxy S8 plus" class="lazy" src="https://cdn4.tgdd.vn/Files/2017/05/01/978135/img_0207_1280x720-800-resize.jpg" style="display:block" title="Đánh giá chi tiết Galaxy S8 plus" /></a></p>

<p>Nếu ngại m&agrave;n h&igrave;nh to kh&oacute; thao t&aacute;c ngo&agrave;i đường, kh&ocirc;ng sao, Galaxy S8 Plus vẫn hỗ trợ t&iacute;nh năng m&agrave;n h&igrave;nh nhỏ để thao t&aacute;c một tay.</p>

<p style="text-align:center"><a class="preventdefault" href="https://cdn.tgdd.vn/Files/2017/05/01/978135/img_0214_1280x720-800-resize.jpg" onclick="return false;"><img alt="Đánh giá chi tiết Galaxy S8 plus" class="lazy" src="https://cdn1.tgdd.vn/Files/2017/05/01/978135/img_0214_1280x720-800-resize.jpg" style="display:block" title="Đánh giá chi tiết Galaxy S8 plus" /></a></p>

<p>Ngo&agrave;i ra, Samsung cũng cho ph&eacute;p t&ugrave;y biến thanh điều hướng với việc thay đổi vị tr&iacute;, m&agrave;u sắc cũng như tốc độ phản hồi n&uacute;t home.</p>

<h3><strong>Camera cải thiện, nhưng Samsung kh&ocirc;ng hề n&oacute;i ra</strong></h3>

<p>Nếu nh&igrave;n sơ qua, Galaxy S8 hay S8 Plus đều kh&ocirc;ng qu&aacute; kh&aacute; biệt tiền nhiệm S7. Vẫn l&agrave; độ ph&acirc;n giải 12 MP, khẩu độ F/1.7, c&ocirc;ng nghệ lấy n&eacute;t Dual-pixel qu&aacute; nhanh qu&aacute; nguy hiểm c&ugrave;ng k&iacute;ch thước cảm biến lớn 1.4 micromet.</p>

<p style="text-align:center"><a class="preventdefault" href="https://cdn.tgdd.vn/Files/2017/05/01/978135/img_0218_1280x720-800-resize.jpg" onclick="return false;"><img alt="Đánh giá chi tiết Galaxy S8 plus" class="lazy" src="https://cdn3.tgdd.vn/Files/2017/05/01/978135/img_0218_1280x720-800-resize.jpg" style="display:block" title="Đánh giá chi tiết Galaxy S8 plus" /></a></p>

<p>Tuy nhi&ecirc;n, sự n&acirc;ng cấp của Samsung kh&aacute; &acirc;m thầm v&agrave; chủ yếu l&agrave; chất lượng thấu k&iacute;nh. M&igrave;nh rất h&agrave;i l&ograve;ng về camera tr&ecirc;n Galaxy S8 Plus, với m&igrave;nh n&oacute; xứng đ&aacute;ng l&agrave; một chiếc camera-phone auto xếp h&agrave;ng top cao.</p>

<p>C&aacute;c bạn c&oacute; thể xem một v&agrave;i h&igrave;nh ảnh m&igrave;nh chụp từ Galaxy S8 Plus:</p>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/91131/samsung-galaxy-s8-plus2-1.jpg" onclick="return false;"><img alt="Ảnh chụp cận cảnh trong nhà" class="lazy" src="https://cdn.tgdd.vn/Products/Images/42/91131/samsung-galaxy-s8-plus2-1.jpg" style="display:block" title="Ảnh chụp cận cảnh trong nhà" /></a><br />
<em>Ảnh cận cảnh trong ph&ograve;ng</em></p>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/91131/samsung-galaxy-s8-plus3-1.jpg" onclick="return false;"><img alt="Màu sắc gần sát với thực tế" class="lazy" src="https://cdn2.tgdd.vn/Products/Images/42/91131/samsung-galaxy-s8-plus3-1.jpg" style="display:block" title="Màu sắc gần sát với thực tế" /></a><br />
<em>M&agrave;u sắc gần s&aacute;t với thực tế</em></p>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/91131/samsung-galaxy-s8-plus1-2.jpg" onclick="return false;"><img alt="Ảnh chụp thiếu sáng" class="lazy" src="https://cdn4.tgdd.vn/Products/Images/42/91131/samsung-galaxy-s8-plus1-2.jpg" style="display:block" title="Ảnh chụp thiếu sáng" /></a></p>

<p>Thuật to&aacute;n xử l&yacute; trong nhiều t&igrave;nh huống đều tương đồng người tiền nhiệm Galaxy S7, tuy nhi&ecirc;n sự kh&aacute;c nhau đến từ chế độ HDR mang lại một bức ảnh c&oacute; dải nhạy s&aacute;ng tuyệt vời. Chưa kể việc flare t&iacute;m hay thấy khi chụp c&oacute; tia đ&egrave;n tr&ecirc;n camera Galaxy S7 hay d&ograve;ng A 2017, đ&atilde; ho&agrave;n to&agrave;n được khắc phục tr&ecirc;n cảm biến S5K2L2 của Galaxy S8 Plus.</p>

<p><strong>Trải nghiệm người d&ugrave;ng c&ugrave;ng Samsung Experience 8.1</strong></p>

<p>M&igrave;nh sẽ kh&ocirc;ng n&oacute;i qu&aacute; nhiều về hiệu năng nữa, do Galaxy S8 Plus chạy tr&ecirc;n con chip Exynos 8895 c&ugrave;ng 4 GB RAM, 64 GB bộ nhớ trong n&ecirc;n mọi t&aacute;c vụ đều rất mượt m&agrave;.</p>

<p style="text-align:center"><a class="preventdefault" href="https://cdn.tgdd.vn/Files/2017/05/01/978135/img_0223_1280x720-800-resize.jpg" onclick="return false;"><img alt="Đánh giá chi tiết Galaxy S8 Plus" class="lazy" src="https://cdn1.tgdd.vn/Files/2017/05/01/978135/img_0223_1280x720-800-resize.jpg" style="display:block" title="Đánh giá chi tiết Galaxy S8 Plus" /></a></p>

<p>M&aacute;y đang được chạy tr&ecirc;n nền Android 7.0 c&ugrave;ng giao diện Samsung Experience 8.1 tuy lạ m&agrave; quen. Mọi t&iacute;nh năng từ Grace UX tr&ecirc;n S7 Edge đều giống hệt.</p>

<p style="text-align:center"><a class="preventdefault" href="https://cdn.tgdd.vn/Files/2017/05/01/978135/img_0202_1280x720-800-resize.jpg" onclick="return false;"><img alt="Đánh giá chi tiết Galaxy S8 Plus" class="lazy" src="https://cdn3.tgdd.vn/Files/2017/05/01/978135/img_0202_1280x720-800-resize.jpg" style="display:block" title="Đánh giá chi tiết Galaxy S8 Plus" /></a></p>

<p>Ngo&agrave;i ra, vấn đề việc tối ưu m&agrave;n h&igrave;nh 18,5:9 tr&ecirc;n S8 Plus cũng kh&ocirc;ng ổn định như k&igrave; vọng. Nhiều ứng dụng d&ugrave; đ&atilde; cho ph&eacute;p ph&oacute;ng tỉ lệ nhưng vẫn trơ ra, v&iacute; dụ như game Dota truyền k&igrave; m&igrave;nh đang chơi.</p>

<p style="text-align:center"><a class="preventdefault" href="https://cdn.tgdd.vn/Files/2017/05/01/978135/img_0204_1280x720-800-resize.jpg" onclick="return false;"><img alt="Đánh giá chi tiết Galaxy S8 Plus" class="lazy" src="https://cdn.tgdd.vn/Files/2017/05/01/978135/img_0204_1280x720-800-resize.jpg" style="display:block" title="Đánh giá chi tiết Galaxy S8 Plus" /></a></p>

<p>Một điểm mới tr&ecirc;n Galaxy S8 v&agrave; S8 Plus ch&iacute;nh l&agrave; c&ocirc; trợ l&yacute; ảo Bixby, nghe c&oacute; vẻ hay nhưng thực chất kh&ocirc;ng hữu &iacute;ch lắm ở Việt Nam. N&atilde;y giờ m&igrave;nh ch&ecirc; S8 Plus hơi nhiều, kh&ocirc;ng c&oacute; nghĩa l&agrave; n&oacute; kh&ocirc;ng l&agrave;m m&igrave;nh th&uacute; vị, tất nhi&ecirc;n phải nhắc đến t&iacute;nh c&aacute; nh&acirc;n h&oacute;a cao tr&ecirc;n giao diện m&agrave; Samsung mang lại cho người d&ugrave;ng.</p>

<p style="text-align:center"><a class="preventdefault" href="https://cdn.tgdd.vn/Files/2017/05/01/978135/img_0227_1280x720-800-resize.jpg" onclick="return false;"><img alt="Đánh giá chi tiết Galaxy S8 Plus" class="lazy" src="https://cdn2.tgdd.vn/Files/2017/05/01/978135/img_0227_1280x720-800-resize.jpg" style="display:block" title="Đánh giá chi tiết Galaxy S8 Plus" /></a></p>

<h3><strong>Thời lượng pin tr&ecirc;n cả mong đợi</strong></h3>

<p>Khi sử dụng m&agrave;n h&igrave;nh 6.2 inch k&egrave;m độ ph&acirc;n giải 2K+, nhưng vi&ecirc;n pin chỉ 3.500 mAh khiến nhiều người ho&agrave;i nghi về thời lượng pin tr&ecirc;n Galaxy S8 Plus. Nhưng sau v&agrave;i ng&agrave;y sử dụng, m&igrave;nh chỉ biết thốt l&ecirc;n 2 chữ : &quot;Qu&aacute; đỉnh!!&quot;.</p>

<p style="text-align:center"><a class="preventdefault" href="https://cdn.tgdd.vn/Files/2017/05/01/978135/img_0233_1280x720-800-resize.jpg" onclick="return false;"><img alt="Đánh giá chi tiết Galaxy S8 Plus" class="lazy" src="https://cdn4.tgdd.vn/Files/2017/05/01/978135/img_0233_1280x720-800-resize.jpg" style="display:block" title="Đánh giá chi tiết Galaxy S8 Plus" /></a></p>

<p>Trong ng&agrave;y đầu ti&ecirc;n m&igrave;nh d&ugrave;ng 4G li&ecirc;n tục v&agrave; hầu như chiếm 3/4 thời gian sử dụng, Galaxy S8 Plus c&oacute; thể trụ với thời gian onscreen gần 6 giờ. Cảm thấy c&oacute; điều g&igrave; đ&oacute; sai sai, m&igrave;nh chuyển sang d&ugrave;ng wifi hẳn v&agrave; m&aacute;y cho thời gian onscreen gần 7 giờ. Thật kinh khủng!</p>

<p style="text-align:center"><a class="preventdefault" href="https://cdn.tgdd.vn/Files/2017/05/01/978135/pin_1280x720-800-resize.jpg" onclick="return false;"><img alt="Đánh giá chi tiết Galaxy S8 Plus" class="lazy" src="https://cdn1.tgdd.vn/Files/2017/05/01/978135/pin_1280x720-800-resize.jpg" style="display:block" title="Đánh giá chi tiết Galaxy S8 Plus" /></a></p>

<p>Khi sử dụng m&aacute;y cực k&igrave; m&aacute;t, chỉ ấm nhẹ khi chơi game hay bật 4G. M&igrave;nh rất bất ngờ về thời gian sử dụng tr&ecirc;n chiếc Galaxy S8 Plus n&agrave;y.</p>

<h3><strong>Si&ecirc;u phẩm ho&agrave;n hảo nhất 2017?</strong></h3>

<p>Tổng kết lại, ch&uacute;ng ta c&oacute; một chiếc m&aacute;y m&agrave;n h&igrave;nh to 6.2 inch nhưng k&iacute;ch thước chỉ ngang c&aacute;c m&aacute;y 5.7 inch hay thậm ch&iacute; l&agrave; 5.5 inch. Đi k&egrave;m theo đ&oacute; l&agrave; hiệu năng tốt, camera chất lượng, thời lượng pin xuất sắc v&agrave; Samsung Experience mượt m&agrave;.</p>

<p style="text-align:center"><a class="preventdefault" href="https://cdn.tgdd.vn/Files/2017/05/01/978135/galaxys82_1280x719-800-resize.jpg" onclick="return false;"><img alt="Đánh giá chi tiết Galaxy S8 Plus" class="lazy" src="https://cdn3.tgdd.vn/Files/2017/05/01/978135/galaxys82_1280x719-800-resize.jpg" style="display:block" title="Đánh giá chi tiết Galaxy S8 Plus" /></a></p>
', N'/UploadFile/images/SamSung/SonySSS8B10_lon.png', 6, 0, NULL, NULL, NULL, 1, 1, 4, 1, 0, N'["/UploadFile/images/SamSung/SonySSS8B1_lon.png","/UploadFile/images/SamSung/SonySSS8B2_lon.png","/UploadFile/images/SamSung/SonySSS8B3_lon.png","/UploadFile/images/SamSung/SonySSS8B4_lon.png","/UploadFile/images/SamSung/SonySSS8B5_lon.png","/UploadFile/images/SamSung/SonySSS8B6_lon.png","/UploadFile/images/SamSung/SonySSS8B7_lon.png","/UploadFile/images/SamSung/SonySSS8B8_lon.png","/UploadFile/images/SamSung/SonySSS8B9_lon.png","/UploadFile/images/SamSung/SonySSS8B_lon.png"]', 1)
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [DonGia], [NgayCapNhap], [CauHinh], [MoTa], [HinhAnh], [SoLuongTon], [LuotXem], [LuotBinhChon], [LuotBinhLuan], [SoLanMua], [Moi], [MaNCC], [MaNSX], [MaLoaiSP], [DaXoa], [HinhAnh1], [Home]) VALUES (17, N'Sony Xperia XZs', CAST(13799000 AS Decimal(18, 0)), CAST(0x0000A76C00BAA01F AS DateTime), N'<div class="detail-col-left53 left">
<h3>Th&ocirc;ng số kỹ thuật</h3>

<ul>
	<li>Th&ocirc;ng số cơ bản</li>
	<li>M&agrave;n h&igrave;nh : 5.2 inch (1080 x 1920 pixels )</li>
	<li>Camera : Ch&iacute;nh: 19.0 MP, Phụ: 13.0 MP</li>
	<li>RAM : 4GB</li>
	<li>Bộ nhớ trong : 64 GB</li>
	<li>Hệ điều h&agrave;nh : Android 7.0 (Nougat)</li>
	<li>Chipset : Qualcomm MSM8996 Snapdragon 820</li>
	<li>CPU : Quad-core (2x2.15 GHz Kryo &amp; 2x1.6 GHz Kryo)</li>
	<li>GPU : Adreno 530</li>
	<li>K&iacute;ch thước : 146 x 72 x 8.1 mm</li>
</ul>

<ul>
	<li>M&agrave;n h&igrave;nh</li>
	<li>C&ocirc;ng nghệ m&agrave;n h&igrave;nh : TRILUMINOS&trade;</li>
	<li>M&agrave;u m&agrave;n h&igrave;nh : 16 Triệu m&agrave;u</li>
	<li>Chuẩn m&agrave;n h&igrave;nh : Full HD</li>
	<li>Độ ph&acirc;n giải m&agrave;n h&igrave;nh : 1080 x 1920 pixels</li>
	<li>C&ocirc;ng nghệ cảm ứng : Đa điểm</li>
	<li>Mặt k&iacute;nh m&agrave;n h&igrave;nh : K&iacute;nh Corning&reg; Gorilla&reg;</li>
	<li>Camera Sau</li>
	<li>Độ ph&acirc;n giải : 19.0 MP</li>
	<li>Quay phim : Quay phim 4K 2160p@30fps</li>
	<li>Đ&egrave;n Flash : C&oacute;</li>
	<li>Chụp ảnh n&acirc;ng cao : Chụp chống biến dạng ảnh, Chụp ảnh trong m&ocirc;i trường &aacute;nh s&aacute;ng yếu: ISO12800 /4000 (Video), Ảnh HDR, Ống k&iacute;nh G rộng 25 mm F2.0, Thu ph&oacute;ng kỹ thuật số 8x, C&ocirc;ng nghệ xử l&yacute; h&igrave;nh ảnh BIONZ&trade; cho di động SteadyShot&trade; với Chế độ Chuyển động th&ocirc;ng minh (chống rung 5 trục)</li>
	<li>Camera Trước</li>
	<li>Video Call : C&oacute;</li>
	<li>Độ ph&acirc;n giải : 13.0 MP</li>
	<li>Th&ocirc;ng tin kh&aacute;c : Camera g&oacute;c rộng, Quay video Full HD, Chế độ l&agrave;m đẹp, Nhận diện khu&ocirc;n mặt</li>
	<li>Cấu h&igrave;nh phần cứng</li>
	<li>Tốc độ CPU : 2 nh&acirc;n 2.15 GHz Kryo &amp; 2 nh&acirc;n 1.6 GHz Kryo</li>
	<li>Số nh&acirc;n : 4 nh&acirc;n</li>
	<li>Chipset : Qualcomm MSM8996 Snapdragon 820</li>
	<li>RAM : 4GB</li>
	<li>Chip đồ họa (GPU) : Adreno 530</li>
	<li>Cảm biến : V&acirc;n tay,gia tốc,khoảng c&aacute;ch,&aacute;p kế,la b&agrave;n</li>
	<li>Bộ nhớ &amp; Lưu trữ</li>
	<li>Danh bạ lưu trữ : Kh&ocirc;ng giới hạn</li>
	<li>Bộ nhớ trong : 64 GB</li>
	<li>Bộ nhớ c&ograve;n lại : Đang cập nhật</li>
	<li>Thẻ nhớ ngo&agrave;i : MicroSDXC</li>
	<li>Hỗ trợ thẻ nhớ tối đa : 256 GB</li>
	<li>Thiết kế &amp; Trọng lượng</li>
	<li>Chất liệu : Khung kim loại + mặt k&iacute;nh cường lực</li>
	<li>Kiểu d&aacute;ng : Thanh (thẳng) + Cảm ứng</li>
	<li>K&iacute;ch thước : 146 x 72 x 8.1 mm</li>
	<li>Trọng lượng : 161g</li>
	<li>Khả năng chống nước : Chuẩn IP68</li>
	<li>Th&ocirc;ng tin pin</li>
	<li>Loại pin : Li-Ion</li>
	<li>Dung lượng pin : 2900 mAh</li>
	<li>Pin c&oacute; thể th&aacute;o rời : Kh&ocirc;ng</li>
	<li>Chế độ sạc nhanh : C&oacute;</li>
	<li>Kết nối &amp; Cổng giao tiếp</li>
	<li>Băng tần 2G : GSM 850 / 900 / 1800 / 1900</li>
	<li>Băng tần 3G : HSDPA 850 / 900 / 1700 / 1900 / 2100</li>
	<li>Băng tần 4G : C&oacute;</li>
	<li>Hỗ trợ SIM : Nano Sim</li>
	<li>Khe cắm sim : 2 SIM 2 s&oacute;ng</li>
	<li>Wifi : 802.11a / b / g / n / ac</li>
	<li>GPS : A-GPS v&agrave; GLONASS</li>
	<li>Bluetooth : Bluetooth v4.2, apt-X, A2DP, LE, EDR</li>
	<li>GPRS/EDGE : C&oacute;</li>
	<li>NFC : C&oacute;</li>
	<li>Kết nối USB : USB Type-C</li>
	<li>Cổng kết nối kh&aacute;c : Hỗ trợ OTG</li>
	<li>Cổng sạc : USB Type-C</li>
	<li>Jack (Input &amp; Output) : 3.5 mm</li>
	<li>Giải tr&iacute; &amp; Ứng dụng</li>
	<li>Xem phim : H.265, 3GP, MP4, AVI, WMV, H.263</li>
	<li>Nghe nhạc : Midi, Lossless, MP3, WAV, WMA</li>
	<li>Ghi &acirc;m : C&oacute;</li>
	<li>FM radio : Kh&ocirc;ng</li>
	<li>Bảo h&agrave;nh</li>
	<li>Thời gian bảo h&agrave;nh : 12 Th&aacute;ng</li>
</ul>
</div>
', N'<h2>Sony Xperia XZs l&agrave; smartphone được Sony đầu tư mạnh mẽ về camera với h&agrave;ng loạt c&aacute;c trang bị cao cấp v&agrave; sở hữu cho m&igrave;nh một mức gi&aacute; b&aacute;n hợp l&yacute; với người ti&ecirc;u d&ugrave;ng.</h2>

<h3><strong>Thiết kế quen thuộc</strong></h3>

<p>M&aacute;y vẫn sở hữu thiết kế quen thuộc được thừa hưởng từ chiếc&nbsp;<a href="https://www.thegioididong.com/dtdd/sony-xperia-xz" target="_blank" title="Điện thoại Xperia XZ">Xperia XZ</a>&nbsp;đ&atilde; ra mắt trước đ&oacute;. Điểm kh&aacute;c biệt để ph&acirc;n biệt nằm ở mặt lưng khi XZs được trang bị cụm camera ch&iacute;nh với nhiều cảm biến hơn.</p>

<p><a class="preventdefault" href="https://www.thegioididong.com/images/42/92082/sony-xperia-xzs9.jpg" onclick="return false;"><img alt="Sony Xperia XZs bên cạnh Sony Xperia XZ" class="lazy" src="https://cdn2.tgdd.vn/Products/Images/42/92082/sony-xperia-xzs9.jpg" style="display:block" title="Sony Xperia XZs bên cạnh Sony Xperia XZ" /></a></p>

<p><em>Sony Xperia XZs b&ecirc;n cạnh Sony Xperia XZ</em></p>

<p>Thiết kế vu&ocirc;ng vức, sạng trọng v&agrave; mạnh mẽ XZs mang đến cho người d&ugrave;ng sự chắc chắn v&agrave; cứng c&aacute;p khi cầm nắm tr&ecirc;n tay.</p>

<p><a class="preventdefault" href="https://www.thegioididong.com/images/42/92082/sony-xperia-xzs10.jpg" onclick="return false;"><img alt="Thiết kế mạnh mẽ, sang trọng" class="lazy" src="https://cdn4.tgdd.vn/Products/Images/42/92082/sony-xperia-xzs10.jpg" style="display:block" title="Thiết kế mạnh mẽ, sang trọng" /></a></p>

<p><em>Thiết kế mạnh mẽ, sang trọng</em></p>

<p>Mặt k&iacute;nh của m&aacute;y cũng được ho&agrave;n thiện cong 2.5D mang lại trải nghiệm thực sự tốt khi vuốt từ c&aacute;c m&eacute;p của cạnh viền v&agrave;o m&agrave;n h&igrave;nh.</p>

<p><a class="preventdefault" href="https://www.thegioididong.com/images/42/92082/sony-xperia-xzs2.jpg" onclick="return false;"><img alt="Mặt kính cong 2.5D thời thượng" class="lazy" src="https://cdn1.tgdd.vn/Products/Images/42/92082/sony-xperia-xzs2.jpg" style="display:block" title="Mặt kính cong 2.5D thời thượng" /></a></p>

<p><em>Mặt k&iacute;nh cong 2.5D thời thượng</em></p>

<p>Một điểm kh&aacute; đ&aacute;ng khen l&agrave; XZs vẫn giữ cho m&igrave;nh ph&iacute;m chụp h&igrave;nh nhanh truyền thống của Sony, c&aacute;i m&agrave; hầu hết c&aacute;c h&atilde;ng sản xuất smartphone hiện nay đều lược bỏ.</p>

<p><a class="preventdefault" href="https://www.thegioididong.com/images/42/92082/sony-xperia-xzs6.jpg" onclick="return false;"><img alt="Cụm phím cứng kích hoạt nhanh camera" class="lazy" src="https://cdn3.tgdd.vn/Products/Images/42/92082/sony-xperia-xzs6.jpg" style="display:block" title="Cụm phím cứng kích hoạt nhanh camera" /></a></p>

<p><em>Cụm ph&iacute;m cứng k&iacute;ch hoạt nhanh camera</em></p>

<h3><strong>Camera h&agrave;ng đầu</strong></h3>

<p>Điểm ch&uacute; &yacute; lớn nhất tr&ecirc;n chiếc XZs l&agrave; khả năng quay video&nbsp;super slow motion l&ecirc;n tới 960 khung h&igrave;nh tr&ecirc;n gi&acirc;y mang lại cho người d&ugrave;ng những thước phim độc nhất v&ocirc; nhị để chia sẻ với bạn b&egrave;.</p>

<p><a class="preventdefault" href="https://www.thegioididong.com/images/42/92082/sony-xperia-xzs7.jpg" onclick="return false;"><img alt="Camera chất lượng cao" class="lazy" src="https://cdn.tgdd.vn/Products/Images/42/92082/sony-xperia-xzs7.jpg" style="display:block" title="Camera chất lượng cao" /></a></p>

<p><em>Camera chất lượng cao</em></p>

<p>Việc quay được video quay chậm 960 fps sẽ gi&uacute;p cho người d&ugrave;ng c&oacute; thể theo d&otilde;i được c&aacute;c chuyển động nhanh một c&aacute;ch ch&acirc;n thực v&agrave; ch&iacute;nh x&aacute;c nhất.</p>

<p><a class="preventdefault" href="https://www.thegioididong.com/images/42/92082/sony-xperia-xzs4.jpg" onclick="return false;"><img alt="Ảnh chụp ban ngày chất lượng tốt" class="lazy" src="https://cdn2.tgdd.vn/Products/Images/42/92082/sony-xperia-xzs4.jpg" style="display:block" title="Ảnh chụp ban ngày chất lượng tốt" /></a></p>

<p><em>Ảnh chụp ban ng&agrave;y chất lượng tốt</em></p>

<p>Camera ch&iacute;nh độ ph&acirc;n giải 19 MP, được trang bị&nbsp;cảm biến h&igrave;nh ảnh ba chiều v&agrave; đặc biệt l&agrave; hệ thống chống rung 5 trục mang lại chất lượng h&igrave;nh ảnh tốt hơn khi chụp.</p>

<p><a class="preventdefault" href="https://www.thegioididong.com/images/42/92082/sony-xperia-xzs1.jpg" onclick="return false;"><img alt="Ảnh chụp đêm" class="lazy" src="https://cdn4.tgdd.vn/Products/Images/42/92082/sony-xperia-xzs1.jpg" style="display:block" title="Ảnh chụp đêm" /></a></p>

<p><em>Ảnh chụp đ&ecirc;m</em></p>

<p>Ngo&agrave;i ra c&ocirc;ng nghệ&nbsp;Motion Eye được trang bị tr&ecirc;n XZs gi&uacute;p m&aacute;y c&oacute; thể dự đo&aacute;n được chuyển động trước v&agrave; chụp những bức ảnh vật thể chuyển động v&agrave; kh&ocirc;ng bị mờ nh&ograve;e.</p>

<p><a class="preventdefault" href="https://www.thegioididong.com/images/42/92082/sony-xperia-xzs3.jpg" onclick="return false;"><img alt="Camera trước cũng cho ảnh chi tiết tốt, hỗ trợ nhận diện khuôn mặt" class="lazy" src="https://cdn1.tgdd.vn/Products/Images/42/92082/sony-xperia-xzs3.jpg" style="display:block" title="Camera trước cũng cho ảnh chi tiết tốt, hỗ trợ nhận diện khuôn mặt" /></a></p>

<p><em>Camera trước cũng cho ảnh chi tiết tốt, hỗ trợ nhận diện khu&ocirc;n mặt</em></p>

<p>Camera trước cũng sở hữu độ ph&acirc;n giải l&ecirc;n tới 13 MP, hỗ trợ lấy n&eacute;t tự động v&agrave; được t&iacute;ch hợp sẵn chế độ l&agrave;m đẹp mang lại những bức ảnh selfie chất lượng.</p>

<h3><strong>Hiệu năng mạnh mẽ</strong></h3>

<p>Cấu h&igrave;nh cũng l&agrave; một n&acirc;ng cấp kh&aacute; đ&aacute;ng gi&aacute; của chiếc XZs với con chip&nbsp;<a href="https://www.thegioididong.com/hoi-dap/qualcomm-snapdragon-820-763732" target="_blank" title="Tìm hiểu chip Snapdragon 820 4 nhân 64-bit">Snapdragon 820 4 nh&acirc;n 64-bit</a>&nbsp;kết hợp với 4 GB RAM mang lại sự ổn định khi sử dụng trong thời gian d&agrave;i.</p>

<p><a class="preventdefault" href="https://www.thegioididong.com/images/42/92082/sony-xperia-xzs12.jpg" onclick="return false;"><img alt="Cấu hình thoải mái đáp ứng mọi nhu cầu sử dụng của người dùng" class="lazy" src="https://cdn3.tgdd.vn/Products/Images/42/92082/sony-xperia-xzs12.jpg" style="display:block" title="Cấu hình thoải mái đáp ứng mọi nhu cầu sử dụng của người dùng" /></a></p>

<p><em>Cấu h&igrave;nh thoải m&aacute;i đ&aacute;p ứng mọi nhu cầu sử dụng của người d&ugrave;ng</em></p>

<p>M&aacute;y cũng sở hữu bộ nhớ trong với dung lượng lớn 64 GB cho người d&ugrave;ng thoải m&aacute;i c&agrave;i đặt ứng dụng v&agrave; lưu trữ dữ liệu c&aacute; nh&acirc;n m&agrave; kh&ocirc;ng cần d&ugrave;ng th&ecirc;m&nbsp;<a href="https://www.thegioididong.com/the-nho-dien-thoai" target="_blank" title="Thẻ nhớ điện thoại">thẻ nhớ</a>&nbsp;ngo&agrave;i.</p>

<h3><strong>M&agrave;n h&igrave;nh hiển thị đẹp</strong></h3>

<p>Với k&iacute;ch thước m&agrave;n h&igrave;nh 5.2 inch c&ugrave;ng độ ph&acirc;n giải FullHD XZs mang lại cho bạn những h&igrave;nh ảnh sắc n&eacute;t v&agrave; ch&acirc;n thực.</p>

<p><a class="preventdefault" href="https://www.thegioididong.com/images/42/92082/sony-xperia-xzs5.jpg" onclick="return false;"><img alt="Màn hình trung thực, góc nhìn rộng" class="lazy" src="https://cdn.tgdd.vn/Products/Images/42/92082/sony-xperia-xzs5.jpg" style="display:block" title="Màn hình trung thực, góc nhìn rộng" /></a></p>

<p><em>M&agrave;n h&igrave;nh trung thực, g&oacute;c nh&igrave;n rộng</em></p>

<p><a href="https://www.thegioididong.com/dtdd-sony" target="_blank" title="Điện thoại Sony">Sony</a>&nbsp;cũng trang bị cho m&aacute;y c&ocirc;ng nghệ&nbsp;TriLuminous gi&uacute;p bạn dễ d&agrave;ng theo d&otilde;i nội dung tr&ecirc;n m&agrave;n h&igrave;nh ngay cả dưới &aacute;nh s&aacute;ng mạnh.</p>

<h3><strong>Android 7 Nougat</strong></h3>

<p>Xperia XZs sẽ chạy tr&ecirc;n nền Android 7.0 c&ugrave;ng vi&ecirc;n pin 2.900 mAh, hỗ trợ c&ocirc;ng nghệ sạc nhanh XCharge.</p>

<p><a class="preventdefault" href="https://www.thegioididong.com/images/42/92082/sony-xperia-xzs8.jpg" onclick="return false;"><img alt="Android mới, mang lại trải nghiệm mượt mà" class="lazy" src="https://cdn2.tgdd.vn/Products/Images/42/92082/sony-xperia-xzs8.jpg" style="display:block" title="Android mới, mang lại trải nghiệm mượt mà" /></a></p>

<p><em>Android mới, mang lại trải nghiệm mượt m&agrave;</em></p>

<p>Với nhiều cải tiến mạnh mẽ cả về cấu h&igrave;nh lẫn camera th&igrave; Xperia XZs xứng đ&aacute;ng l&agrave; si&ecirc;u phẩm đ&aacute;ng sở hữu cho người d&ugrave;ng trong khoảng thời gian đầu năm 2017.</p>
', N'/UploadFile/images/Sony/SonySonyXZsB_lon.png', 16, 0, NULL, NULL, NULL, 1, 1, 3, 1, 0, N'["/UploadFile/images/Sony/SonySonyXZsB2_lon.png","/UploadFile/images/Sony/SonySonyXZsB3_lon.png","/UploadFile/images/Sony/SonySonyXZsB4_lon.png","/UploadFile/images/Sony/SonySonyXZsB5_lon.png","/UploadFile/images/Sony/SonySonyXZsB6_lon.png","/UploadFile/images/Sony/SonySonyXZsB_lon.png"]', 0)
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [DonGia], [NgayCapNhap], [CauHinh], [MoTa], [HinhAnh], [SoLuongTon], [LuotXem], [LuotBinhChon], [LuotBinhLuan], [SoLanMua], [Moi], [MaNCC], [MaNSX], [MaLoaiSP], [DaXoa], [HinhAnh1], [Home]) VALUES (18, N'Xiaomi Redmi Note 4', CAST(3700000 AS Decimal(18, 0)), CAST(0x0000A7A80186915D AS DateTime), N'<h3>Th&ocirc;ng số kỹ thuật</h3>

<ul>
	<li>M&agrave;n h&igrave;nh</li>
	<li>C&ocirc;ng nghệ m&agrave;n h&igrave;nh
	<div><a href="https://www.thegioididong.com/hoi-dap/man-hinh-ips-lcd-la-gi-905752" target="_blank">IPS LCD</a></div>
	</li>
	<li>Độ ph&acirc;n giải
	<div>Full HD (1080 x 1920 pixels)</div>
	</li>
	<li>M&agrave;n h&igrave;nh rộng
	<div>5.5&quot;</div>
	</li>
	<li>
	<div class="ph">Mặt k&iacute;nh cảm ứng</div>

	<div>K&iacute;nh cường lực</div>
	</li>
	<li>Camera sau</li>
	<li>Độ ph&acirc;n giải
	<div>13 MP</div>
	</li>
	<li>Quay phim
	<div><a href="https://www.thegioididong.com/tin-tuc/chuan-quay-phim-tren-dien-thoai-596472#fullhd" target="_blank">Quay phim FullHD 1080p@30fps</a></div>
	</li>
	<li>Đ&egrave;n Flash
	<div><a href="https://www.thegioididong.com/hoi-dap/den-flash-led-2-tong-mau-dual-tone-led-tren-di-898387" target="_blank">Đ&egrave;n LED 2 t&ocirc;ng m&agrave;u</a></div>
	</li>
	<li>Chụp ảnh n&acirc;ng cao
	<div><a href="https://www.thegioididong.com/hoi-dap/che-do-autofocus-trong-chup-anh-tren-smartphone-906408" target="_blank">Tự động lấy n&eacute;t</a>, <a href="https://www.thegioididong.com/hoi-dap/che-do-touch-focus-khi-chup-anh-tren-smartphone-906412" target="_blank">Chạm lấy n&eacute;t</a>, <a href="https://www.thegioididong.com/hoi-dap/nhan-dien-khuon-mat-la-gi-907903" target="_blank">Nhận diện khu&ocirc;n mặt</a>, <a href="https://www.thegioididong.com/hoi-dap/che-do-chup-anh-hdr-tren-smartphone-la-gi-906400" target="_blank">HDR</a>, <a href="https://www.thegioididong.com/hoi-dap/chup-anh-panorama-toan-canh-tren-camera-cua-smar-906425" target="_blank">Panorama</a></div>
	</li>
	<li>Camera trước</li>
	<li>Độ ph&acirc;n giải
	<div>5 MP</div>
	</li>
	<li>Videocall
	<div>C&oacute;</div>
	</li>
	<li>Th&ocirc;ng tin kh&aacute;c
	<div><a href="https://www.thegioididong.com/tin-tuc/chuan-quay-phim-tren-dien-thoai-596472#fullhd" target="_blank">Quay video Full HD</a>, <a href="https://www.thegioididong.com/tin-tuc/cac-tinh-nang-chup-anh-camera-truoc-769270#lamdep" target="_blank">Chế độ l&agrave;m đẹp</a></div>
	</li>
	<li>Hệ điều h&agrave;nh - CPU</li>
	<li>Hệ điều h&agrave;nh
	<div><a href="https://www.thegioididong.com/hoi-dap/android-6-marshmallow-co-gi-hot-726146" target="_blank">Android 6.0 (Marshmallow)</a></div>
	</li>
	<li>Chipset (h&atilde;ng SX CPU)
	<div><a href="https://www.thegioididong.com/hoi-dap/qualcomm-snapdragon-625-839970" target="_blank">Snapdragon 625 8 nh&acirc;n 64-bit</a></div>
	</li>
	<li>Tốc độ CPU
	<div>2.0 GHz</div>
	</li>
	<li>Chip đồ họa (GPU)
	<div>Adreno 506</div>
	</li>
	<li>Bộ nhớ &amp; Lưu trữ</li>
	<li>RAM
	<div>3 GB</div>
	</li>
	<li>Bộ nhớ trong
	<div>32 GB</div>
	</li>
	<li>Bộ nhớ c&ograve;n lại (khả dụng)
	<div>Đang cập nhật</div>
	</li>
	<li>Thẻ nhớ ngo&agrave;i
	<div>MicroSD, hỗ trợ tối đa 128 GB</div>
	</li>
	<li>Kết nối</li>
	<li>Mạng di động
	<div><a href="https://www.thegioididong.com/hoi-dap/4g-la-gi-731757" target="_blank">Có h&ocirc;̃ trợ 4G</a></div>
	</li>
	<li>SIM
	<div>Nano SIM &amp; Micro SIM (SIM 2 chung khe thẻ nhớ)</div>
	</li>
	<li>Wifi
	<div><a href="https://www.thegioididong.com/tin-tuc/wifi-la-gi-cai-dat-wifi-hotspot-nhu-the-nao--590309#80211ac" target="_blank">Wi-Fi 802.11 a/b/g/n/ac</a>, <a href="https://www.thegioididong.com/hoi-dap/wifi-dual-band-la-gi-736489" target="_blank">Dual-band</a>, <a href="https://www.thegioididong.com/tin-tuc/wi-fi-direct-la-gi--590298" target="_blank">Wi-Fi Direct</a>, <a href="https://www.thegioididong.com/tin-tuc/wifi-la-gi-cai-dat-wifi-hotspot-nhu-the-nao--590309#wifihotspot" target="_blank">Wi-Fi hotspot</a></div>
	</li>
	<li>GPS
	<div><a href="https://www.thegioididong.com/tin-tuc/he-thong-dinh-vi-toan-cau-gps-la-gi--590552#agps" target="_blank">A-GPS</a>, <a href="https://www.thegioididong.com/hoi-dap/he-thong-dinh-vi-gps-va-glonass-la-gi-729876#glonass" target="_blank">GLONASS</a></div>
	</li>
	<li>Bluetooth
	<div><a href="https://www.thegioididong.com/hoi-dap/bluetooth-42-la-gi-895615" target="_blank">v4.2</a>, <a href="https://www.thegioididong.com/hoi-dap/cong-nghe-bluetooth-a2dp-la-gi-723725" target="_blank">A2DP</a>, <a href="https://www.thegioididong.com/hoi-dap/cong-nghe-bluetooth-743602#le" target="_blank">LE</a></div>
	</li>
	<li>C&ocirc;̉ng k&ecirc;́t n&ocirc;́i/sạc
	<div><a href="http://www.thegioididong.com/tin-tuc/micro-usb-la-gi--590081" target="_blank">Micro USB</a></div>
	</li>
	<li>Jack tai nghe
	<div>3.5 mm</div>
	</li>
	<li>Kết nối kh&aacute;c
	<div><a href="https://www.thegioididong.com/tin-tuc/ket-noi-otg-la-gi-va-cach-su-dung-tren-smartphone-nhu-the-nao-834314" target="_blank">OTG</a></div>
	</li>
	<li>Thiết kế &amp; Trọng lượng</li>
	<li>Thiết kế
	<div><a href="https://www.thegioididong.com/tin-tuc/tim-hieu-cac-kieu-thiet-ke-tren-di-dong-va-may-tin-597153#nguyenkhoi" target="_blank">Nguy&ecirc;n khối</a></div>
	</li>
	<li>Chất liệu
	<div>Kim loại</div>
	</li>
	<li>K&iacute;ch thước
	<div>D&agrave;i 151 mm - Ngang 76 mm - D&agrave;y 8.4 mm</div>
	</li>
	<li>Trọng lượng
	<div>165 g</div>
	</li>
	<li>Th&ocirc;ng tin pin &amp; Sạc</li>
	<li>Dung lượng pin
	<div>4100 mAh</div>
	</li>
	<li>Loại pin
	<div><a href="https://www.thegioididong.com/tin-tuc/pin-li-po-la-gi--596475" target="_blank">Pin chuẩn Li-Po</a></div>
	</li>
	<li>
	<div class="ph">C&ocirc;ng nghệ pin</div>

	<div>Tiết kiệm pin</div>
	</li>
	<li>Tiện &iacute;ch</li>
	<li>Bảo mật n&acirc;ng cao
	<div>Mở kh&oacute;a bằng v&acirc;n tay</div>
	</li>
	<li>T&iacute;nh năng đặc biệt
	<div>Đang cập nhật</div>
	</li>
	<li>Ghi &acirc;m
	<div><a href="https://www.thegioididong.com/hoi-dap/microphone-chong-on-la-gi-894183" target="_blank">C&oacute;, microphone chuy&ecirc;n dụng chống ồn</a></div>
	</li>
	<li>Radio
	<div>Kh&ocirc;ng</div>
	</li>
	<li>Xem phim
	<div><a href="https://www.thegioididong.com/hoi-dap/cac-dinh-dang-video-va-am-thanh-pho-bien-hien-nay-740243#h265" target="_blank">H.265</a>, <a href="https://www.thegioididong.com/hoi-dap/cac-dinh-dang-video-va-am-thanh-740243#mp4" target="_blank">MP4</a>, <a href="https://www.thegioididong.com/hoi-dap/cac-dinh-dang-video-va-am-thanh-740243#avi" target="_blank">AVI</a>, <a href="https://www.thegioididong.com/hoi-dap/cac-dinh-dang-video-va-am-thanh-740243#wmv" target="_blank">WMV</a>, <a href="https://www.thegioididong.com/hoi-dap/cac-dinh-dang-video-va-am-thanh-740243#h264" target="_blank">H.264(MPEG4-AVC)</a>, <a href="https://www.thegioididong.com/hoi-dap/cac-dinh-dang-video-va-am-thanh-pho-bien-hien-nay-740243#divx" target="_blank">DivX</a>, <a href="https://www.thegioididong.com/hoi-dap/cac-dinh-dang-video-va-am-thanh-pho-bien-hien-nay-740243#xvid" target="_blank">Xvid</a></div>
	</li>
	<li>Nghe nhạc
	<div><a href="https://www.thegioididong.com/hoi-dap/cac-dinh-dang-video-va-am-thanh-740243#lossless" target="_blank">Lossless</a>, <a href="https://www.thegioididong.com/hoi-dap/cac-dinh-dang-video-va-am-thanh-740243#mp3" target="_blank">MP3</a>, <a href="https://www.thegioididong.com/hoi-dap/cac-dinh-dang-video-va-am-thanh-740243#wav" target="_blank">WAV</a>, <a href="https://www.thegioididong.com/hoi-dap/cac-dinh-dang-video-va-am-thanh-740243#wma" target="_blank">WMA</a>, <a href="https://www.thegioididong.com/hoi-dap/cac-dinh-dang-video-va-am-thanh-740243#eaac" target="_blank">eAAC+</a>, <a href="https://www.thegioididong.com/hoi-dap/cac-dinh-dang-video-va-am-thanh-740243#ac3" target="_blank">AC3</a>, <a href="https://www.thegioididong.com/hoi-dap/cac-dinh-dang-video-va-am-thanh-740243#flac" target="_blank">FLAC</a></div>
	</li>
</ul>
', N'<h2 style="text-align:justify"><strong>Với những th&agrave;nh c&ocirc;ng m&agrave; chiếc&nbsp;Xiaomi Redmi Note 3 mang lại th&igrave; mới đ&acirc;y <a href="https://www.thegioididong.com/dtdd-xiaomi" target="_blank" title="Xiaomi" type="Xiaomi">Xiaomi</a> lại tiếp tục giới thiệu th&ecirc;m phi&ecirc;n bản n&acirc;ng cấp l&agrave; chiếc&nbsp;Xiaomi Redmi Note 4 với cấu h&igrave;nh mạnh hơn, thiết kế đẹp hơn.</strong></h2>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/84905/xiaomi-redmi-note-4323.jpg" onclick="return false;"><img alt="Thiết kế tinh xảo" class="lazy" src="https://cdn3.tgdd.vn/Products/Images/42/84905/xiaomi-redmi-note-4323.jpg" style="display:block" title="Thiết kế tinh xảo" /></a></p>

<h3 style="text-align:justify"><strong>Thiết kế tinh xảo</strong></h3>

<p style="text-align:justify">Xiaomi Redmi Note 4 sở hữu cho m&igrave;nh vẻ bề ngo&agrave;i sang trọng v&agrave; tinh tế kh&ocirc;ng k&eacute;m g&igrave; c&aacute;c thiết bị cao cấp. M&aacute;y sở hữu thiết kế nguy&ecirc;n khối với chất liệu kim loại chắc chắn.</p>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/84905/xiaomi-redmi-note-4-mau-vang-dong-2.jpg" onclick="return false;"><img alt="Thiết kế nguyên khối được gia công tỉ mỉ" class="lazy" src="https://cdn.tgdd.vn/Products/Images/42/84905/xiaomi-redmi-note-4-mau-vang-dong-2.jpg" style="display:block" title="Thiết kế nguyên khối được gia công tỉ mỉ" /></a></p>

<p style="text-align:center"><em>Thiết kế sang trọng với c&aacute;c chi tiết được gia c&ocirc;ng tỉ mỉ</em></p>

<p style="text-align:justify">Phần mặt k&iacute;nh của&nbsp;Redmi Note 4 cũng được ho&agrave;n thiện cong 2.5D mang lại trải nghiệm mượt m&agrave; cho người d&ugrave;ng.</p>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/84905/xiaomi-redmi-note-4-mau-vang-dong-3.jpg" onclick="return false;"><img alt="Mặt kính cong 2.5D thời trang" class="lazy" src="https://cdn2.tgdd.vn/Products/Images/42/84905/xiaomi-redmi-note-4-mau-vang-dong-3.jpg" style="display:block" title="Mặt kính cong 2.5D thời trang" /></a></p>

<p style="text-align:center"><em>Mặt k&iacute;nh cong 2.5D thời trang</em></p>

<p style="text-align:justify">Phần mặt lưng được l&agrave;m liền mạch với camera kh&ocirc;ng lồi, c&aacute;c đường anten bắt s&oacute;ng cũng được thiết kế tinh tế tạo sự đồng nhất cho m&aacute;y.</p>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/84905/xiaomi-redmi-note-4-mau-vang-dong-5.jpg" onclick="return false;"><img alt="Thiết kế mặt lưng tinh tế" class="lazy" src="https://cdn4.tgdd.vn/Products/Images/42/84905/xiaomi-redmi-note-4-mau-vang-dong-5.jpg" style="display:block" title="Thiết kế mặt lưng tinh tế" /></a></p>

<p style="text-align:center"><em>Mặt lưng liền mạch với camera phẳng ho&agrave;n to&agrave;n</em></p>

<h3 style="text-align:justify"><strong>M&agrave;n h&igrave;nh hiển thị đẹp</strong></h3>

<p style="text-align:justify">M&aacute;y sở hữu cho m&igrave;nh m&agrave;n h&igrave;nh c&oacute; k&iacute;ch thước 5.5 inch độ ph&acirc;n giải&nbsp;1080 x 1920 pixels c&ugrave;ng tấm nền&nbsp;<a href="https://www.thegioididong.com/tin-tuc/loai-man-hinh-tft-lcd-amoled-la-gi--592346#ipslcd" target="_blank" title="Tìm hiểu về màn hình IPS LCD">IPS LCD</a>&nbsp;đem lại cho bạn một g&oacute;c nh&igrave;n tốt v&agrave; khả năng t&aacute;i tạo m&agrave;u sắc trung thực.</p>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/84905/xiaomi-redmi-note-4-mau-vang-dong-1.jpg" onclick="return false;"><img alt="Màn hình hiển thị đẹp" class="lazy" src="https://cdn1.tgdd.vn/Products/Images/42/84905/xiaomi-redmi-note-4-mau-vang-dong-1.jpg" style="display:block" title="Màn hình hiển thị đẹp" /></a></p>

<p style="text-align:center"><em>M&agrave;n h&igrave;nh hiển thị đẹp</em></p>

<p style="text-align:justify">Ngo&agrave;i ra m&aacute;y cũng được trang bị c&ocirc;ng nghệ th&iacute;ch nghi với m&ocirc;i trường xung quanh gi&uacute;p tăng độ s&aacute;ng v&agrave; tương phản của m&aacute;y khi sử dụng dưới c&aacute;c điều kiện &aacute;nh s&aacute;ng mạnh.</p>

<p style="text-align:justify"><strong>Hiệu năng mạnh mẽ</strong></p>

<p style="text-align:justify">So với Redmi Note 3 th&igrave; với thế hệ tiếp theo Xiaomi đ&atilde; n&acirc;ng cấp con chip từ Helio X10 l&ecirc;n con chip <a href="https://www.thegioididong.com/hoi-dap/qualcomm-snapdragon-625-839970" target="_blank" title="Snapdragon 625" type="Snapdragon 625">Snapdragon 625</a> mạnh mẽ đi k&egrave;m với đ&oacute; l&agrave; 3 GB RAM v&agrave; 32 GB bộ nhớ trong.</p>

<p style="text-align:center">&nbsp;</p>

<div class="video"><iframe frameborder="0" src="https://www.youtube.com/embed/u6R_ryS1w7s?rel=0"></iframe></div>

<p>&nbsp;</p>

<p style="text-align:center"><em>M&aacute;y cho trải nghiệm kh&ocirc;ng k&eacute;m cạnh g&igrave; so với c&aacute;c sản phẩm c&ugrave;ng ph&acirc;n kh&uacute;c</em></p>

<p style="text-align:justify">M&aacute;y cũng hỗ trợ khe cắm thẻ nhớ mở rộng MicoSD tối đa l&ecirc;n tới 128 GB c&ugrave;ng kết nối 4G tốc độ cao. Redmi Note 4 chạy sẵn <a href="https://www.thegioididong.com/hoi-dap/android-6-marshmallow-co-gi-hot-726146" target="_blank" title="Tìm hiểu Android 6.0">Android 6.0</a> khi đến tay người d&ugrave;ng.</p>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/84905/xiaomi-redmi-note-4-mau-vang-dong-9.jpg" onclick="return false;"><img alt="Khe sim và thẻ nhớ" class="lazy" src="https://cdn3.tgdd.vn/Products/Images/42/84905/xiaomi-redmi-note-4-mau-vang-dong-9.jpg" style="display:block" title="Khe sim và thẻ nhớ" /></a></p>

<p style="text-align:center"><em>Khe cắm sim v&agrave; thẻ nhớ tr&ecirc;n m&aacute;y</em></p>

<h3 style="text-align:justify"><strong>Camera chất lượng cao</strong></h3>

<p style="text-align:justify">Với camera ch&iacute;nh độ ph&acirc;n giải 13 MP c&ugrave;ng đ&egrave;n flash&nbsp;Dual-tone LED trợ s&aacute;ng th&igrave; m&aacute;y đem lại chất lượng ảnh kh&aacute; tốt. Trong điều kiện đủ s&aacute;ng m&aacute;y cho ảnh c&oacute; chi tiết cao, m&agrave;u sắc tươi s&aacute;ng v&agrave; trung thực.</p>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/84905/xiaomi-redmi-note-4-mau-vang-dong-14.jpg" onclick="return false;"><img alt="Camera chất lượng khá tốt" class="lazy" src="https://cdn.tgdd.vn/Products/Images/42/84905/xiaomi-redmi-note-4-mau-vang-dong-14.jpg" style="display:block" title="Camera chất lượng khá tốt" /></a></p>

<p style="text-align:center"><em>Camera ch&iacute;nh c&oacute; chất lượng tốt</em></p>

<p style="text-align:justify">Camera trước của m&aacute;y cũng c&oacute; độ ph&acirc;n giải lớn 5 MP, hỗ trợ quay phim độ ph&acirc;n giải Full HD v&agrave; c&oacute; chế độ l&agrave;m đẹp được t&iacute;ch hợp sẵn gi&uacute;p bạn c&oacute; những bức ảnh tự sướng ảo diệu.</p>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/84905/xiaomi-redmi-note-4xiaomi-redmi-note-43.jpg" onclick="return false;"><img alt="Hình ảnh được chụp từ Redmi Note 4" class="lazy" src="https://cdn2.tgdd.vn/Products/Images/42/84905/xiaomi-redmi-note-4xiaomi-redmi-note-43.jpg" style="display:block" title="Hình ảnh được chụp từ Redmi Note 4" /></a></p>

<p style="text-align:center"><em>H&igrave;nh ảnh được chụp từ Redmi Note 4</em></p>

<p style="text-align:center"><a class="preventdefault" href="https://www.thegioididong.com/images/42/84905/xiaomi-redmi-note-4xiaomi-redmi-note-42.jpg" onclick="return false;"><img alt="Chi tiết ảnh thu lại khá cao" class="lazy" src="https://cdn4.tgdd.vn/Products/Images/42/84905/xiaomi-redmi-note-4xiaomi-redmi-note-42.jpg" style="display:block" title="Chi tiết ảnh thu lại khá cao" /></a></p>

<p style="text-align:center"><em>Chi tiết ảnh thu lại kh&aacute; cao</em></p>

<h3 style="text-align:justify"><strong>Thời lượng pin khủng</strong></h3>

<p style="text-align:justify">Pin cũng l&agrave; điểm mạnh tr&ecirc;n Redmi Note 4 với vi&ecirc;n pin c&oacute; dung lượng l&ecirc;n tới 4100 mAh th&igrave; m&aacute;y c&oacute; thể cho bạn thời gian sử dụng l&ecirc;n tới hơn 2 ng&agrave;y với c&aacute;c t&aacute;c vụ cơ bản.</p>

<p>Với mức gi&aacute; b&aacute;n hấp dẫn cũng nhiều t&iacute;nh năng vượt trội th&igrave; Redmi Note 4 thực sự đang tạo n&ecirc;n cơn sốt tr&ecirc;n thị trường <a href="https://www.thegioididong.com/dtdd" style="text-align: justify;" target="_blank" title="Điện thoại di động">smartphone </a>gi&aacute; rẻ.</p>

<p>&nbsp;</p>

<p><a href="/UploadFile/files/PhanTich.docx">Link text</a></p>
', N'/UploadFile/images/Xiaomi/Sonyredmenote4B1_lon.png', 1, NULL, NULL, NULL, NULL, 1, 1, 5, 1, 0, N'["/UploadFile/images/Xiaomi/Sonyredmenote4B2_lon.png","/UploadFile/images/Xiaomi/Sonyredmenote4B_lon.png","/UploadFile/images/Xiaomi/Master.jpg","/UploadFile/images/Xiaomi/1475059444319148.jpg"]', 0)
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [DonGia], [NgayCapNhap], [CauHinh], [MoTa], [HinhAnh], [SoLuongTon], [LuotXem], [LuotBinhChon], [LuotBinhLuan], [SoLanMua], [Moi], [MaNCC], [MaNSX], [MaLoaiSP], [DaXoa], [HinhAnh1], [Home]) VALUES (19, N'iPad Mini 3 wifi+4G (64GB)', CAST(8699000 AS Decimal(18, 0)), CAST(0x0000A7680165B57E AS DateTime), N'<div class="tabs_sub_hidden tabs_sub_show" id="tab_tinhnang">
<div style="padding:10xpx">
<table cellpadding="5" cellspacing="0" class=" infoDetail table" style="border-left-color:#cccccc; border-left-style:solid; border-left-width:1px; border-top-color:#cccccc; border-top-style:solid; border-top-width:1px; color:#333333; font-family:Roboto,Arial,Helvetica,sans-serif; font-size:14px; width:100%">
	<tbody>
		<tr>
		</tr>
		<tr>
			<td colspan="2" style="background-color:#f0f0f0">Tổng quan</td>
		</tr>
		<tr>
			<td>Điện thoại,SMS</td>
			<td>Kh&ocirc;ng</td>
		</tr>
		<tr>
			<td>Mạng 2G</td>
			<td>GSM 850 / 900 / 1800 / 1900</td>
		</tr>
		<tr>
			<td>Mạng 3G</td>
			<td>HSDPA 850 / 900 / 1900 / 2100</td>
		</tr>
		<tr>
			<td>Ra mắt</td>
			<td>Th&aacute;ng 10 năm 2014</td>
		</tr>
		<tr>
			<td colspan="2" style="background-color:#f0f0f0">K&iacute;ch thước</td>
		</tr>
		<tr>
			<td>K&iacute;ch thước</td>
			<td>200 x 134.7 x 7.5 mm</td>
		</tr>
		<tr>
			<td>Trọng lượng</td>
			<td>341g</td>
		</tr>
		<tr>
			<td colspan="2" style="background-color:#f0f0f0">Hiển thị</td>
		</tr>
		<tr>
			<td>Loại</td>
			<td>M&agrave;n h&igrave;nh cảm ứng điện dung LED-backlit IPS LCD, 16 triệu m&agrave;u</td>
		</tr>
		<tr>
			<td>K&iacute;ch cỡ m&agrave;n h&igrave;nh</td>
			<td>1536 x 2048 pixels, 7.9 inches, 324 ppi</td>
		</tr>
		<tr>
			<td>Kh&aacute;c</td>
			<td>- Cảm ứng đa điểm<br />
			- Apple Pay (Visa, MasterCard, AMEX certified)<br />
			- Mặt k&iacute;nh chống trầy xước, chống thấm<br />
			- Cảm biến gia tốc<br />
			- Cảm biến con quay hồi chuyển<br />
			- Cảm biến la b&agrave;n số<br />
			- Cảm biến dấu v&acirc;n tay (Touch ID)</td>
		</tr>
		<tr>
			<td colspan="2" style="background-color:#f0f0f0">&Acirc;m thanh</td>
		</tr>
		<tr>
			<td>Kiểu chu&ocirc;ng</td>
			<td>Kh&ocirc;ng hỗ trợ</td>
		</tr>
		<tr>
			<td>Ng&otilde; ra audio 3.5mm</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Kh&aacute;c</td>
			<td>- Stereo Speakers</td>
		</tr>
		<tr>
			<td colspan="2" style="background-color:#f0f0f0">Bộ nhớ</td>
		</tr>
		<tr>
			<td>Danh bạ</td>
			<td>Khả năng lưu c&aacute;c mục v&agrave; fields kh&ocirc;ng giới hạn, danh bạ h&igrave;nh ảnh</td>
		</tr>
		<tr>
			<td>C&aacute;c số đ&atilde; gọi</td>
			<td>Kh&ocirc;ng hỗ trợ</td>
		</tr>
		<tr>
			<td>Bộ nhớ trong</td>
			<td>64 GB, 1 GB RAM DDR3</td>
		</tr>
		<tr>
			<td>Khe cắm thẻ nhớ</td>
			<td>Kh&ocirc;ng</td>
		</tr>
		<tr>
			<td colspan="2" style="background-color:#f0f0f0">Truyền dữ liệu</td>
		</tr>
		<tr>
			<td>GPRS</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>EDGE</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Tốc độ 3G</td>
			<td>DC-HSDPA, 42 Mbps; HSDPA, 21 Mbps; HSUPA, 5.76 Mbps</td>
		</tr>
		<tr>
			<td>NFC</td>
			<td>Kh&ocirc;ng</td>
		</tr>
		<tr>
			<td>WLAN</td>
			<td>Wi-Fi 802.11 a/b/g/n, dual-band</td>
		</tr>
		<tr>
			<td>Bluetooth</td>
			<td>v4.0, A2DP, EDR</td>
		</tr>
		<tr>
			<td>USB</td>
			<td>Cổng Lightning</td>
		</tr>
		<tr>
			<td colspan="2" style="background-color:#f0f0f0">CHỤP ẢNH</td>
		</tr>
		<tr>
			<td>Camera ch&iacute;nh</td>
			<td>5 MP, 2592 х 1944 pixels, autofocus</td>
		</tr>
		<tr>
			<td>Đặc điểm</td>
			<td>Geo-tagging, chạm lấy n&eacute;t, nhận diện khu&ocirc;n mặt/nụ cười, HDR (photo/panorama)</td>
		</tr>
		<tr>
			<td>Quay phim</td>
			<td>1080p@30fps</td>
		</tr>
		<tr>
			<td>Camera phụ</td>
			<td>1.2 MP, 720p@30fps, nhận diện khu&ocirc;n mặt, FaceTime tr&ecirc;n Wi-Fi hoặc Cellular</td>
		</tr>
		<tr>
			<td colspan="2" style="background-color:#f0f0f0">ĐẶC ĐIỂM</td>
		</tr>
		<tr>
			<td>Hệ điều h&agrave;nh</td>
			<td>iOS 8.1</td>
		</tr>
		<tr>
			<td>Bộ xử l&yacute;</td>
			<td>Dual-core 1.3 GHz Cyclone (ARM v8-based), PowerVR G6430 (chip đồ họa l&otilde;i tứ)</td>
		</tr>
		<tr>
			<td>Chipset</td>
			<td>Apple A7</td>
		</tr>
		<tr>
			<td>Tin nhắn</td>
			<td>iMessage, SMS (threaded view), MMS, Email, Push Email</td>
		</tr>
		<tr>
			<td>Tr&igrave;nh duyệt</td>
			<td>HTML5 (Safari)</td>
		</tr>
		<tr>
			<td>Radio</td>
			<td>Kh&ocirc;ng</td>
		</tr>
		<tr>
			<td>Tr&ograve; chơi</td>
			<td>C&oacute;, c&oacute; thể tải th&ecirc;m</td>
		</tr>
		<tr>
			<td>M&agrave;u sắc</td>
			<td>Space Gray/Black, Silver/White, Gold</td>
		</tr>
		<tr>
			<td>Ng&ocirc;n ngữ</td>
			<td>Tiếng Anh, Tiếng Việt</td>
		</tr>
		<tr>
			<td>Định vị to&agrave;n cầu</td>
			<td>Hỗ trợ A-GPS v&agrave; GLONASSS</td>
		</tr>
		<tr>
			<td>Java</td>
			<td>Kh&ocirc;ng</td>
		</tr>
		<tr>
			<td>Kh&aacute;c</td>
			<td>- Sử dụng Nano-Sim<br />
			- Chống ồn với mic chuy&ecirc;n dụng<br />
			- Chia sẽ file AirDrop<br />
			- Ra lệnh bằng giọng n&oacute;i Siri<br />
			- Dịch vụ điện to&aacute;n đ&aacute;m m&acirc;y iCloud<br />
			- T&iacute;ch hợp Twitter v&agrave; Facebook<br />
			- Maps<br />
			- Xem / Chỉnh sửa h&igrave;nh ảnh<br />
			- Ghi &acirc;m giọng n&oacute;i<br />
			- TV-out<br />
			- Xem văn bản ((Word, Excel, PowerPoint)<br />
			- Nhập liệu đo&aacute;n trước từ</td>
		</tr>
		<tr>
			<td colspan="2" style="background-color:#f0f0f0">Pin</td>
		</tr>
		<tr>
			<td>Pin chuẩn</td>
			<td>Kh&ocirc;ng thể th&aacute;o rời Li-Po 6470 mAh (24.3 Wh)</td>
		</tr>
		<tr>
			<td>Thời gian chờ</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Thời gian thoại<br />
			&nbsp;</td>
		</tr>
	</tbody>
</table>
</div>
</div>
', N'<div class="tabs_sub_hidden tabs_sub_show" id="tab_tongquan">
<div style="padding:10xpx">&nbsp;&nbsp;&nbsp;<strong>Apple ra iPad Air 2 mỏng nhất thế giới, c&oacute; cảm biến v&acirc;n tay</strong>
<p>Tablet của Apple c&oacute; &quot;số đo&quot; 6,1 mm, trong khi phi&ecirc;n bản iPad Air l&agrave; 7,5 mm, v&agrave; Apple khẳng định đ&acirc;y l&agrave; m&aacute;y t&iacute;nh bảng mỏng nhất tr&ecirc;n thị trường.</p>

<p>Độ mỏng n&agrave;y đ&aacute;nh bại th&agrave;nh t&iacute;ch 6,4 mm của Sony Xperia Z3 Tablet Compact v&agrave; 6,6 mm của Galaxy Tab S. Dell cũng đ&atilde; giới&nbsp;thiệu Venue 8 chỉ 6 mm nhưng phải đến th&aacute;ng 11, m&aacute;y mới được b&aacute;n ra trong khi bộ đ&ocirc;i iPad mới đ&atilde; bắt đầu được xuất xưởng từ tuần sau.</p>

<p>Với độ mỏng giảm 18% so với phi&ecirc;n bản cũ, hai chiếc iPad Air 2 gộp lại vẫn mảnh mai hơn một chiếc iPad thế hệ đầu.&nbsp;Apple cho biết, để đạt được độ mỏng đ&aacute;ng kinh ngạc, m&agrave;n h&igrave;nh LCD, k&iacute;nh,&nbsp;cảm biến cảm ứng quang học được gắn liền v&agrave; kh&ocirc;ng c&oacute; kh&ocirc;ng kh&iacute; giữa c&aacute;c lớp. Ngo&agrave;i ra,&nbsp;m&agrave;n h&igrave;nh của iPad Air 2 được phủ th&ecirc;m lớp chống lo&aacute;, gi&uacute;p giảm độ lo&aacute; tới 56%.</p>

<p>B&ecirc;n cạnh đ&oacute;,&nbsp;tablet mới của Apple c&ograve;n được trang bị cảm biến nhận diện v&acirc;n tay Touch ID. Cảm biến n&agrave;y c&ograve;n hỗ trợ v&iacute; điện tử Apple Pay để cho ph&eacute;p người d&ugrave;ng thực hiện thanh to&aacute;n trong c&aacute;c ứng dụng v&agrave; tại c&aacute;c đại l&yacute;, cửa h&agrave;ng b&aacute;n lẻ đối t&aacute;c của Apple.</p>

<p>M&aacute;y cũng c&oacute; th&ecirc;m phi&ecirc;n bản m&agrave;u v&agrave;ng ngo&agrave;i m&agrave;u x&aacute;m v&agrave; trắng.</p>

<table align="center" border="0" cellpadding="3" cellspacing="0" class=" tplCaption" style="border-style:none; border-width:0px; color:#000000; font-size:13px; margin:0px; padding:0px; width:770px">
	<tbody>
		<tr>
			<td><img alt="apple413-3119-1413484396.jpg" src="http://m.f5.img.vnexpress.net/2014/10/17/apple413-3119-1413484396.jpg" style="width:764px" /></td>
		</tr>
		<tr>
			<td>
			<p><em>Ảnh: EnGadget.</em></p>
			</td>
		</tr>
	</tbody>
</table>

<p>Sản phẩm mới t&iacute;ch hợp chip A8X, một phi&ecirc;n bản tuỳ biến của chip trong iPhone 6 nhưng hiệu năng cao hơn. Đ&acirc;y l&agrave; chip 64-bit với tốc độ nhanh hơn 40% (nhưng Apple kh&ocirc;ng n&oacute;i r&otilde; nhanh hơn so với iPad Air hay so với sản phẩm đối thủ) v&agrave; c&oacute; tới 3 tỷ b&oacute;ng b&aacute;n dẫn, trong khi chip A8 trong iPhone 6 l&agrave; 2 tỷ b&oacute;ng b&aacute;n dẫn v&agrave; trong chip A7 l&agrave; 1 tỷ transistor.</p>

<p>Camera c&oacute; độ ph&acirc;n giải 8 megapixel, f/2.4, hỗ trợ quay video Full HD v&agrave; t&iacute;nh năng chụp ảnh to&agrave;n cảnh với độ ph&acirc;n giải 43 megapixel. iPad Air 2 c&ograve;n c&oacute; thể quay phim slow motion v&agrave; timelapse.</p>

<p>Sản phẩm được trang bị Wi-Fi 802.11ac tốc độ nhanh hơn phi&ecirc;n bản trước đ&oacute; 2,8 lần. iPad Air 2 c&oacute; thể hoạt động li&ecirc;n tục 10 tiếng.</p>

<p>&nbsp;</p>

<p>Năm nay, Apple kh&ocirc;ng d&agrave;nh nhiều thời gian để n&oacute;i về iPad Mini 3, v&agrave; nhiều người tin rằng đ&oacute; l&agrave; v&igrave; h&atilde;ng n&agrave;y đ&atilde; c&oacute; một sản phẩm kh&aacute;c đ&aacute;ng ch&uacute; &yacute; hơn: iPhone 6 Plus. Chiếc smartphone 5,5 inch n&agrave;y ho&agrave;n to&agrave;n c&oacute; thể đ&oacute;ng vai tr&ograve; của một tablet cỡ nhỏ v&agrave; hứa hẹn sẽ phần n&agrave;o ảnh hưởng tới doanh số của iPad Mini. Tuy nhi&ecirc;n, Apple vẫn duy tr&igrave; c&aacute;c d&ograve;ng sản phẩm để người d&ugrave;ng c&oacute; th&ecirc;m nhiều lựa chọn v&agrave; để tr&aacute;nh bị c&aacute;c đối thủ &quot;lấn s&acirc;n&quot;.</p>

<p>Người sử dụng c&oacute; thể bắt đầu đặt h&agrave;ng bộ đ&ocirc;i iPad mới từ ng&agrave;y 17/10 v&agrave; sản phẩm sẽ c&oacute; mặt tr&ecirc;n thị trường v&agrave;o&nbsp;tuần sau.&nbsp;</p>

<p style="text-align:right">Theo:&nbsp;Số h&oacute;a</p>
</div>
</div>
', N'/UploadFile/images/Ipad/Sonymini3iPad7_lon251014021918.png', 6, 0, NULL, NULL, NULL, 1, 1, 2, 3, 0, N'["/UploadFile/images/Ipad/Sonymini3iPad1_lon251014021825.png","/UploadFile/images/Ipad/Sonymini3iPad2_lon251014021832.png","/UploadFile/images/Ipad/Sonymini3iPad3_lon251014021841.png","/UploadFile/images/Ipad/Sonymini3iPad4_lon251014021849.png","/UploadFile/images/Ipad/Sonymini3iPad5_lon251014021857.png","/UploadFile/images/Ipad/Sonymini3iPad6_lon251014021905.png","/UploadFile/images/Ipad/Sonymini3iPad_lon251014021817.png"]', 1)
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [DonGia], [NgayCapNhap], [CauHinh], [MoTa], [HinhAnh], [SoLuongTon], [LuotXem], [LuotBinhChon], [LuotBinhLuan], [SoLanMua], [Moi], [MaNCC], [MaNSX], [MaLoaiSP], [DaXoa], [HinhAnh1], [Home]) VALUES (20, N'iPad Mini 3 wifi+4G (16GB)', CAST(7599000 AS Decimal(18, 0)), CAST(0x0000A7680166B96B AS DateTime), N'<div class="tabs_sub_hidden tabs_sub_show" id="tab_tinhnang">
<div style="padding:10xpx">
<table cellpadding="5" cellspacing="0" class=" infoDetail table" style="border-left-color:#cccccc; border-left-style:solid; border-left-width:1px; border-top-color:#cccccc; border-top-style:solid; border-top-width:1px; color:#333333; font-family:Roboto,Arial,Helvetica,sans-serif; font-size:14px; width:100%">
	<tbody>
		<tr>
		</tr>
		<tr>
			<td colspan="2" style="background-color:#f0f0f0">Tổng quan</td>
		</tr>
		<tr>
			<td>Điện thoại,SMS</td>
			<td>Kh&ocirc;ng</td>
		</tr>
		<tr>
			<td>Mạng 2G</td>
			<td>GSM 850 / 900 / 1800 / 1900</td>
		</tr>
		<tr>
			<td>Mạng 3G</td>
			<td>HSDPA 850 / 900 / 1900 / 2100</td>
		</tr>
		<tr>
			<td>Ra mắt</td>
			<td>Th&aacute;ng 10 năm 2014</td>
		</tr>
		<tr>
			<td colspan="2" style="background-color:#f0f0f0">K&iacute;ch thước</td>
		</tr>
		<tr>
			<td>K&iacute;ch thước</td>
			<td>200 x 134.7 x 7.5 mm</td>
		</tr>
		<tr>
			<td>Trọng lượng</td>
			<td>341g</td>
		</tr>
		<tr>
			<td colspan="2" style="background-color:#f0f0f0">Hiển thị</td>
		</tr>
		<tr>
			<td>Loại</td>
			<td>M&agrave;n h&igrave;nh cảm ứng điện dung LED-backlit IPS LCD, 16 triệu m&agrave;u</td>
		</tr>
		<tr>
			<td>K&iacute;ch cỡ m&agrave;n h&igrave;nh</td>
			<td>1536 x 2048 pixels, 7.9 inches, 324 ppi</td>
		</tr>
		<tr>
			<td>Kh&aacute;c</td>
			<td>- Cảm ứng đa điểm<br />
			- Apple Pay (Visa, MasterCard, AMEX certified)<br />
			- Mặt k&iacute;nh chống trầy xước, chống thấm<br />
			- Cảm biến gia tốc<br />
			- Cảm biến con quay hồi chuyển<br />
			- Cảm biến la b&agrave;n số<br />
			- Cảm biến dấu v&acirc;n tay (Touch ID)</td>
		</tr>
		<tr>
			<td colspan="2" style="background-color:#f0f0f0">&Acirc;m thanh</td>
		</tr>
		<tr>
			<td>Kiểu chu&ocirc;ng</td>
			<td>Kh&ocirc;ng hỗ trợ</td>
		</tr>
		<tr>
			<td>Ng&otilde; ra audio 3.5mm</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Kh&aacute;c</td>
			<td>- Stereo Speakers</td>
		</tr>
		<tr>
			<td colspan="2" style="background-color:#f0f0f0">Bộ nhớ</td>
		</tr>
		<tr>
			<td>Danh bạ</td>
			<td>Khả năng lưu c&aacute;c mục v&agrave; fields kh&ocirc;ng giới hạn, danh bạ h&igrave;nh ảnh</td>
		</tr>
		<tr>
			<td>C&aacute;c số đ&atilde; gọi</td>
			<td>Kh&ocirc;ng hỗ trợ</td>
		</tr>
		<tr>
			<td>Bộ nhớ trong</td>
			<td>16 GB, 1 GB RAM DDR3</td>
		</tr>
		<tr>
			<td>Khe cắm thẻ nhớ</td>
			<td>Kh&ocirc;ng</td>
		</tr>
		<tr>
			<td colspan="2" style="background-color:#f0f0f0">Truyền dữ liệu</td>
		</tr>
		<tr>
			<td>GPRS</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>EDGE</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Tốc độ 3G</td>
			<td>DC-HSDPA, 42 Mbps; HSDPA, 21 Mbps; HSUPA, 5.76 Mbps</td>
		</tr>
		<tr>
			<td>NFC</td>
			<td>Kh&ocirc;ng</td>
		</tr>
		<tr>
			<td>WLAN</td>
			<td>Wi-Fi 802.11 a/b/g/n, dual-band</td>
		</tr>
		<tr>
			<td>Bluetooth</td>
			<td>v4.0, A2DP, EDR</td>
		</tr>
		<tr>
			<td>USB</td>
			<td>Cổng Lightning</td>
		</tr>
		<tr>
			<td colspan="2" style="background-color:#f0f0f0">CHỤP ẢNH</td>
		</tr>
		<tr>
			<td>Camera ch&iacute;nh</td>
			<td>5 MP, 2592 х 1944 pixels, autofocus</td>
		</tr>
		<tr>
			<td>Đặc điểm</td>
			<td>Geo-tagging, chạm lấy n&eacute;t, nhận diện khu&ocirc;n mặt/nụ cười, HDR (photo/panorama)</td>
		</tr>
		<tr>
			<td>Quay phim</td>
			<td>1080p@30fps</td>
		</tr>
		<tr>
			<td>Camera phụ</td>
			<td>1.2 MP, 720p@30fps, nhận diện khu&ocirc;n mặt, FaceTime tr&ecirc;n Wi-Fi hoặc Cellular</td>
		</tr>
		<tr>
			<td colspan="2" style="background-color:#f0f0f0">ĐẶC ĐIỂM</td>
		</tr>
		<tr>
			<td>Hệ điều h&agrave;nh</td>
			<td>iOS 8.1</td>
		</tr>
		<tr>
			<td>Bộ xử l&yacute;</td>
			<td>Dual-core 1.3 GHz Cyclone (ARM v8-based), PowerVR G6430 (chip đồ họa l&otilde;i tứ)</td>
		</tr>
		<tr>
			<td>Chipset</td>
			<td>Apple A7</td>
		</tr>
		<tr>
			<td>Tin nhắn</td>
			<td>iMessage, SMS (threaded view), MMS, Email, Push Email</td>
		</tr>
		<tr>
			<td>Tr&igrave;nh duyệt</td>
			<td>HTML5 (Safari)</td>
		</tr>
		<tr>
			<td>Radio</td>
			<td>Kh&ocirc;ng</td>
		</tr>
		<tr>
			<td>Tr&ograve; chơi</td>
			<td>C&oacute;, c&oacute; thể tải th&ecirc;m</td>
		</tr>
		<tr>
			<td>M&agrave;u sắc</td>
			<td>Space Gray/Black, Silver/White, Gold</td>
		</tr>
		<tr>
			<td>Ng&ocirc;n ngữ</td>
			<td>Tiếng Anh, Tiếng Việt</td>
		</tr>
		<tr>
			<td>Định vị to&agrave;n cầu</td>
			<td>Hỗ trợ A-GPS v&agrave; GLONASSS</td>
		</tr>
		<tr>
			<td>Java</td>
			<td>Kh&ocirc;ng</td>
		</tr>
		<tr>
			<td>Kh&aacute;c</td>
			<td>- Sử dụng Nano-Sim<br />
			- Chống ồn với mic chuy&ecirc;n dụng<br />
			- Chia sẽ file AirDrop<br />
			- Ra lệnh bằng giọng n&oacute;i Siri<br />
			- Dịch vụ điện to&aacute;n đ&aacute;m m&acirc;y iCloud<br />
			- T&iacute;ch hợp Twitter v&agrave; Facebook<br />
			- Maps<br />
			- Xem / Chỉnh sửa h&igrave;nh ảnh<br />
			- Ghi &acirc;m giọng n&oacute;i<br />
			- TV-out<br />
			- Xem văn bản ((Word, Excel, PowerPoint)<br />
			- Nhập liệu đo&aacute;n trước từ</td>
		</tr>
		<tr>
			<td colspan="2" style="background-color:#f0f0f0">Pin</td>
		</tr>
		<tr>
			<td>Pin chuẩn</td>
			<td>Kh&ocirc;ng thể th&aacute;o rời Li-Po 6470 mAh (24.3 Wh)</td>
		</tr>
		<tr>
			<td>Thời gian chờ</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Thời gian thoại<br />
			&nbsp;</td>
		</tr>
	</tbody>
</table>
</div>
</div>
', N'<div class="tabs_sub_hidden tabs_sub_show" id="tab_tongquan">
<div style="padding:10xpx">&nbsp;&nbsp;&nbsp;<strong>Apple ra iPad Air 2 mỏng nhất thế giới, c&oacute; cảm biến v&acirc;n tay</strong>
<p>Tablet của Apple c&oacute; &quot;số đo&quot; 6,1 mm, trong khi phi&ecirc;n bản iPad Air l&agrave; 7,5 mm, v&agrave; Apple khẳng định đ&acirc;y l&agrave; m&aacute;y t&iacute;nh bảng mỏng nhất tr&ecirc;n thị trường.</p>

<p>Độ mỏng n&agrave;y đ&aacute;nh bại th&agrave;nh t&iacute;ch 6,4 mm của Sony Xperia Z3 Tablet Compact v&agrave; 6,6 mm của Galaxy Tab S. Dell cũng đ&atilde; giới&nbsp;thiệu Venue 8 chỉ 6 mm nhưng phải đến th&aacute;ng 11, m&aacute;y mới được b&aacute;n ra trong khi bộ đ&ocirc;i iPad mới đ&atilde; bắt đầu được xuất xưởng từ tuần sau.</p>

<p>Với độ mỏng giảm 18% so với phi&ecirc;n bản cũ, hai chiếc iPad Air 2 gộp lại vẫn mảnh mai hơn một chiếc iPad thế hệ đầu.&nbsp;Apple cho biết, để đạt được độ mỏng đ&aacute;ng kinh ngạc, m&agrave;n h&igrave;nh LCD, k&iacute;nh,&nbsp;cảm biến cảm ứng quang học được gắn liền v&agrave; kh&ocirc;ng c&oacute; kh&ocirc;ng kh&iacute; giữa c&aacute;c lớp. Ngo&agrave;i ra,&nbsp;m&agrave;n h&igrave;nh của iPad Air 2 được phủ th&ecirc;m lớp chống lo&aacute;, gi&uacute;p giảm độ lo&aacute; tới 56%.</p>

<p>B&ecirc;n cạnh đ&oacute;,&nbsp;tablet mới của Apple c&ograve;n được trang bị cảm biến nhận diện v&acirc;n tay Touch ID. Cảm biến n&agrave;y c&ograve;n hỗ trợ v&iacute; điện tử Apple Pay để cho ph&eacute;p người d&ugrave;ng thực hiện thanh to&aacute;n trong c&aacute;c ứng dụng v&agrave; tại c&aacute;c đại l&yacute;, cửa h&agrave;ng b&aacute;n lẻ đối t&aacute;c của Apple.</p>

<p>M&aacute;y cũng c&oacute; th&ecirc;m phi&ecirc;n bản m&agrave;u v&agrave;ng ngo&agrave;i m&agrave;u x&aacute;m v&agrave; trắng.</p>

<table align="center" border="0" cellpadding="3" cellspacing="0" class=" tplCaption" style="border-style:none; border-width:0px; color:#000000; font-size:13px; margin:0px; padding:0px; width:770px">
	<tbody>
		<tr>
			<td><img alt="apple413-3119-1413484396.jpg" src="http://m.f5.img.vnexpress.net/2014/10/17/apple413-3119-1413484396.jpg" style="width:764px" /></td>
		</tr>
		<tr>
			<td>
			<p><em>Ảnh: EnGadget.</em></p>
			</td>
		</tr>
	</tbody>
</table>

<p>Sản phẩm mới t&iacute;ch hợp chip A8X, một phi&ecirc;n bản tuỳ biến của chip trong iPhone 6 nhưng hiệu năng cao hơn. Đ&acirc;y l&agrave; chip 64-bit với tốc độ nhanh hơn 40% (nhưng Apple kh&ocirc;ng n&oacute;i r&otilde; nhanh hơn so với iPad Air hay so với sản phẩm đối thủ) v&agrave; c&oacute; tới 3 tỷ b&oacute;ng b&aacute;n dẫn, trong khi chip A8 trong iPhone 6 l&agrave; 2 tỷ b&oacute;ng b&aacute;n dẫn v&agrave; trong chip A7 l&agrave; 1 tỷ transistor.</p>

<p>Camera c&oacute; độ ph&acirc;n giải 8 megapixel, f/2.4, hỗ trợ quay video Full HD v&agrave; t&iacute;nh năng chụp ảnh to&agrave;n cảnh với độ ph&acirc;n giải 43 megapixel. iPad Air 2 c&ograve;n c&oacute; thể quay phim slow motion v&agrave; timelapse.</p>

<p>Sản phẩm được trang bị Wi-Fi 802.11ac tốc độ nhanh hơn phi&ecirc;n bản trước đ&oacute; 2,8 lần. iPad Air 2 c&oacute; thể hoạt động li&ecirc;n tục 10 tiếng.</p>

<p>&nbsp;</p>

<p>Năm nay, Apple kh&ocirc;ng d&agrave;nh nhiều thời gian để n&oacute;i về iPad Mini 3, v&agrave; nhiều người tin rằng đ&oacute; l&agrave; v&igrave; h&atilde;ng n&agrave;y đ&atilde; c&oacute; một sản phẩm kh&aacute;c đ&aacute;ng ch&uacute; &yacute; hơn: iPhone 6 Plus. Chiếc smartphone 5,5 inch n&agrave;y ho&agrave;n to&agrave;n c&oacute; thể đ&oacute;ng vai tr&ograve; của một tablet cỡ nhỏ v&agrave; hứa hẹn sẽ phần n&agrave;o ảnh hưởng tới doanh số của iPad Mini. Tuy nhi&ecirc;n, Apple vẫn duy tr&igrave; c&aacute;c d&ograve;ng sản phẩm để người d&ugrave;ng c&oacute; th&ecirc;m nhiều lựa chọn v&agrave; để tr&aacute;nh bị c&aacute;c đối thủ &quot;lấn s&acirc;n&quot;.</p>

<p>Người sử dụng c&oacute; thể bắt đầu đặt h&agrave;ng bộ đ&ocirc;i iPad mới từ ng&agrave;y 17/10 v&agrave; sản phẩm sẽ c&oacute; mặt tr&ecirc;n thị trường v&agrave;o&nbsp;tuần sau.&nbsp;</p>

<p style="text-align:right">Theo:&nbsp;Số h&oacute;a</p>
</div>
</div>
', N'/UploadFile/images/Ipad/Sonymini3iPad7_lon251014021918.png', 7, 0, NULL, NULL, NULL, 1, 1, 2, 3, 0, N'["/UploadFile/images/Ipad/Sonymini3iPad1_lon251014021825.png","/UploadFile/images/Ipad/Sonymini3iPad2_lon251014021832.png","/UploadFile/images/Ipad/Sonymini3iPad3_lon251014021841.png","/UploadFile/images/Ipad/Sonymini3iPad4_lon251014021849.png","/UploadFile/images/Ipad/Sonymini3iPad5_lon251014021857.png","/UploadFile/images/Ipad/Sonymini3iPad6_lon251014021905.png","/UploadFile/images/Ipad/Sonymini3iPad7_lon251014021918.png","/UploadFile/images/Ipad/Sonymini3iPad_lon251014021817.png"]', 1)
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [DonGia], [NgayCapNhap], [CauHinh], [MoTa], [HinhAnh], [SoLuongTon], [LuotXem], [LuotBinhChon], [LuotBinhLuan], [SoLanMua], [Moi], [MaNCC], [MaNSX], [MaLoaiSP], [DaXoa], [HinhAnh1], [Home]) VALUES (21, N'iPad Mini 4 wifi (32GB)', CAST(9700000 AS Decimal(18, 0)), CAST(0x0000A76801688B48 AS DateTime), N'<div class="detail-col-left53 left">
<h3>Th&ocirc;ng số kỹ thuật</h3>

<ul>
	<li>Th&ocirc;ng số cơ bản</li>
	<li>M&agrave;n h&igrave;nh : 7.9 inch(2048 x 1536 pixels)</li>
	<li>Vi xử l&yacute; CPU : Apple A8</li>
	<li>Chip đồ họa (GPU) : PowerVR GXA6450</li>
	<li>RAM : 2GB DDR3</li>
	<li>Bộ nhớ trong : 32 GB</li>
	<li>Camera : Camera sau: 8MP Camera trước: 1.2MP</li>
	<li>Kết nối : 4G, 3G, Bluetooth, Wi-Fi</li>
	<li>Thời gian sử dụng : 9 giờ</li>
	<li>Hệ điều h&agrave;nh : iOS 9</li>
</ul>

<ul>
	<li>M&agrave;n h&igrave;nh</li>
	<li>C&ocirc;ng nghệ m&agrave;n h&igrave;nh : LED backlit LCD</li>
	<li>M&agrave;u m&agrave;n h&igrave;nh : 16 triệu m&agrave;u</li>
	<li>Độ ph&acirc;n giải : 1536 x 2048 pixels</li>
	<li>K&iacute;ch thước m&agrave;n h&igrave;nh : 7.9 inch</li>
	<li>C&ocirc;ng nghệ cảm ứng : Điện dung đa điểm</li>
	<li>Cấu h&igrave;nh phần cứng</li>
	<li>Loại CPU (Chipset) : Apple A8</li>
	<li>Tốc độ CPU : 1.5 GHz</li>
	<li>Số nh&acirc;n : 2 Nh&acirc;n</li>
	<li>Chip đồ hoạ (GPU) : PowerVR GXA6450</li>
	<li>RAM : 2 GB</li>
	<li>Cảm biến : La b&agrave;n, Con quay hồi chuyển 3 chiều, Gia tốc, &Aacute;nh s&aacute;ng, Fingerprint Sensor</li>
	<li>Bộ nhớ &amp; Lưu trữ</li>
	<li>Bộ nhớ trong : 32 GB</li>
	<li>Thẻ nhớ ngo&agrave;i : Kh&ocirc;ng</li>
	<li>Hỗ trợ thẻ nhớ tối đa : Kh&ocirc;ng</li>
	<li>Chụp h&igrave;nh &amp; Quay phim</li>
	<li>Camera sau : 8 MP</li>
	<li>Camera trước : 1.2 MP</li>
	<li>Đ&egrave;n Flash : Kh&ocirc;ng</li>
	<li>T&iacute;nh năng camera : Gắn thẻ địa lý, Tự đ&ocirc;̣ng l&acirc;́y nét</li>
	<li>Quay Phim : 1080p@30fps</li>
	<li>Kết nối &amp; Cổng giao tiếp</li>
	<li>Số khe sim : 1 Sim</li>
	<li>Loại Sim : Nano-SIM</li>
	<li>Thực hiện cuộc gọi : FaceTime</li>
	<li>Hỗ trợ 3G : HSDPA 850 / 900 / 1900 / 2100</li>
	<li>Hỗ trợ 4G : LTE</li>
	<li>Wifi : Wi-Fi 802.11 a/b/g/n</li>
	<li>Bluetooth : v 4.2</li>
	<li>GPS : A-GPS support; GLONASS</li>
	<li>Cổng kết nối/ sạc : Lighting</li>
	<li>Jack (Input &amp; Output) : 3.5 mm</li>
	<li>Hỗ trợ OTG : C&oacute;</li>
	<li>Kết nối kh&aacute;c : Kh&ocirc;ng</li>
	<li>Giải tr&iacute; &amp; Ứng dụng</li>
	<li>Xem phim : MPEG-4, H.264, FLV, AAC, 3GP, JPEG, GIF, Divx, WMV, AMR, WMA, MP4, AVI</li>
	<li>Nghe nhạc : WAVE, WAV, MP3, AAC+, WMA, AAC, MIDI</li>
	<li>Ghi &acirc;m : C&oacute;</li>
	<li>C&ocirc;ng vụ văn ph&ograve;ng : Hỗ trợ ứng dụng văn ph&ograve;ng</li>
	<li>Ứng dụng kh&aacute;c : Facebook, Game, Lịch, Đồng hồ, B&aacute;o thức, Bản đồ, Mail, Sổ tay</li>
	<li>Thiết kế &amp; Trọng lượng</li>
	<li>Chất liệu : Hợp kim nh&ocirc;m</li>
	<li>K&iacute;ch thước (DxRxC) : 203.2 x 134.8 x 6.1 mm</li>
	<li>Trọng lượng : 299g</li>
	<li>Pin &amp; Dung lượng</li>
	<li>Loại pin : Lithium - Polymer</li>
	<li>Dung lượng pin : 19.1 Wh (Khoảng 5124 mAh)</li>
	<li>Thời gian sử dụng : 9 giờ</li>
	<li>Thời gian sạc đầy : ~ 3 giờ</li>
	<li>Bảo h&agrave;nh</li>
	<li>Thời gian bảo h&agrave;nh : 12 Th&aacute;ng</li>
</ul>
</div>
', N'<h2 style="margin-left:0px; margin-right:0px">iPad Mini 4 Wifi 32GB với thiết kế sang trọng c&ugrave;ng dung lượng pin lớn sẽ gi&uacute;p bạn l&agrave;m việc v&agrave; giải tr&iacute; mọi l&uacute;c mọi nơi.</h2>

<h3 style="margin-left:0px; margin-right:0px"><strong>Nhẹ hơn v&agrave; mỏng hơn</strong></h3>

<p style="margin-left:0px; margin-right:0px">Với khả năng gia c&ocirc;ng cao cấp của Apple, iPad Mini 4 được l&agrave;m rất gọn g&agrave;ng v&agrave; hiện đại với th&acirc;n h&igrave;nh vẫn rất sang chảnh nhờ chất liệu kim loại sang trọng.</p>

<p style="margin-left:0px; margin-right:0px"><img alt="iPad Mini 4 (giữa) có bề dày mỏng hơn (bên trên là iPad Mini 2)" src="https://www.thegioididong.com/images/522/73089/ipad-mini-4-cellular-16gb-11-1.jpg" title="iPad Mini 4 (giữa) có bề dày mỏng hơn (bên trên là iPad Mini 2)" /></p>

<p style="margin-left:0px; margin-right:0px"><em>iPad Mini 4 (giữa) c&oacute; bề d&agrave;y mỏng hơn (b&ecirc;n tr&ecirc;n l&agrave;&nbsp;<a href="https://www.thegioididong.com/may-tinh-bang/ipad-mini-2-retina-16g-wifi" style="margin: 0px; padding: 0px; text-decoration: none; color: rgb(80, 168, 227);" target="_blank" title="Máy tính bảng iPad Mini 2 Retina 16G/Wifi">iPad Mini 2</a>)</em></p>

<p style="margin-left:0px; margin-right:0px"><em><img alt="Các đường cắt vát được làm rất tinh tế và sang trọng, nút gạt chế độ âm thanh đã được loại bỏ trên phiên bản iPad Mini 4 này" src="https://www.thegioididong.com/images/522/73089/ipad-mini-4-cellular-16gb-12-1.jpg" title="Các đường cắt vát được làm rất tinh tế và sang trọng, nút gạt chế độ âm thanh đã được loại bỏ trên phiên bản iPad Mini 4 này" /></em></p>

<p style="margin-left:0px; margin-right:0px"><em>C&aacute;c đường cắt v&aacute;t được l&agrave;m rất tinh tế v&agrave; sang trọng, n&uacute;t gạt chế độ &acirc;m thanh đ&atilde; được loại bỏ tr&ecirc;n phi&ecirc;n bản iPad Mini 4 n&agrave;y</em></p>

<p style="margin-left:0px; margin-right:0px"><img alt="iPad Mini 4 được đục 1 hàng loa thay vì 2 hàng 2 loa như thế hệ trước" src="https://www.thegioididong.com/images/522/73089/ipad-mini-46-1.jpg" /></p>

<p style="margin-left:0px; margin-right:0px"><em>iPad Mini 4 được đục 1 h&agrave;ng loa thay v&igrave; 2 h&agrave;ng 2 loa như thế hệ trước</em></p>

<h3 style="margin-left:0px; margin-right:0px"><strong>Đẹp v&agrave; sắc n&eacute;t</strong></h3>

<p style="margin-left:0px; margin-right:0px">Mặt trước iPad Mini 4 sở hữu m&agrave;n h&igrave;nh k&iacute;ch thước 7.9 inch, độ ph&acirc;n giải 1536 x 2048 pixels, nội dung&nbsp;hiển thị&nbsp;kh&aacute; thoải m&aacute;i để quan s&aacute;t, c&aacute;c trang web, văn bản rộng, đầy đủ.</p>

<p style="margin-left:0px; margin-right:0px"><em><img alt="Góc nhìn màn hình rất rộng và thoải mái" src="https://www.thegioididong.com/images/522/73089/ipad-mini-48.jpg" title="Góc nhìn màn hình rất rộng và thoải mái" /></em></p>

<p style="margin-left:0px; margin-right:0px"><em>G&oacute;c nh&igrave;n m&agrave;n h&igrave;nh rất rộng v&agrave; thoải m&aacute;i</em></p>

<p style="margin-left:0px; margin-right:0px">C&ocirc;ng nghệ một lớp m&agrave;n h&igrave;nh được&nbsp;<a href="https://www.thegioididong.com/may-tinh-bang-apple-ipad" style="margin: 0px; padding: 0px; text-decoration: none; color: rgb(80, 168, 227);" target="_blank" title="Máy tính bảng Apple">Apple</a>&nbsp;sử dụng tr&ecirc;n iPad mini 4, nhờ đ&oacute; th&acirc;n m&aacute;y mỏng hơn, đem đến chất lượng h&igrave;nh ảnh sắc n&eacute;t hơn, độ s&aacute;ng cao v&agrave; chống l&oacute;a tốt.</p>

<p style="margin-left:0px; margin-right:0px"><img alt="Công nghệ màn hình" src="https://www.thegioididong.com/images/522/73089/ipad-mini-49.jpg" title="Công nghệ màn hình" /></p>

<p style="margin-left:0px; margin-right:0px">Mẹo l&agrave;m chủ m&agrave;n h&igrave;nh iPad:</p>

<p style="margin-left:0px; margin-right:0px">- Bạn v&agrave;o&nbsp;<strong>c&agrave;i đặt &gt; c&agrave;i đặt chung &gt; trợ năng &gt; chọn thang m&agrave;u x&aacute;m</strong>&nbsp;để m&agrave;n h&igrave;nh của m&aacute;y đưa về 2 m&agrave;u đen trắng, gi&uacute;p tiết kiệm pin hơn.</p>

<p style="margin-left:0px; margin-right:0px">- Việc đọc chữ đen tr&ecirc;n nền trắng sẽ l&agrave;m bạn dễ bị đau mắt, h&atilde;y sử dụng chức năng&nbsp;<strong>đảo m&agrave;u</strong>&nbsp;trong&nbsp;trợ năng&nbsp;để đưa m&aacute;y về nền đen to&agrave;n bộ.</p>

<h3 style="margin-left:0px; margin-right:0px"><strong>Hệ điều h&agrave;nh mới, vi xử l&yacute; cũ nhưng rất mạnh mẽ với RAM 2 GB</strong></h3>

<p style="margin-left:0px; margin-right:0px">iPad Mini 4&nbsp;được chip&nbsp;Apple A8 nhưng với&nbsp;mức RAM 2 GB tăng cường khả năng hoạt động đa nhiệm tốt hơn.&nbsp;iPad Mini 4 c&oacute; tốc độ CPU nhanh hơn 1.3 lần, tốc độ GPU hơn 1.6 lần.</p>

<p style="margin-left:0px; margin-right:0px">&nbsp;</p>

<div class="video" style="font-family:Helvetica,Arial,; padding:0px"><iframe frameborder="0" src="https://www.youtube.com/embed/Yyl9IDWRr2Q?rel=0"></iframe></div>

<p style="margin-left:0px; margin-right:0px">&nbsp;</p>

<p style="margin-left:0px; margin-right:0px"><em>Video test tốc độ của iPad Mini 4 với&nbsp;<a href="https://www.thegioididong.com/may-tinh-bang/ipad-air-2-cellular" style="margin: 0px; padding: 0px; text-decoration: none; color: rgb(80, 168, 227);" target="_blank" title="Máy tính bảng iPad Air 2 Cellular 16GB">iPad Air 2</a>&nbsp;v&agrave; iPad Mini 2 (iPad Mini 4 ở giữa)</em></p>

<p style="margin-left:0px; margin-right:0px">M&aacute;y c&oacute; khả năng chạy đa nhiệm chia đ&ocirc;i m&agrave;n h&igrave;nh gi&uacute;p bạn xem được nhiều nội dung hơn trong c&ugrave;ng một thời điểm.</p>

<p style="margin-left:0px; margin-right:0px"><img alt="Tính năng đa nhiệm, chia đôi màn hình, video thoại không lạ với Android nhưng rất hữu dụng với iPad" src="https://www.thegioididong.com/images/522/73089/ipad-mini-411.jpg" title="Tính năng đa nhiệm, chia đôi màn hình, video thoại không lạ với Android nhưng rất hữu dụng với iPad" /></p>

<p style="margin-left:0px; margin-right:0px"><em>T&iacute;nh năng đa nhiệm, chia đ&ocirc;i m&agrave;n h&igrave;nh, video thoại kh&ocirc;ng lạ với Android nhưng rất hữu dụng với iPad</em></p>

<h3 style="margin-left:0px; margin-right:0px"><strong>Camera iSight 8 MP</strong></h3>

<p style="margin-left:0px; margin-right:0px">So với 5 MP như thế hệ trước, 8 MP tr&ecirc;n iPad Mini 4 đ&atilde; mạnh mẽ hơn nhiều với chi tiết h&igrave;nh ảnh cao hơn, hỗ trợ chụp ảnh HDR, chụp li&ecirc;n tục, quay video chuyển động chậm Slow Motion.</p>

<p style="margin-left:0px; margin-right:0px"><img alt="Tốc độ chụp trên các thiết bị của Apple vẫn rất nhanh và có khả năng lấy nét rất chính xác" src="https://www.thegioididong.com/images/522/73089/ipad-mini-4-cellular-16gb-5.jpg" title="Tốc độ chụp trên các thiết bị của Apple vẫn rất nhanh và có khả năng lấy nét rất chính xác" /></p>

<p style="margin-left:0px; margin-right:0px"><em>Tốc độ chụp tr&ecirc;n c&aacute;c thiết bị của Apple vẫn rất nhanh v&agrave; c&oacute; khả năng lấy n&eacute;t rất ch&iacute;nh x&aacute;c</em></p>

<h3 style="margin-left:0px; margin-right:0px"><strong>Nhỏ hơn nhưng tốt hơn</strong></h3>

<p style="margin-left:0px; margin-right:0px">Mặc d&ugrave; c&oacute; mức pin chỉ 5124 mAh, tuy nhi&ecirc;n khả năng quản l&yacute; pin của hệ điều h&agrave;nh mới l&agrave; tốt hơn do đ&oacute; pin sử dụng thậm ch&iacute; c&ograve;n được k&eacute;o d&agrave;i hơn so với thế hệ trước.</p>

<p style="margin-left:0px; margin-right:0px"><img alt="Thời gian sử dụng pin thoải mái trong nhiều ngày" src="https://www.thegioididong.com/images/522/89540/ipad-mini-4-wifi-32gb-1.jpg" title="Thời gian sử dụng pin thoải mái trong nhiều ngày" /></p>

<p style="margin-left:0px; margin-right:0px"><em>Thời gian sử dụng pin thoải m&aacute;i trong nhiều ng&agrave;y</em></p>

<p style="margin-left:0px; margin-right:0px">iPad Mini 4 l&agrave; một sản phẩm mới được n&acirc;ng cấp kh&aacute; nhiều so với phi&ecirc;n bản cũ, tuy sự n&acirc;ng cấp chỉ tập trung v&agrave;o hệ điều h&agrave;nh v&agrave; camera, c&aacute;c trải nghiệm tr&ecirc;n thiết bị kh&ocirc;ng nghi ngờ g&igrave;, vẫn rất cao cấp v&agrave; đ&aacute;ng gi&aacute;.</p>
', N'/UploadFile/images/Ipad/SonyiPadmini4BB3_lon220915025701.png', 11, 0, NULL, NULL, NULL, 1, 1, 2, 3, 0, N'["/UploadFile/images/Ipad/SonyiPadmini4BB1_lon220915025648.png","/UploadFile/images/Ipad/SonyiPadmini4BB2_lon220915024747.png","/UploadFile/images/Ipad/SonyiPadmini4BB_lon220915024728.png","/UploadFile/images/Ipad/SonyiPadmini4BB7_lon220915024818.png","/UploadFile/images/Ipad/SonyiPadmini4BB6_lon220915024812.png","/UploadFile/images/Ipad/SonyiPadmini4BB5_lon220915024806.png"]', 1)
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [DonGia], [NgayCapNhap], [CauHinh], [MoTa], [HinhAnh], [SoLuongTon], [LuotXem], [LuotBinhChon], [LuotBinhLuan], [SoLanMua], [Moi], [MaNCC], [MaNSX], [MaLoaiSP], [DaXoa], [HinhAnh1], [Home]) VALUES (22, N'iPad Mini 4 wifi+4G (32GB)', CAST(11899000 AS Decimal(18, 0)), CAST(0x0000A78E00D28454 AS DateTime), N'<div class="detail-col-left53 left">
<h3>Th&ocirc;ng số kỹ thuật</h3>

<ul>
	<li>Th&ocirc;ng số cơ bản</li>
	<li>M&agrave;n h&igrave;nh : 7.9 inch(2048 x 1536 pixels)</li>
	<li>Vi xử l&yacute; CPU : Apple A8</li>
	<li>Chip đồ họa (GPU) : PowerVR GXA6450</li>
	<li>RAM : 2GB DDR3</li>
	<li>Bộ nhớ trong : 32 GB</li>
	<li>Camera : Camera sau: 8MP Camera trước: 1.2MP</li>
	<li>Kết nối : 4G, 3G, Bluetooth, Wi-Fi</li>
	<li>Thời gian sử dụng : 9 giờ</li>
	<li>Hệ điều h&agrave;nh : iOS 9</li>
</ul>

<ul>
	<li>M&agrave;n h&igrave;nh</li>
	<li>C&ocirc;ng nghệ m&agrave;n h&igrave;nh : LED backlit LCD</li>
	<li>M&agrave;u m&agrave;n h&igrave;nh : 16 triệu m&agrave;u</li>
	<li>Độ ph&acirc;n giải : 1536 x 2048 pixels</li>
	<li>K&iacute;ch thước m&agrave;n h&igrave;nh : 7.9 inch</li>
	<li>C&ocirc;ng nghệ cảm ứng : Điện dung đa điểm</li>
	<li>Cấu h&igrave;nh phần cứng</li>
	<li>Loại CPU (Chipset) : Apple A8</li>
	<li>Tốc độ CPU : 1.5 GHz</li>
	<li>Số nh&acirc;n : 2 Nh&acirc;n</li>
	<li>Chip đồ hoạ (GPU) : PowerVR GXA6450</li>
	<li>RAM : 2 GB</li>
	<li>Cảm biến : La b&agrave;n, Con quay hồi chuyển 3 chiều, Gia tốc, &Aacute;nh s&aacute;ng, Fingerprint Sensor</li>
	<li>Bộ nhớ &amp; Lưu trữ</li>
	<li>Bộ nhớ trong : 32 GB</li>
	<li>Thẻ nhớ ngo&agrave;i : Kh&ocirc;ng</li>
	<li>Hỗ trợ thẻ nhớ tối đa : Kh&ocirc;ng</li>
	<li>Chụp h&igrave;nh &amp; Quay phim</li>
	<li>Camera sau : 8 MP</li>
	<li>Camera trước : 1.2 MP</li>
	<li>Đ&egrave;n Flash : Kh&ocirc;ng</li>
	<li>T&iacute;nh năng camera : Gắn thẻ địa lý, Tự đ&ocirc;̣ng l&acirc;́y nét</li>
	<li>Quay Phim : 1080p@30fps</li>
	<li>Kết nối &amp; Cổng giao tiếp</li>
	<li>Số khe sim : 1 Sim</li>
	<li>Loại Sim : Nano-SIM</li>
	<li>Thực hiện cuộc gọi : FaceTime</li>
	<li>Hỗ trợ 3G : HSDPA 850 / 900 / 1900 / 2100</li>
	<li>Hỗ trợ 4G : LTE</li>
	<li>Wifi : Wi-Fi 802.11 a/b/g/n</li>
	<li>Bluetooth : v 4.2</li>
	<li>GPS : A-GPS support; GLONASS</li>
	<li>Cổng kết nối/ sạc : Lighting</li>
	<li>Jack (Input &amp; Output) : 3.5 mm</li>
	<li>Hỗ trợ OTG : C&oacute;</li>
	<li>Kết nối kh&aacute;c : Kh&ocirc;ng</li>
	<li>Giải tr&iacute; &amp; Ứng dụng</li>
	<li>Xem phim : MPEG-4, H.264, FLV, AAC, 3GP, JPEG, GIF, Divx, WMV, AMR, WMA, MP4, AVI</li>
	<li>Nghe nhạc : WAVE, WAV, MP3, AAC+, WMA, AAC, MIDI</li>
	<li>Ghi &acirc;m : C&oacute;</li>
	<li>C&ocirc;ng vụ văn ph&ograve;ng : Hỗ trợ ứng dụng văn ph&ograve;ng</li>
	<li>Ứng dụng kh&aacute;c : Facebook, Game, Lịch, Đồng hồ, B&aacute;o thức, Bản đồ, Mail, Sổ tay</li>
	<li>Thiết kế &amp; Trọng lượng</li>
	<li>Chất liệu : Hợp kim nh&ocirc;m</li>
	<li>K&iacute;ch thước (DxRxC) : 203.2 x 134.8 x 6.1 mm</li>
	<li>Trọng lượng : 299g</li>
	<li>Pin &amp; Dung lượng</li>
	<li>Loại pin : Lithium - Polymer</li>
	<li>Dung lượng pin : 19.1 Wh (Khoảng 5124 mAh)</li>
	<li>Thời gian sử dụng : 9 giờ</li>
	<li>Thời gian sạc đầy : ~ 3 giờ</li>
	<li>Bảo h&agrave;nh</li>
	<li>Thời gian bảo h&agrave;nh : 12 Th&aacute;ng</li>
</ul>
</div>
', N'<h2 style="margin-left:0px; margin-right:0px">iPad Mini 4 Wifi 32GB với thiết kế sang trọng c&ugrave;ng dung lượng pin lớn sẽ gi&uacute;p bạn l&agrave;m việc v&agrave; giải tr&iacute; mọi l&uacute;c mọi nơi.</h2>

<h3 style="margin-left:0px; margin-right:0px"><strong>Nhẹ hơn v&agrave; mỏng hơn</strong></h3>

<p style="margin-left:0px; margin-right:0px">Với khả năng gia c&ocirc;ng cao cấp của Apple, iPad Mini 4 được l&agrave;m rất gọn g&agrave;ng v&agrave; hiện đại với th&acirc;n h&igrave;nh vẫn rất sang chảnh nhờ chất liệu kim loại sang trọng.</p>

<p style="margin-left:0px; margin-right:0px"><img alt="iPad Mini 4 (giữa) có bề dày mỏng hơn (bên trên là iPad Mini 2)" src="https://www.thegioididong.com/images/522/73089/ipad-mini-4-cellular-16gb-11-1.jpg" title="iPad Mini 4 (giữa) có bề dày mỏng hơn (bên trên là iPad Mini 2)" /></p>

<p style="margin-left:0px; margin-right:0px"><em>iPad Mini 4 (giữa) c&oacute; bề d&agrave;y mỏng hơn (b&ecirc;n tr&ecirc;n l&agrave;&nbsp;<a href="https://www.thegioididong.com/may-tinh-bang/ipad-mini-2-retina-16g-wifi" style="margin: 0px; padding: 0px; text-decoration: none; color: rgb(80, 168, 227);" target="_blank" title="Máy tính bảng iPad Mini 2 Retina 16G/Wifi">iPad Mini 2</a>)</em></p>

<p style="margin-left:0px; margin-right:0px"><em><img alt="Các đường cắt vát được làm rất tinh tế và sang trọng, nút gạt chế độ âm thanh đã được loại bỏ trên phiên bản iPad Mini 4 này" src="https://www.thegioididong.com/images/522/73089/ipad-mini-4-cellular-16gb-12-1.jpg" title="Các đường cắt vát được làm rất tinh tế và sang trọng, nút gạt chế độ âm thanh đã được loại bỏ trên phiên bản iPad Mini 4 này" /></em></p>

<p style="margin-left:0px; margin-right:0px"><em>C&aacute;c đường cắt v&aacute;t được l&agrave;m rất tinh tế v&agrave; sang trọng, n&uacute;t gạt chế độ &acirc;m thanh đ&atilde; được loại bỏ tr&ecirc;n phi&ecirc;n bản iPad Mini 4 n&agrave;y</em></p>

<p style="margin-left:0px; margin-right:0px"><img alt="iPad Mini 4 được đục 1 hàng loa thay vì 2 hàng 2 loa như thế hệ trước" src="https://www.thegioididong.com/images/522/73089/ipad-mini-46-1.jpg" /></p>

<p style="margin-left:0px; margin-right:0px"><em>iPad Mini 4 được đục 1 h&agrave;ng loa thay v&igrave; 2 h&agrave;ng 2 loa như thế hệ trước</em></p>

<h3 style="margin-left:0px; margin-right:0px"><strong>Đẹp v&agrave; sắc n&eacute;t</strong></h3>

<p style="margin-left:0px; margin-right:0px">Mặt trước iPad Mini 4 sở hữu m&agrave;n h&igrave;nh k&iacute;ch thước 7.9 inch, độ ph&acirc;n giải 1536 x 2048 pixels, nội dung&nbsp;hiển thị&nbsp;kh&aacute; thoải m&aacute;i để quan s&aacute;t, c&aacute;c trang web, văn bản rộng, đầy đủ.</p>

<p style="margin-left:0px; margin-right:0px"><em><img alt="Góc nhìn màn hình rất rộng và thoải mái" src="https://www.thegioididong.com/images/522/73089/ipad-mini-48.jpg" title="Góc nhìn màn hình rất rộng và thoải mái" /></em></p>

<p style="margin-left:0px; margin-right:0px"><em>G&oacute;c nh&igrave;n m&agrave;n h&igrave;nh rất rộng v&agrave; thoải m&aacute;i</em></p>

<p style="margin-left:0px; margin-right:0px">C&ocirc;ng nghệ một lớp m&agrave;n h&igrave;nh được&nbsp;<a href="https://www.thegioididong.com/may-tinh-bang-apple-ipad" style="margin: 0px; padding: 0px; text-decoration: none; color: rgb(80, 168, 227);" target="_blank" title="Máy tính bảng Apple">Apple</a>&nbsp;sử dụng tr&ecirc;n iPad mini 4, nhờ đ&oacute; th&acirc;n m&aacute;y mỏng hơn, đem đến chất lượng h&igrave;nh ảnh sắc n&eacute;t hơn, độ s&aacute;ng cao v&agrave; chống l&oacute;a tốt.</p>

<p style="margin-left:0px; margin-right:0px"><img alt="Công nghệ màn hình" src="https://www.thegioididong.com/images/522/73089/ipad-mini-49.jpg" title="Công nghệ màn hình" /></p>

<p style="margin-left:0px; margin-right:0px">Mẹo l&agrave;m chủ m&agrave;n h&igrave;nh iPad:</p>

<p style="margin-left:0px; margin-right:0px">- Bạn v&agrave;o&nbsp;<strong>c&agrave;i đặt &gt; c&agrave;i đặt chung &gt; trợ năng &gt; chọn thang m&agrave;u x&aacute;m</strong>&nbsp;để m&agrave;n h&igrave;nh của m&aacute;y đưa về 2 m&agrave;u đen trắng, gi&uacute;p tiết kiệm pin hơn.</p>

<p style="margin-left:0px; margin-right:0px">- Việc đọc chữ đen tr&ecirc;n nền trắng sẽ l&agrave;m bạn dễ bị đau mắt, h&atilde;y sử dụng chức năng&nbsp;<strong>đảo m&agrave;u</strong>&nbsp;trong&nbsp;trợ năng&nbsp;để đưa m&aacute;y về nền đen to&agrave;n bộ.</p>

<h3 style="margin-left:0px; margin-right:0px"><strong>Hệ điều h&agrave;nh mới, vi xử l&yacute; cũ nhưng rất mạnh mẽ với RAM 2 GB</strong></h3>

<p style="margin-left:0px; margin-right:0px">iPad Mini 4&nbsp;được chip&nbsp;Apple A8 nhưng với&nbsp;mức RAM 2 GB tăng cường khả năng hoạt động đa nhiệm tốt hơn.&nbsp;iPad Mini 4 c&oacute; tốc độ CPU nhanh hơn 1.3 lần, tốc độ GPU hơn 1.6 lần.</p>

<p style="margin-left:0px; margin-right:0px">&nbsp;</p>

<div class="video" style="font-family:Helvetica,Arial,; padding:0px"><iframe frameborder="0" src="https://www.youtube.com/embed/Yyl9IDWRr2Q?rel=0"></iframe></div>

<p style="margin-left:0px; margin-right:0px">&nbsp;</p>

<p style="margin-left:0px; margin-right:0px"><em>Video test tốc độ của iPad Mini 4 với&nbsp;<a href="https://www.thegioididong.com/may-tinh-bang/ipad-air-2-cellular" style="margin: 0px; padding: 0px; text-decoration: none; color: rgb(80, 168, 227);" target="_blank" title="Máy tính bảng iPad Air 2 Cellular 16GB">iPad Air 2</a>&nbsp;v&agrave; iPad Mini 2 (iPad Mini 4 ở giữa)</em></p>

<p style="margin-left:0px; margin-right:0px">M&aacute;y c&oacute; khả năng chạy đa nhiệm chia đ&ocirc;i m&agrave;n h&igrave;nh gi&uacute;p bạn xem được nhiều nội dung hơn trong c&ugrave;ng một thời điểm.</p>

<p style="margin-left:0px; margin-right:0px"><img alt="Tính năng đa nhiệm, chia đôi màn hình, video thoại không lạ với Android nhưng rất hữu dụng với iPad" src="https://www.thegioididong.com/images/522/73089/ipad-mini-411.jpg" title="Tính năng đa nhiệm, chia đôi màn hình, video thoại không lạ với Android nhưng rất hữu dụng với iPad" /></p>

<p style="margin-left:0px; margin-right:0px"><em>T&iacute;nh năng đa nhiệm, chia đ&ocirc;i m&agrave;n h&igrave;nh, video thoại kh&ocirc;ng lạ với Android nhưng rất hữu dụng với iPad</em></p>

<h3 style="margin-left:0px; margin-right:0px"><strong>Camera iSight 8 MP</strong></h3>

<p style="margin-left:0px; margin-right:0px">So với 5 MP như thế hệ trước, 8 MP tr&ecirc;n iPad Mini 4 đ&atilde; mạnh mẽ hơn nhiều với chi tiết h&igrave;nh ảnh cao hơn, hỗ trợ chụp ảnh HDR, chụp li&ecirc;n tục, quay video chuyển động chậm Slow Motion.</p>

<p style="margin-left:0px; margin-right:0px"><img alt="Tốc độ chụp trên các thiết bị của Apple vẫn rất nhanh và có khả năng lấy nét rất chính xác" src="https://www.thegioididong.com/images/522/73089/ipad-mini-4-cellular-16gb-5.jpg" title="Tốc độ chụp trên các thiết bị của Apple vẫn rất nhanh và có khả năng lấy nét rất chính xác" /></p>

<p style="margin-left:0px; margin-right:0px"><em>Tốc độ chụp tr&ecirc;n c&aacute;c thiết bị của Apple vẫn rất nhanh v&agrave; c&oacute; khả năng lấy n&eacute;t rất ch&iacute;nh x&aacute;c</em></p>

<h3 style="margin-left:0px; margin-right:0px"><strong>Nhỏ hơn nhưng tốt hơn</strong></h3>

<p style="margin-left:0px; margin-right:0px">Mặc d&ugrave; c&oacute; mức pin chỉ 5124 mAh, tuy nhi&ecirc;n khả năng quản l&yacute; pin của hệ điều h&agrave;nh mới l&agrave; tốt hơn do đ&oacute; pin sử dụng thậm ch&iacute; c&ograve;n được k&eacute;o d&agrave;i hơn so với thế hệ trước.</p>

<p style="margin-left:0px; margin-right:0px"><img alt="Thời gian sử dụng pin thoải mái trong nhiều ngày" src="https://www.thegioididong.com/images/522/89540/ipad-mini-4-wifi-32gb-1.jpg" title="Thời gian sử dụng pin thoải mái trong nhiều ngày" /></p>

<p style="margin-left:0px; margin-right:0px"><em>Thời gian sử dụng pin thoải m&aacute;i trong nhiều ng&agrave;y</em></p>

<p style="margin-left:0px; margin-right:0px">iPad Mini 4 l&agrave; một sản phẩm mới được n&acirc;ng cấp kh&aacute; nhiều so với phi&ecirc;n bản cũ, tuy sự n&acirc;ng cấp chỉ tập trung v&agrave;o hệ điều h&agrave;nh v&agrave; camera, c&aacute;c trải nghiệm tr&ecirc;n thiết bị kh&ocirc;ng nghi ngờ g&igrave;, vẫn rất cao cấp v&agrave; đ&aacute;ng gi&aacute;.</p>
', N'/UploadFile/images/Ipad/SonyiPadmini4BB7_lon220915024818.png', 11, NULL, NULL, NULL, NULL, 1, 1, 2, 3, 0, N'["/UploadFile/images/Ipad/SonyiPadmini4BB3_lon220915024754.png","/UploadFile/images/Ipad/SonyiPadmini4BB2_lon220915025654.png","/UploadFile/images/Ipad/SonyiPadmini4BB_lon220915025642.png","/UploadFile/images/Ipad/SonyiPadmini4BB4_lon220915024800.png","/UploadFile/images/Ipad/SonyiPadmini4BB5_lon220915024806.png","/UploadFile/images/Ipad/SonyiPadmini4BB6_lon220915024812.png"]', 1)
SET IDENTITY_INSERT [dbo].[SanPham] OFF
SET IDENTITY_INSERT [dbo].[Slides] ON 

INSERT [dbo].[Slides] ([ID], [Ten], [MoTa], [HinhAnh], [Url], [ThuTu], [DaXoa]) VALUES (1, N'Chon SamSung nhận quà khủng', NULL, N'/UploadFile/images/Slide/100000_chon-samsung-nhan-qua-khung-760x325.jpg', NULL, 1, 0)
INSERT [dbo].[Slides] ([ID], [Ten], [MoTa], [HinhAnh], [Url], [ThuTu], [DaXoa]) VALUES (3, N'GalaxyS8 trúng quà khủng', NULL, N'/UploadFile/images/Slide/100000_galaxy-s8-760x325-1.jpg', NULL, 2, 0)
INSERT [dbo].[Slides] ([ID], [Ten], [MoTa], [HinhAnh], [Url], [ThuTu], [DaXoa]) VALUES (5, N'Khuyến mãi Iphone 7,7 plus', NULL, N'/UploadFile/images/Slide/636295466061281404_banner_h1_iPhone.jpg', NULL, 3, 0)
INSERT [dbo].[Slides] ([ID], [Ten], [MoTa], [HinhAnh], [Url], [ThuTu], [DaXoa]) VALUES (6, N'Airpad mới', NULL, N'/UploadFile/images/Slide/air_moi_1489226983.jpg', NULL, 4, 0)
SET IDENTITY_INSERT [dbo].[Slides] OFF
SET IDENTITY_INSERT [dbo].[ThanhVien] ON 

INSERT [dbo].[ThanhVien] ([MaThanhVien], [TaiKhoan], [MatKhau], [HoTen], [DiaChi], [Email], [SoDienThoai], [CauHoi], [CauTraLoi], [MaLoaiTV]) VALUES (1, N'admin', N'66ac0757935b79eb86f386436a9fa8be', N'Hoàng Thị Hương', N'15/b tổ 3 phường Quang Vinh', N'huongthi@gmail.com', N'0909465745', NULL, NULL, 1)
INSERT [dbo].[ThanhVien] ([MaThanhVien], [TaiKhoan], [MatKhau], [HoTen], [DiaChi], [Email], [SoDienThoai], [CauHoi], [CauTraLoi], [MaLoaiTV]) VALUES (2, N'admin2', N'e10adc3949ba59abbe56e057f20f883e', N'Lê Văn Sơn', N'15/b tổ 3 phường Quang Vinh', N'son@gmail.com', N'01656606281', NULL, NULL, 2)
INSERT [dbo].[ThanhVien] ([MaThanhVien], [TaiKhoan], [MatKhau], [HoTen], [DiaChi], [Email], [SoDienThoai], [CauHoi], [CauTraLoi], [MaLoaiTV]) VALUES (3, N'gala', N'52a43bc4333b63e5cd9e952357795054', N'Đỗ Xuân Thành', NULL, NULL, NULL, NULL, NULL, 6)
INSERT [dbo].[ThanhVien] ([MaThanhVien], [TaiKhoan], [MatKhau], [HoTen], [DiaChi], [Email], [SoDienThoai], [CauHoi], [CauTraLoi], [MaLoaiTV]) VALUES (9, N'quanuser1228', N'373d2ac397d792a78cb720e3b124c3d6', N'Thanh Quan', N'15/1b', N'quan@gmail.com', N'01656606281', N'Bạn đang làm nghề gì', N'sinh viên', 6)
INSERT [dbo].[ThanhVien] ([MaThanhVien], [TaiKhoan], [MatKhau], [HoTen], [DiaChi], [Email], [SoDienThoai], [CauHoi], [CauTraLoi], [MaLoaiTV]) VALUES (10, N'toivatoi', N'e10adc3949ba59abbe56e057f20f883e', N'Minh Tiên', N'15/1b', N'doxuanthanhcntt@gmail.com', N'01656606281', N'Ca sĩ bạn yêu thích là gì', N'MTP', 6)
INSERT [dbo].[ThanhVien] ([MaThanhVien], [TaiKhoan], [MatKhau], [HoTen], [DiaChi], [Email], [SoDienThoai], [CauHoi], [CauTraLoi], [MaLoaiTV]) VALUES (11, N'thanhbinh', N'2108ad237b418ad14c3d1c5252ebc0b0', N'Nguyễn Võ Thanh Bình', N'15/1 phường Quang Vinh, Biên Hòa, Đồng Nai', N'thanhbinh@gmail.com', N'0909465745', N'Bạn đang làm nghề gì', N'sinh viên', 4)
INSERT [dbo].[ThanhVien] ([MaThanhVien], [TaiKhoan], [MatKhau], [HoTen], [DiaChi], [Email], [SoDienThoai], [CauHoi], [CauTraLoi], [MaLoaiTV]) VALUES (1011, N'badboy', N'e10adc3949ba59abbe56e057f20f883e', N'Thanh Quan', N'15/1b', N'thanhquan@gmail.com', N'01656606281', N'Ca sĩ bạn yêu thích là gì', N'MTP', 6)
SET IDENTITY_INSERT [dbo].[ThanhVien] OFF
ALTER TABLE [dbo].[BinhLuan]  WITH CHECK ADD  CONSTRAINT [FK_BinhLuan_SanPham] FOREIGN KEY([MaSP])
REFERENCES [dbo].[SanPham] ([MaSP])
GO
ALTER TABLE [dbo].[BinhLuan] CHECK CONSTRAINT [FK_BinhLuan_SanPham]
GO
ALTER TABLE [dbo].[BinhLuan]  WITH CHECK ADD  CONSTRAINT [FK_BinhLuan_ThanhVien] FOREIGN KEY([MaThanhVien])
REFERENCES [dbo].[ThanhVien] ([MaThanhVien])
GO
ALTER TABLE [dbo].[BinhLuan] CHECK CONSTRAINT [FK_BinhLuan_ThanhVien]
GO
ALTER TABLE [dbo].[ChiTietDonDatHang]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietDonDatHang_DonDatHang] FOREIGN KEY([MaDDH])
REFERENCES [dbo].[DonDatHang] ([MaDDH])
GO
ALTER TABLE [dbo].[ChiTietDonDatHang] CHECK CONSTRAINT [FK_ChiTietDonDatHang_DonDatHang]
GO
ALTER TABLE [dbo].[ChiTietDonDatHang]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietDonDatHang_SanPham] FOREIGN KEY([MaSP])
REFERENCES [dbo].[SanPham] ([MaSP])
GO
ALTER TABLE [dbo].[ChiTietDonDatHang] CHECK CONSTRAINT [FK_ChiTietDonDatHang_SanPham]
GO
ALTER TABLE [dbo].[ChiTietPhieuNhap]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietPhieuNhap_PhieuNhap] FOREIGN KEY([MaPN])
REFERENCES [dbo].[PhieuNhap] ([MaPN])
GO
ALTER TABLE [dbo].[ChiTietPhieuNhap] CHECK CONSTRAINT [FK_ChiTietPhieuNhap_PhieuNhap]
GO
ALTER TABLE [dbo].[ChiTietPhieuNhap]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietPhieuNhap_SanPham] FOREIGN KEY([MaSP])
REFERENCES [dbo].[SanPham] ([MaSP])
GO
ALTER TABLE [dbo].[ChiTietPhieuNhap] CHECK CONSTRAINT [FK_ChiTietPhieuNhap_SanPham]
GO
ALTER TABLE [dbo].[DonDatHang]  WITH CHECK ADD  CONSTRAINT [FK_DonDatHang_KhachHang] FOREIGN KEY([MaKH])
REFERENCES [dbo].[KhachHang] ([MaKH])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[DonDatHang] CHECK CONSTRAINT [FK_DonDatHang_KhachHang]
GO
ALTER TABLE [dbo].[KhachHang]  WITH CHECK ADD  CONSTRAINT [FK_KhachHang_ToTable] FOREIGN KEY([MaThanhVien])
REFERENCES [dbo].[ThanhVien] ([MaThanhVien])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[KhachHang] CHECK CONSTRAINT [FK_KhachHang_ToTable]
GO
ALTER TABLE [dbo].[LoaiThanhVien_Quyen]  WITH CHECK ADD  CONSTRAINT [FK_LoaiThanhVien_Quyen_LoaiThanhVien] FOREIGN KEY([MaLoaiTV])
REFERENCES [dbo].[LoaiThanhVien] ([MaLoaiThanhVien])
GO
ALTER TABLE [dbo].[LoaiThanhVien_Quyen] CHECK CONSTRAINT [FK_LoaiThanhVien_Quyen_LoaiThanhVien]
GO
ALTER TABLE [dbo].[LoaiThanhVien_Quyen]  WITH CHECK ADD  CONSTRAINT [FK_LoaiThanhVien_Quyen_Quyen] FOREIGN KEY([MaQuyen])
REFERENCES [dbo].[Quyen] ([MaQuyen])
GO
ALTER TABLE [dbo].[LoaiThanhVien_Quyen] CHECK CONSTRAINT [FK_LoaiThanhVien_Quyen_Quyen]
GO
ALTER TABLE [dbo].[PhieuNhap]  WITH CHECK ADD  CONSTRAINT [FK_PhieuNhap_ToTable] FOREIGN KEY([MaNCC])
REFERENCES [dbo].[NhaCungCap] ([MaNCC])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PhieuNhap] CHECK CONSTRAINT [FK_PhieuNhap_ToTable]
GO
ALTER TABLE [dbo].[SanPham]  WITH CHECK ADD  CONSTRAINT [FK_SanPham_LoaiSP] FOREIGN KEY([MaLoaiSP])
REFERENCES [dbo].[LoaiSanPham] ([MaLoaiSP])
GO
ALTER TABLE [dbo].[SanPham] CHECK CONSTRAINT [FK_SanPham_LoaiSP]
GO
ALTER TABLE [dbo].[SanPham]  WITH CHECK ADD  CONSTRAINT [FK_SanPham_NhaCungCap] FOREIGN KEY([MaNCC])
REFERENCES [dbo].[NhaCungCap] ([MaNCC])
GO
ALTER TABLE [dbo].[SanPham] CHECK CONSTRAINT [FK_SanPham_NhaCungCap]
GO
ALTER TABLE [dbo].[SanPham]  WITH CHECK ADD  CONSTRAINT [FK_SanPham_NhaSanXuat] FOREIGN KEY([MaNSX])
REFERENCES [dbo].[NhaSanXuat] ([MaNSX])
GO
ALTER TABLE [dbo].[SanPham] CHECK CONSTRAINT [FK_SanPham_NhaSanXuat]
GO
ALTER TABLE [dbo].[ThanhVien]  WITH CHECK ADD  CONSTRAINT [FK_ThanhVien_ToTable] FOREIGN KEY([MaLoaiTV])
REFERENCES [dbo].[LoaiThanhVien] ([MaLoaiThanhVien])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ThanhVien] CHECK CONSTRAINT [FK_ThanhVien_ToTable]
GO
