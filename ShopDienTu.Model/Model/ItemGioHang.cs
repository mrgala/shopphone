﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopDienTu.Model.Model
{
    public class ItemGioHang
    {
        public int MaSP { set; get; }

        public string TenSP { set; get; }

        public int SoLuong { set; get; }

        public decimal DonGia { set; get; }

        public decimal ThanhTien { set; get; }

        public string HinhAnh { set; get; }
    }
}
