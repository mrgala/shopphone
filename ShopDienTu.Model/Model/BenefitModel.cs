﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopDienTu.Model.Model
{
    public class BenefitModel
    {

        public DateTime Date { get; set; }

        public decimal Revenue { get; set; }

        public decimal Benefit { get; set; }
    }
}
