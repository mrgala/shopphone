﻿using ShopDienTu.Model.Infrastructure;
using ShopDienTu.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopDienTu.Model.DAO
{
    public class ChiTietPhieuNhapDao : Disposable
    {
        ShopDienTuEntities db;

        public ChiTietPhieuNhapDao()
        {
            db = new ShopDienTuEntities();
        }

        public void Add(ChiTietPhieuNhap ctpn)
        {
            db.ChiTietPhieuNhaps.Add(ctpn);
            db.SaveChanges();
        }

        public void AddRange(IEnumerable<ChiTietPhieuNhap> ctpn)
        {
            db.ChiTietPhieuNhaps.AddRange(ctpn);
            db.SaveChanges();
        }

        public IEnumerable<ChiTietPhieuNhap> GetListByID(int id)
        {
            return db.ChiTietPhieuNhaps.Where(x => x.MaPN == id);
        }

        public void DeletList(IEnumerable<ChiTietPhieuNhap> lstCTPN)
        {
            db.ChiTietPhieuNhaps.RemoveRange(lstCTPN);
            db.SaveChanges();
        }

        public void UpdateRange(int id, int[] lstNew,int[]lstOld)
        {
            var lstCtpn = db.ChiTietPhieuNhaps.Where(n => n.MaPN == id);
            int k = 0;
            foreach (var item in lstCtpn)
            {
                var sp = db.SanPhams.SingleOrDefault(x => x.MaSP == item.MaSP);
                sp.SoLuongTon -= lstOld[k];
                sp.SoLuongTon += lstNew[k];
                item.SoLuongNhap = lstNew[k];
                k++;
            }
            var pn = db.PhieuNhaps.SingleOrDefault(n => n.MaPN == id);
            pn.NgaySua = DateTime.Now;
            db.SaveChanges();
        }

        protected override void DisposeCore()
        {
            if (db != null)
            {
                db.Dispose();
            }
        }
    }
}
