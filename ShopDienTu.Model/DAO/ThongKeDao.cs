﻿using ShopDienTu.Model.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopDienTu.Model.DAO
{
    public class ThongKeDao
    {
        ShopDienTuEntities db;

        public ThongKeDao()
        {
            db = new ShopDienTuEntities();
        }

        public double ThongKeDonHang()
        {
            return db.DonDatHangs.Count();
        }

        public double ThongKeUser()
        {
            return db.ThanhViens.Count();
        }

        public IEnumerable<BenefitModel> GetBenefit(string fromDate, string toDate)
        {
            var parameter = new SqlParameter[]
            {
                new SqlParameter("@fromDate",fromDate),
                new SqlParameter("@toDate",toDate + " " + DateTime.Now.ToLongTimeString())
            };
            var lstBenefit = db.Database.SqlQuery<BenefitModel>("Benefit @fromDate,@toDate",parameter).ToList<BenefitModel>();
            return lstBenefit;
        }

        public List<ChiTietPhieuNhap> GetListPhieuNhap(DateTime fromDate,DateTime toDate)
        {
            toDate = toDate.AddHours(20.0f);
            var result = db.PhieuNhaps.Where(x => x.NgayNhap >= fromDate && x.NgayNhap <= toDate);
                //.Select(g => new
                //{
                //    NgayNhap = g.Key,
                //    ChiTietPhieuNhap = g.Select(x=>x.MaPN == 1)
                //});
            var lstChitietPhieuNhaps = new List<ChiTietPhieuNhap>();
            foreach (var item in result)
            {
                var lstChitietPhieuNhap = db.ChiTietPhieuNhaps.Where(x => x.MaPN == item.MaPN).ToList();
                lstChitietPhieuNhaps.AddRange(lstChitietPhieuNhap);
            }
            return lstChitietPhieuNhaps;
        }
    }
}
