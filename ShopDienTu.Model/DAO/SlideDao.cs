﻿using ShopDienTu.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopDienTu.Model.DAO
{
    public class SlideDao
    {
        ShopDienTuEntities db = null;

        public SlideDao()
        {
            db = new ShopDienTuEntities();
        }

        public IEnumerable<Slide> GetSlideAll()
        {
            return db.Slides.OrderBy(x => x.ThuTu);
        }
    }
}
