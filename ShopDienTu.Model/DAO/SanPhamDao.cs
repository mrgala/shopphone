﻿using ShopDienTu.Model.Infrastructure;
using ShopDienTu.Model.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopDienTu.Model.DAO
{
    public class SanPhamDao : Disposable
    {
        ShopDienTuEntities db;

        public SanPhamDao()
        {
            db = new ShopDienTuEntities();
        }

        public IEnumerable<SanPham> GetAll(out int totalCount)
        {
            totalCount = db.SanPhams.Count();
            return db.SanPhams.OrderByDescending(x=>x.NgayCapNhap);
        }

        public IEnumerable<SanPham> GetAlls()
        { 
            return db.SanPhams.OrderBy(x => x.TenSP);
        }

        public void Add(SanPham sp)
        {
            db.SanPhams.Add(sp);
            db.SaveChanges();
        }

        public SanPham GetByID(int? id)
        {
            return db.SanPhams.SingleOrDefault(x => x.MaSP == id);
        }

        public IEnumerable<SanPham> GetSortListByID(int? id,out int totalCount,string sort,int page, int PageSize)
        {
            var lstSP = db.SanPhams.Where(x => x.MaNSX == id);
            totalCount = lstSP.Count();
            switch (sort)
            {
                case "new":
                    lstSP = lstSP.OrderByDescending(x => x.NgayCapNhap);
                    break;
                case "price":
                    lstSP = lstSP.OrderBy(x => x.DonGia);
                    break;
                case "popular":
                    lstSP = lstSP.OrderBy(x => x.LuotXem);
                    break;
                default:
                    lstSP = lstSP.OrderBy(x => x.TenSP);
                    break;
            }
            return lstSP;

            //IEnumerable<SanPham> lstSP = from sp in db.SanPhams
            //            where sp.MaNSX == id
            //            orderby sp.TenSP, sp.DonGia
            //            select sp;
            //totalCount = lstSP.Count();
            //return lstSP;
        }

        public void Update(SanPham sp)
        {
            db.Entry(sp).State = EntityState.Modified;
            db.SaveChanges();
        }

        public IEnumerable<SanPham> Search(string keyword, out int totalCount)
        {
            var model = db.SanPhams.Where(x => x.TenSP.Contains(keyword));
            totalCount = model.Count();
            return model.OrderByDescending(x => x.NgayCapNhap);
        }

        public IEnumerable<SanPham> GetOutOfStock(out int totalCount)
        {
            var model = db.SanPhams.Where(x => x.SoLuongTon < 5);
            totalCount = model.Count();
            return model.OrderBy(x=>x.TenSP);
        }

        public IEnumerable<SanPham> GetHot(int maLoai, int top)
        {
            return db.SanPhams.Where(x => x.MaLoaiSP == maLoai && x.Moi == true && x.DaXoa == false && x.Home == true).OrderByDescending(x=>x.NgayCapNhap).Take(top);
        }

        public IEnumerable<SanPham> GetRelated(int? maLoai, int ?maNSX, int top)
        {
            return db.SanPhams.Where(x => x.MaLoaiSP == maLoai && x.MaNSX == maNSX).OrderByDescending(x => x.NgayCapNhap).Take(top);
        }

        public void Delete(int id)
        {
            var model = db.SanPhams.SingleOrDefault(x => x.MaSP == id);
            db.SanPhams.Remove(model);
            db.SaveChanges();
        }

        public List<SanPham> GetListProductByName(string keyword)
        {
            //db.Configuration.ProxyCreationEnabled = false;
            var model = db.SanPhams.Where(x=>x.DaXoa == false && x.TenSP.Contains(keyword)).ToList();
            return model;
        }

        protected override void DisposeCore()
        {
            if (db != null)
            {
                db.Dispose();
            }
        }

    }
}
