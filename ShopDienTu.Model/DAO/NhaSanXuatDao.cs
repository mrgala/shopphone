﻿using ShopDienTu.Model.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopDienTu.Model.DAO
{
    public class NhaSanXuatDao
    {
        private ShopDienTuEntities db = null;

        public NhaSanXuatDao()
        {
            db = new ShopDienTuEntities();
        }

        public IEnumerable<NhaSanXuat> GetAll()
        {
            return db.NhaSanXuats.OrderBy(x => x.TenNSX);
        }

        public void Add(NhaSanXuat nhasx)
        {
            db.NhaSanXuats.Add(nhasx);
            db.SaveChanges();
        }

        public NhaSanXuat GetByID(int id)
        {
            return db.NhaSanXuats.SingleOrDefault(x => x.MaNSX == id);
        }

        public void Update(NhaSanXuat nhasx)
        {
            db.Entry(nhasx).State = EntityState.Modified;
            db.SaveChanges();
        }

        public void Delete(int id)
        {
            var model = db.NhaSanXuats.SingleOrDefault(x => x.MaNSX == id);
            db.NhaSanXuats.Remove(model);
            db.SaveChanges();
        }
    }
}
