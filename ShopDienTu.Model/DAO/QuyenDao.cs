﻿using ShopDienTu.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopDienTu.Model.DAO
{
    public class QuyenDao
    {
        ShopDienTuEntities db = null;

        public QuyenDao()
        {
            db = new ShopDienTuEntities();
        }

        public IEnumerable<Quyen> GetAll()
        {
            return db.Quyens;
        }
    }
}
