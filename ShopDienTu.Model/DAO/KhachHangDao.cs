﻿using ShopDienTu.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopDienTu.Model.DAO
{
    public class KhachHangDao
    {
        ShopDienTuEntities db;

        DonDatHang ddh;

        public KhachHangDao()
        {
            db = new ShopDienTuEntities();
            ddh = new DonDatHang();
        }

        public void AddKhachHang(KhachHang kh,List<ItemGioHang> lstCart)
        {
            db.KhachHangs.Add(kh);

            ddh.MaKH = kh.MaKH;
            ddh.NgayDat = DateTime.Now;
            ddh.TinhTrangGiaoHang = false;
            ddh.DaThanhToan = false;
            ddh.UuDai = 0;
            ddh.DaHuy = false;
            ddh.DaXoa = false;
            db.DonDatHangs.Add(ddh);

            foreach (var item in lstCart)
            {
                var ctddh = new ChiTietDonDatHang();
                ctddh.MaDDH = ddh.MaDDH;
                ctddh.MaSP = item.MaSP;
                ctddh.TenSP = item.TenSP;
                ctddh.SoLuong = item.SoLuong;
                ctddh.DonGia = item.DonGia;
                db.ChiTietDonDatHangs.Add(ctddh);
            }
            db.SaveChanges();
        }

        public KhachHang ConvertKhachHang(KhachHang kh,ThanhVien tv)
        {
            kh.TenKH = tv.HoTen;
            kh.DiaChi = tv.DiaChi;
            kh.Email = tv.Email;
            kh.SoDienThoai = tv.SoDienThoai;
            kh.MaThanhVien = tv.MaThanhVien;

            return kh;
        }
    }
}
