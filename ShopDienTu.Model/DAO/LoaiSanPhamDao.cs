﻿using ShopDienTu.Model.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopDienTu.Model.DAO
{
    public class LoaiSanPhamDao
    {
        ShopDienTuEntities db = null;

        public LoaiSanPhamDao()
        {
            db = new ShopDienTuEntities();
        }

        public IEnumerable<LoaiSanPham> GetAll()
        {
            return db.LoaiSanPhams.OrderBy(x=>x.TenLoai);
        }

        public void Add(LoaiSanPham loaiSP)
        {
            db.LoaiSanPhams.Add(loaiSP);
            db.SaveChanges();
        }

        public LoaiSanPham GetByID(int id)
        {
            return db.LoaiSanPhams.SingleOrDefault(x => x.MaLoaiSP == id);
        }

        public void Update(LoaiSanPham loaiSp)
        {
            db.Entry(loaiSp).State = EntityState.Modified;
            db.SaveChanges();
        }

        public void Delete(int id)
        {
            var model = db.LoaiSanPhams.SingleOrDefault(x => x.MaLoaiSP == id);
            db.LoaiSanPhams.Remove(model);
            db.SaveChanges();
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        if (db != null)
        //            db.Dispose();
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

    }
}
