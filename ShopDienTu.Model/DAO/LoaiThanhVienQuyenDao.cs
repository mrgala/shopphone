﻿using ShopDienTu.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopDienTu.Model.DAO
{
    public class LoaiThanhVienQuyenDao
    {
        ShopDienTuEntities db;

        public LoaiThanhVienQuyenDao()
        {
            db = new ShopDienTuEntities();
        }

        public IEnumerable<LoaiThanhVien_Quyen> GetListByID(int? id)
        {
            return db.LoaiThanhVien_Quyen.Where(x => x.MaLoaiTV == id);
        }

        public void DeleteList(IEnumerable<LoaiThanhVien_Quyen> lstQuyen)
        {
            db.LoaiThanhVien_Quyen.RemoveRange(lstQuyen);
            db.SaveChanges();
        }

        public void Add(LoaiThanhVien_Quyen loaiTVQuyen)
        {
            db.LoaiThanhVien_Quyen.Add(loaiTVQuyen);
        }

        public void Save()
        {
            db.SaveChanges();
        }
    }
}
