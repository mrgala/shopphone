﻿using ShopDienTu.Model.Model;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace ShopDienTu.Model.DAO
{
    public class NhaCungCapDao
    {
        private ShopDienTuEntities db = null;

        public NhaCungCapDao()
        {
            db = new ShopDienTuEntities();
        }

        public IEnumerable<NhaCungCap> GetAll()
        {
            return db.NhaCungCaps.OrderBy(x=>x.TenNCC);
        }

        public void Add(NhaCungCap nhacc)
        {
            db.NhaCungCaps.Add(nhacc);
            db.SaveChanges();
        }

        public NhaCungCap GetByID(int id)
        {
            return db.NhaCungCaps.SingleOrDefault(x => x.MaNCC == id);
        }

        public void Update(NhaCungCap nhacc)
        {
            db.Entry(nhacc).State = EntityState.Modified;
            db.SaveChanges();
        }

        public void Delete(int id)
        {
            var model = db.NhaCungCaps.SingleOrDefault(x => x.MaNCC == id);
            db.NhaCungCaps.Remove(model);
            db.SaveChanges();
        }
    }
}