﻿using ShopDienTu.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopDienTu.Model.DAO
{
    public class QuanLyDonHangDao
    {
        ShopDienTuEntities db;

        public QuanLyDonHangDao()
        {
            db = new ShopDienTuEntities();
        }

        public IEnumerable<DonDatHang> GetAllUnPaid()
        {
            return db.DonDatHangs.Where(x => x.DaThanhToan == false).OrderBy(x => x.NgayDat);
        }

        public IEnumerable<DonDatHang> GetAllDelivery()
        {
            return db.DonDatHangs.Where(x => x.TinhTrangGiaoHang == false && x.DaThanhToan == true).OrderBy(x => x.NgayDat);
        }

        public IEnumerable<DonDatHang> GetAllSuccess()
        {
            return db.DonDatHangs.Where(x => x.TinhTrangGiaoHang == true && x.DaThanhToan == true).OrderBy(x => x.NgayDat);
            
            //cách viết khác của linq

            //var dh = db.DonDatHangs.Select(x => new DonDatHang
            //{
            //    MaDDH = x.MaDDH,
            //}).ToList();
        }
    }
}
