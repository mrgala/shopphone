﻿using ShopDienTu.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopDienTu.Model.DAO
{
    public class ThanhVienDao
    {
        ShopDienTuEntities db = null;

        public ThanhVienDao()
        {
            db = new ShopDienTuEntities();
        }

        public int DangNhap(string taikhoan, string matkhau, bool isAdmin = false)
        {
            var result = db.ThanhViens.SingleOrDefault(x => x.TaiKhoan == taikhoan);
            if (result == null)
            {
                return 0;
            }
            else
            {
                if (isAdmin == true)
                {

                    if (result.MaLoaiTV == 6 || result.MaLoaiTV == 7)
                    {
                        return -2;
                    }
                    else
                    {
                        if (result.MatKhau == matkhau)
                        {                            
                            return 1;
                        }
                        else
                            return -1;
                    }
                }
                else
                {
                    if (result.MatKhau == matkhau)
                        return 1;
                    else
                        return -1;
                }
            }
        }

        public ThanhVien GetUserByTK(string taikhoan, string matkhau)
        {
            return db.ThanhViens.SingleOrDefault(x => x.TaiKhoan == taikhoan && x.MatKhau == matkhau);
        }

        public bool CheckUserName(string userName)
        {
            return db.ThanhViens.Count(x => x.TaiKhoan == userName) > 0;
        }

        public bool CheckEmail(string email)
        {
            return db.ThanhViens.Count(x => x.Email == email) > 0;
        }

        public bool Add(ThanhVien tv)
        {
            if (tv != null)
            {
                db.ThanhViens.Add(tv);
                db.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public ThanhVien GetByID(int? id)
        {
            return db.ThanhViens.SingleOrDefault(x => x.MaThanhVien == id);
        }

        public IEnumerable<ThanhVien> GetListByCondition(int a)
        {
            return db.ThanhViens.Where(x => x.MaThanhVien < a);
        }
    }
}
