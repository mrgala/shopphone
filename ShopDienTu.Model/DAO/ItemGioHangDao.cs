﻿using ShopDienTu.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopDienTu.Model.DAO
{
    public class ItemGioHangDao
    {
        ShopDienTuEntities db = null;

        public ItemGioHangDao()
        {
            db = new ShopDienTuEntities();
        }

        public ItemGioHang SetNew(SanPham sp)
        {
            ItemGioHang item = new ItemGioHang();
            item.MaSP = sp.MaSP;
            item.TenSP = sp.TenSP;
            item.HinhAnh = sp.HinhAnh;
            item.DonGia = sp.DonGia;
            item.SoLuong = 1;
            item.ThanhTien = item.DonGia * item.SoLuong;
            return item;
        }

    }
}
