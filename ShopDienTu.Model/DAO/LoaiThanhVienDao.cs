﻿using ShopDienTu.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopDienTu.Model.DAO
{
    public class LoaiThanhVienDao
    {
        ShopDienTuEntities db = null;

        public LoaiThanhVienDao()
        {
            db = new ShopDienTuEntities();
        }

        public IEnumerable<LoaiThanhVien> GetListByCondition(int a)
        {
            return db.LoaiThanhViens.Where(x => x.MaLoaiThanhVien < a);
        }

        public LoaiThanhVien GetByID(int? id)
        {
            return db.LoaiThanhViens.SingleOrDefault(x => x.MaLoaiThanhVien == id);
        }
    }
}
