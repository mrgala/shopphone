﻿using ShopDienTu.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopDienTu.Model.DAO
{
    public class PhieuNhapDao
    {
        ShopDienTuEntities db;

        public PhieuNhapDao()
        {
            db = new ShopDienTuEntities();
        }

        public void Add(PhieuNhap pn)
        {
            db.PhieuNhaps.Add(pn);
            db.SaveChanges();
        }

        public IEnumerable<PhieuNhap> GetAll()
        {
            return db.PhieuNhaps.OrderBy(x=>x.NgayNhap);
        }

        public void Delete(int id)
        {
            var pn = db.PhieuNhaps.SingleOrDefault(x => x.MaPN == id);
            db.PhieuNhaps.Remove(pn);
            db.SaveChanges();
        }

        public PhieuNhap GetByID(int id)
        {
            return db.PhieuNhaps.SingleOrDefault(x => x.MaPN == id);
        }
    }
}
