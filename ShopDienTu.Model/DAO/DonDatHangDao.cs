﻿using ShopDienTu.Model.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopDienTu.Model.DAO
{
    public class DonDatHangDao
    {
        ShopDienTuEntities db;

        public DonDatHangDao()
        {
            db = new ShopDienTuEntities();
        }

        public void AddDonHang(DonDatHang ddh)
        {
            db.DonDatHangs.Add(ddh);
            db.SaveChanges();
        }

        public DonDatHang GetByID(int? id)
        {
            return db.DonDatHangs.SingleOrDefault(x => x.MaDDH == id);
        }

        public void Update(DonDatHang ddh)
        {
            db.Entry(ddh).State = EntityState.Modified;
            db.SaveChanges();
        }
    }
}
