﻿var common = {
    init: function () {
        common.registerEvents();
    },
    registerEvents: function () {
        $("#txtKeyword").autocomplete({
            minLength: 2,
            source: function (request, response) {
                $.ajax({
                    url: "/sanpham/GetListProductByName",
                    dataType: "json",
                    data: {
                        keyword: request.term
                    },
                    success: function (res) {
                        response(res.data);
                    }
                });
            },
            focus: function (event, ui) {
                $("#txtKeyword").val(ui.item.TenSP);
                return false;
            },
            select: function (event, ui) {
                $("#txtKeyword").val(ui.item.TenSP);
                return false;
            }
        }).autocomplete("instance")._renderItem = function (ul, item) {
            return $("<li>")
              .append("<img class='imageClass' src=" + item.HinhAnh + " alt= " + item.TenSP + '/>')
              .append("<a href='" + 'chi-tiet-san-pham-' + item.TenNSX + '-' + item.MaSP +"'>"+ item.TenSP + "</a>")
              .appendTo(ul);
        };

    }
}
common.init();