﻿$(function () {
    $(".dateNhap").datepicker({ dateFormat: 'dd/mm/yy' });
});

var a = 0;
$('#btnAdd').click(function () {

    var id_cuoi = $(".tblChiTietPhieuNhap").find("tr:last-child").attr("data-id");

    i = parseInt(id_cuoi) + 1;
    //var noidung = "<tr><td>Cột 1</td><td>Cột 2</td><td>Cột 3</td></tr>"
    //$('.tblChiTietPhieuNhap').append(noidung);
    var tdnoidung = $(".trAppend").html();
    //Bước 2:Tạo 1 thẻ tr bao ngoài nội dung
    var trnoidung = "<tr class=\"trAppended\" data-id=\"" + i + "\">" + tdnoidung + "</tr>";
    ////Bước 3: Lấy thẻ table append vào 1 tr
    $(".tblChiTietPhieuNhap").append(trnoidung);
    loadIDLENTHE();

    a = a + 1;
    if (a <= 0) {
        $('#btnNhapHang').prop("disabled", true);
    } else {
        $('#btnNhapHang').prop("disabled", false);
    }
});

//Xử lý xóa basic
//$(".btnDelete").click(function () {
//    $(this).closest(".trAppend").remove();
//})

function loadIDLENTHE() {
    $(".trAppended").each(function () {
        //Lấy thuộc tính data-id của thẻ tr hiện
        var id = $(this).attr("data-id");
        var nameMaSanPham = "[" + id + "].MaSP"; //Tạo ra mã sản phẩm
        var nameSoLuongNhap = "[" + id + "].SoLuongNhap"; //Tạo ra số lượng nhập
        var nameDonGiaNhap = "[" + id + "].DonGiaNhap";   //Tạo ra đơn giá nhập
        $(this).find(".ddlSanPham").attr("name", nameMaSanPham);//Gán name cho dropdownlist
        $(this).find(".txtDonGia").attr("name", nameDonGiaNhap);//Gán name đơn giá nhập
        $(this).find(".txtSoLuong").attr("name", nameSoLuongNhap);//Gán name số lượng nhập
    })
};

function CapNhatID() {
    var id_cuoi = $(".tblChiTietPhieuNhap").find(".trFirstChild").attr("data-id");
    i = parseInt(id_cuoi) + 1;
    $(".trAppended").each(function () {
        var id = i;
        //Cập nhật id khi xóa
        $(this).attr("data-id", i);
        var nameMaSanPham = "[" + id + "].MaSP"; //Tạo ra mã sản phẩm
        var nameSoLuongNhap = "[" + id + "].SoLuongNhap"; //Tạo ra số lượng nhập
        var nameDonGiaNhap = "[" + id + "].DonGiaNhap";   //Tạo ra đơn giá nhập
        $(this).find(".ddlSanPham").attr("name", nameMaSanPham);//Gán name cho dropdownlist
        $(this).find(".txtDonGia").attr("name", nameDonGiaNhap);//Gán name đơn giá nhập
        $(this).find(".txtSoLuong").attr("name", nameSoLuongNhap);//Gán name số lượng nhập
        i++;
    })
}

//Xử lý xóa theo ủy nhiệm Delegate
$("body").delegate(".btnDelete", "click", function () {

    $(this).closest("tr").remove();
    CapNhatID();

    a = a - 1;
    if (a <= 0) {
        $('#btnNhapHang').prop("disabled", true);
    } else {
        $('#btnNhapHang').prop("disabled", false);
    }
})

$("#btnNhapHang").click(function () {
    if (kiemtraDonGia() == false) {
        return false
    } else {

    }
    //if (kiemtraSoLuong() == false) {
    //    return false
    //}
})

function kiemtraDonGia() {
    var bl = true;
    //Duyệt vòng lặp each
    var id_cuoi = $(".tblChiTietPhieuNhap").find(".trFirstChild").attr("data-id");
    i = parseInt(id_cuoi) + 1;
    $(".txtDonGia").each(function () {
        var id = i;
        var nameDonGia = "input[name=" + "'[" + id + "].DonGiaNhap'";
        var giatri = $(nameDonGia).val();
        giatri = giatri.replace(/\$|\,/g, '');
        if (giatri == 0) {
            alert("Đơn giá không hợp lệ");
            bl = false
            return bl;
        }
            $(nameDonGia).val(giatri);
            i++;
        })
    return bl;
}

function kiemtraSoLuong() {
    var bl = true;
    //Duyệt vòng lặp each
    $(".txtSoLuong").each(function () {
        var giatri = $(this).val();
        if (isNaN(giatri) == true || (giatri <= 0)) {
            alert("Đơn giá không hợp lệ");
            bl = false
            return bl;
        }
    })
    return bl;
}