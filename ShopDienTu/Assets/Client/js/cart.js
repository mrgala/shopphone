﻿
$("tbody").delegate(".btnDelete", "click", function () {
    $(this).closest("tr").remove();
    $("#btnDatHang").prop('disabled', true);
    $("#btnCapNhat").prop('disabled', false);
    CapNhatID();
    CapNhatTongCong();
    CapNhatSoLuong();
});

function CapNhatTongCong() {
    var lastID = $(".table-bordered").find(".trFirstChild").attr("data-id");
    var tongcong = 0;
    i = parseInt(lastID) + 1;
    $(".thanhtien").each(function () {
        var id = i;
        var nameThanhTien = "input[name=" + "'[" + id + "].ThanhTien'";
        var thanhtien = $(nameThanhTien).val();
        tongcong += parseInt(thanhtien);
        i++;
    })
    var tongcongText = formatCurrency(tongcong);
    $("#txtTongGia").text(tongcongText);
    $("#so").text(tongcongText);
    $("#txtTongCong").text(tongcongText);
}

function CapNhatID() {
    var lastID = $(".table-bordered").find(".trFirstChild").attr("data-id");
    i = parseInt(lastID) + 1;
    $(".trLstCart").each(function () {
        var id = i;
        //Cập nhật id khi xóa
        $(this).attr("data-id", i);
        var nameMaSP = "[" + id + "].MaSP";
        var nameTenSanPham = "[" + id + "].TenSP";
        var nameHinhAnh = "[" + id + "].HinhAnh";
        var nameSoLuong = "[" + id + "].SoLuong";
        var nameDonGia = "[" + id + "].DonGia";
        var nameThanhTien = "[" + id + "].ThanhTien";
        var classThanhTien = "txtThanhTien" + id;
        $(this).find(".masp").attr("name", nameMaSP);
        $(this).find(".tensp").attr("name", nameTenSanPham);
        $(this).find(".hinhanh").attr("name", nameHinhAnh);
        $(this).find(".soluong").attr("name", nameSoLuong);
        $(this).find(".dongia").attr("name", nameDonGia);
        $(this).find(".thanhtien").attr("name", nameThanhTien);

        $(this).find(".btn-minus").attr("value", i);
        $(this).find(".btn-plus").attr("value", i);

        $(this).find("#idThanhTien").removeClass().addClass(classThanhTien)
        i++;
    })
}

function CapNhatSoLuong() {
    var lastID = $(".table-bordered").find(".trFirstChild").attr("data-id");
    i = parseInt(lastID) + 1;
    var soluong = 0;
    $(".soluong").each(function () {
        var id = i;
        var nameSoLuong = "input[name=" + "'[" + id + "].SoLuong'";
        var sl = $(nameSoLuong).val();
        soluong += parseInt(sl);
        i++;
    })
    $("#idSoLuong").text(soluong)
}
$('.btn-plus').click(function () {
    var value = $(this).attr("value");

    var nameSoLuong = "input[name=" + "'[" + value + "].SoLuong'";
    var nameDonGia = "input[name=" + "'[" + value + "].DonGia'";
    var nameThanhTien = "input[name=" + "'[" + value + "].ThanhTien'";

    var soluong = $(nameSoLuong).val();
    var dongia = parseInt($(nameDonGia).val());



    soluong++;
    var total = soluong * dongia;

    $(nameSoLuong).val(soluong);
    $(nameThanhTien).val(total);

    var txtThanhTien = "." + "txtThanhTien" + value;
    var totalText = formatCurrency(total);
    $(txtThanhTien).text(totalText)

    CapNhatTongCong();
    CapNhatSoLuong();
    $("#btnDatHang").prop('disabled', true);
    $("#btnCapNhat").prop('disabled', false);


    //var tonggia = $("#txtTongGia").text();
    //tonggia = tonggia.replace(/\$|\,/g, '');
    //tonggia = parseInt(tonggia) + dongia;
    //$("#txtTongGia").text(formatCurrency(tonggia));
    //$("#so").text(formatCurrency(tonggia));
    //$("#txtTongCong").text(formatCurrency(tonggia));

})

$('.btn-minus').click(function () {
    var value = $(this).attr("value");

    var nameSoLuong = "input[name=" + "'[" + value + "].SoLuong'";
    var nameDonGia = "input[name=" + "'[" + value + "].DonGia'";
    var nameThanhTien = "input[name=" + "'[" + value + "].ThanhTien'";

    var soluong = $(nameSoLuong).val();
    var dongia = parseInt($(nameDonGia).val());

    //var tonggia = $("#txtTongGia").text();
    if (soluong <= 1) {
       soluong = 1;
    //    tonggia = tonggia.replace(/\$|\,/g, '');
    //    tonggia = parseInt(tonggia);
    //    $("#txtTongGia").text(formatCurrency(tonggia));
    //    $("#so").text(formatCurrency(tonggia));
    //    $("#txtTongCong").text(formatCurrency(tonggia));
    }
    else {
        soluong--;
    //    tonggia = tonggia.replace(/\$|\,/g, '');
    //    tonggia = parseInt(tonggia) - dongia;
    //    $("#txtTongGia").text(formatCurrency(tonggia));
    //    $("#so").text(formatCurrency(tonggia));
    //    $("#txtTongCong").text(formatCurrency(tonggia));
    }

    var total = soluong * dongia;

    $(nameSoLuong).val(soluong);
    $(nameThanhTien).val(total);

    var txtThanhTien = "." + "txtThanhTien" + value;
    var totalText = formatCurrency(total);
    $(txtThanhTien).text(totalText)

    CapNhatTongCong();
    CapNhatSoLuong();
    $("#btnDatHang").prop('disabled', true);
    $("#btnCapNhat").prop('disabled', false);

    

    //CapNhatTongCong();
})


//$(".soluong").delegate(".btn-plus", "click", function () {

//    var myClass = $(this).attr("value");
//    alert(myClass);
//});