﻿using AutoMapper;
using ShopDienTu.Infastructure.Core;
using ShopDienTu.Infastructure.Extension;
using ShopDienTu.Model.DAO;
using ShopDienTu.Model.Model;
using ShopDienTu.Models;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using PagedList;
using Microsoft.Ajax.Utilities;

namespace ShopDienTu.Controllers
{
    public class SanPhamController : BaseController
    {
        private SanPhamDao spDao;

        public SanPhamController()
        {
            spDao = new SanPhamDao();
        }

        public ActionResult Index(int id, int flag = 1, int page= 1, string sort="")
        {
            int PageSize = 6;
            int totalCount = 0;
            int PageNumber = page - 1; 

            var model = spDao.GetSortListByID(id, out totalCount,sort, page, PageSize).ToPagedList(PageNumber+1, PageSize);

            var viewModel = Mapper.Map<IEnumerable<SanPham>, IEnumerable<SanPhamViewModel>>(model);

            IPagedList<SanPhamViewModel> spView = new StaticPagedList<SanPhamViewModel>(viewModel, PageNumber+1, PageSize, totalCount);

            ViewBag.ID = id;

            ViewBag.Page = PageNumber + 1;

            ViewBag.Sort = sort;

            if (page == 1 && flag == 1)
            {
                return View(spView);
            }

            return PartialView("IndexPartial", spView);

        }

        public ActionResult Search(string keyword,int flag = 1,int page = 1)
        {
            if (string.IsNullOrEmpty(keyword))
            {
                return RedirectToAction("Index", "HomePage");
            }

            int PageSize = 6;
            int totalCount = 0;
            int PageNumber = page - 1;

            var model = spDao.Search(keyword, out totalCount).ToPagedList(PageNumber + 1, PageSize);
            var viewModel = Mapper.Map<IEnumerable<SanPham>, IEnumerable<SanPhamViewModel>>(model);

            ViewBag.Keyword = keyword;

            ViewBag.TotalCount = totalCount;

            IPagedList<SanPhamViewModel> spView = new StaticPagedList<SanPhamViewModel>(viewModel, PageNumber + 1, PageSize, totalCount);

            if (page == 1 && flag == 1)
            {
                return View(spView);
            }
            return PartialView("SearchPartial", spView);
        }

        public JsonResult GetListProductByName(string keyword)
        {
            var model = spDao.GetListProductByName(keyword);
            //var viewModel = Mapper.Map<IEnumerable<SanPham>, IEnumerable<SanPhamViewModel>>(model);
            List<Dictionary<string, string>> listJson = new List<Dictionary<string, string>>();
            foreach (var item in model)
            {
                Dictionary<string, string> dataJson = new Dictionary<string, string>();
                dataJson["TenSP"] = item.TenSP;
                dataJson["HinhAnh"] = item.HinhAnh;
                dataJson["MaSP"] = item.MaSP.ToString();
                dataJson["TenNSX"] = item.NhaSanXuat.TenNSX;
                listJson.Add(dataJson);
            }
            return Json(new
            {
                data = listJson
            }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Detail(int id)
        {
            var spModel = spDao.GetByID(id);
            if (spModel == null)
            {
                return HttpNotFound();
            }
            var spViewModel = new SanPhamViewModel();

            spViewModel.UpdateSanPham(spModel);

            List<string> lstImage = new JavaScriptSerializer().Deserialize<List<string>>(spViewModel.HinhAnh1);

            ViewBag.MoreImage = lstImage;

            var lstRelated = spDao.GetRelated(spViewModel.MaLoaiSP, spViewModel.MaNSX, 6);

            var lstRelatedViewModel = Mapper.Map<IEnumerable<SanPham>, IEnumerable<SanPhamViewModel>>(lstRelated);

            ViewBag.RelatedProduct = lstRelatedViewModel;

            return View(spViewModel);
        }
    }
}