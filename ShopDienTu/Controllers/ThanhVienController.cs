﻿using ShopDienTu.Areas.Admin.Models;
using ShopDienTu.Common;
using ShopDienTu.Model.DAO;
using System.Web.Mvc;
using ShopDienTu.Models;
using System.Collections.Generic;
using ShopDienTu.Model.Model;
using ShopDienTu.Infastructure.Extension;
using BotDetect.Web.Mvc;
using ShopDienTu.Infastructure.Core;

namespace ShopDienTu.Controllers
{
    public class ThanhVienController : BaseController
    {
        private ThanhVienDao thanhVienDao;

        public ThanhVienController()
        {
            thanhVienDao = new ThanhVienDao();
        }

        [HttpPost]
        public ActionResult DangNhap(string type,LoginModel loginModel)
        {
            //if(type == "btnCartLogin")
            if (ModelState.IsValid)
            {
                var result = thanhVienDao.DangNhap(loginModel.TaiKhoan, Encryptor.MD5Hash(loginModel.MatKhau), false);
                if (result == 1)
                {
                    var tv = thanhVienDao.GetUserByTK(loginModel.TaiKhoan, Encryptor.MD5Hash(loginModel.MatKhau));
                    var clientSession = new UserLogin();
                    clientSession.UserID = tv.MaThanhVien;
                    clientSession.UserName = tv.TaiKhoan;
                    clientSession.Name = tv.HoTen;
                    if (tv.MaLoaiTV == 6 || tv.MaLoaiTV == 7)
                    {
                        Session.Add(CommonConstants.USER_SESSION, clientSession);
                        return Content("<script>window.location.reload()</script>");
                    }
                    else
                    {
                        Session.Add(CommonConstants.ADMIN_SESSION, clientSession);
                        return Content("<script>window.location.href ='admin'</script>");
                        //return Content("<script>window.location ='@Url.Action(" + "\"Index" + "\"" + "," + "\"Home" + "\"" + ")';" + "</script>");
                    }
                }
                else if (result == 0)
                {
                    return Content("Tài khoản không tồn tại.");
                }
                else if (result == -1)
                {
                    return Content("Mật khẩu không đúng.");
                }
            }
            return Content("Tài khoản không tồn tại.");
        }

        public ActionResult DangXuat()
        {
            Session[CommonConstants.USER_SESSION] = null;
            return RedirectToAction("Index", "HomePage");
        }

        public List<string> LoadCauHoi()
        {
            List<string> listCauHoi = new List<string>();
            listCauHoi.Add("Ca sĩ bạn yêu thích là gì");
            listCauHoi.Add("Bạn đang làm nghề gì");
            listCauHoi.Add("Con vật bạn yêu thích");
            listCauHoi.Add("Ngày sinh của bạn");
            listCauHoi.Add("Bạn sinh ở đâu");

            return listCauHoi;
        }

        [HttpGet]
        public ActionResult DangKy()
        {
            ViewBag.CauHoi = new SelectList(LoadCauHoi());
            return View();
        }

        [ValidateInput(false)]
        [HttpPost]
        [CaptchaValidation("CaptchaCode", "registerCapcha", "Mã xác nhận không đúng")]
        public ActionResult DangKy(MemberViewModel viewModel)
        {
            ViewBag.CauHoi = new SelectList(LoadCauHoi());
            if (ModelState.IsValid)
            {
                if (thanhVienDao.CheckUserName(viewModel.TaiKhoan))
                {
                    ModelState.AddModelError("", "Tên tài khoản đã tồn tại");
                }
                else if (thanhVienDao.CheckEmail(viewModel.Email))
                {
                    ModelState.AddModelError("", "Email đã tồn tại");
                }
                else
                {
                    var thanhVienModel = new ThanhVien();
                    thanhVienModel.UpdateThanhVien(viewModel);
                    thanhVienModel.MaLoaiTV = 6;
                    var result = thanhVienDao.Add(thanhVienModel);
                    if (result == true)
                    {
                        SetAlert("Đăng ký thành công", "success");
                        return RedirectToAction("DangKy", "ThanhVien");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Đăng ký thất bại");
                    }
                }
            }
            return View(viewModel);
        }
    }
}