﻿using AutoMapper;
using ShopDienTu.Infastructure.Core;
using ShopDienTu.Model.DAO;
using ShopDienTu.Model.Model;
using ShopDienTu.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopDienTu.Controllers
{
    public class HomePageController : BaseController
    {
        private SanPhamDao sanphamDao;
        private SlideDao slideDao;

        public HomePageController()
        {
            sanphamDao = new SanPhamDao();
            slideDao = new SlideDao();
        }
        public ActionResult Index()
        {
            var homeVM = new HomeViewModel();

            var lstPhoneModel = sanphamDao.GetHot(1, 6);
            var lstPhoneViewModel = Mapper.Map<IEnumerable<SanPham>, IEnumerable<SanPhamViewModel>>(lstPhoneModel);

            var lstLaptopModel = sanphamDao.GetHot(2, 3);
            var lstLaptopViewModel = Mapper.Map<IEnumerable<SanPham>, IEnumerable<SanPhamViewModel>>(lstLaptopModel);

            var lstTabletModel = sanphamDao.GetHot(3, 6);
            var lstTabletViewModel = Mapper.Map<IEnumerable<SanPham>, IEnumerable<SanPhamViewModel>>(lstTabletModel);

            var lstSlideModel = slideDao.GetSlideAll();
            var lstSlideViewModel = Mapper.Map<IEnumerable<Slide>, IEnumerable<SlideViewModel>>(lstSlideModel);

            homeVM.HotPhone = lstPhoneViewModel;
            homeVM.HotLapTop = lstLaptopViewModel;
            homeVM.HotTablet = lstTabletViewModel;
            homeVM.HotSlide = lstSlideViewModel;

            return View(homeVM);
        }

        [ChildActionOnly]
        public ActionResult Header()
        {
            return PartialView();
        }

        [ChildActionOnly]
        public ActionResult Menu()
        {
            var lstSanPham = sanphamDao.GetAlls();
            var lstSanPhamViewModel = Mapper.Map<IEnumerable<SanPham>, IEnumerable<SanPhamViewModel>>(lstSanPham);
            return PartialView(lstSanPhamViewModel);
        }
    }
}