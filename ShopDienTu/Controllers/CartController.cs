﻿using ShopDienTu.Common;
using ShopDienTu.Infastructure.Core;
using ShopDienTu.Model.DAO;
using ShopDienTu.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ShopDienTu.Infastructure.Extension;
using ShopDienTu.Model.Model;
using AutoMapper;
using System.Configuration;

namespace ShopDienTu.Controllers
{
    public class CartController : BaseController
    {
        private SanPhamDao spDao;

        private KhachHangDao khDao;

        private ThanhVienDao tvDao;

        private DonDatHangDao ddhDao;

        private ItemGioHangDao cartDao;

        List<ItemGioHangViewModel> lstCart;

        CartViewModel cartModel;

        public CartController()
        {
            spDao = new SanPhamDao();
            cartDao = new ItemGioHangDao();
            khDao = new KhachHangDao();
            tvDao = new ThanhVienDao();
            ddhDao = new DonDatHangDao();
        }

        //private List<ItemGioHangViewModel> GetAll()
        //{            
        //    if (lstCart == null)
        //    {
        //        //Nếu sesion giỏ hàng chưa tồn tại
        //        lstCart = new List<ItemGioHangViewModel>();
        //        Session.Add(CommonConstants.CART_SESSION, lstCart);
        //    }
        //    return lstCart;
        //}

        private double TinhTongSoLuong()
        {
            lstCart = Session[CommonConstants.CART_SESSION] as List<ItemGioHangViewModel>;
            if (lstCart == null)
                return 0;
            return lstCart.Sum(n => n.SoLuong);
        }

        public ActionResult AddCart(int MaSP, string strUrl)
        {
            var sp = spDao.GetByID(MaSP);
            if (sp == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            lstCart = Session[CommonConstants.CART_SESSION] as List<ItemGioHangViewModel>;
            if (lstCart == null)
            {
                lstCart = new List<ItemGioHangViewModel>();
                Session.Add(CommonConstants.CART_SESSION, lstCart);
            }
            ItemGioHangViewModel spCheck = lstCart.SingleOrDefault(x => x.MaSP == MaSP);
            if (spCheck != null)
            {
                if (sp.SoLuongTon < spCheck.SoLuong)
                {
                    return View("Thông báo");
                }
                spCheck.SoLuong++;
                spCheck.ThanhTien = spCheck.SoLuong * spCheck.DonGia;
                SetAlert("Thêm giỏ hàng thành công", "success");
                return Redirect(strUrl);
            }
            else
            {
                var item = cartDao.SetNew(sp);
                if (sp.SoLuongTon < item.SoLuong)
                {
                    return View("Thông báo");
                }
                var itemVM = new ItemGioHangViewModel();
                itemVM.UpdateGioHang(item);
                lstCart.Add(itemVM);
                SetAlert("Thêm giỏ hàng thành công", "success");
                return Redirect(strUrl);
            }
        }

        [ChildActionOnly]
        public ActionResult Cart()
        {
            if (TinhTongSoLuong() == 0)
            {
                ViewBag.TongSoLuong = 0;
                return PartialView();
            }
            ViewBag.TongSoLuong = TinhTongSoLuong();
            return PartialView();
        }

        public ActionResult ViewCart()
        {
            cartModel = new CartViewModel();
            lstCart = Session[CommonConstants.CART_SESSION] as List<ItemGioHangViewModel>;
            cartModel.LstCart = lstCart;
            return View(cartModel);
        }

        [HttpPost]
        public ActionResult MyAction(string submitButton, List<ItemGioHangViewModel> _lstCart, KhachHangViewModel khModel)
        {
            switch (submitButton)
            {
                case "Đặt hàng":
                    return (DatHang(_lstCart, khModel));
                case "Cập nhật":
                    return (CapNhat(_lstCart));
                default:
                    return View();
            }
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult DatHang(List<ItemGioHangViewModel> lstCart, KhachHangViewModel khViewModel)
        {
            cartModel = new CartViewModel();

            var khModel = new KhachHang();

            var lstCartModel = Mapper.Map<List<ItemGioHangViewModel>, List<ItemGioHang>>(lstCart);

            string content = System.IO.File.ReadAllText(Server.MapPath("~/assets/client/template/neworder.html"));

            var toEmail = ConfigurationManager.AppSettings["ToEmailAddress"].ToString();

            if (Session[CommonConstants.USER_SESSION] != null)
            {
                var user = new UserLogin();
                user = (UserLogin)Session[CommonConstants.USER_SESSION];
                var tv = tvDao.GetByID(user.UserID);
                khModel = khDao.ConvertKhachHang(khModel,tv);
                khDao.AddKhachHang(khModel, lstCartModel);
                SetAlert("Bạn đã đặt hàng thành công, chúng tôi sẽ liên hệ với bạn để xác nhận ", "success");

                new MailHelper().ConfigMail(content, khModel.TenKH,khModel.SoDienThoai,khModel.Email,khModel.DiaChi,lstCart.Sum(x=>x.ThanhTien).ToString("N0"));

                new MailHelper().SendMail(khModel.Email, "Đơn hàng mới từ Hoài Vương Shop", content);
                new MailHelper().SendMail(toEmail, "Đơn hàng mới từ Hoài Vương Shop", content);
            }
            else
            {
                if (ModelState.IsValid)
                {

                    khModel.UpdateKhachHang(khViewModel);
                    khDao.AddKhachHang(khModel, lstCartModel);
                    SetAlert("Bạn đã đặt hàng thành công, chúng tôi sẽ liên hệ với bạn để xác nhận ", "success");

                    new MailHelper().ConfigMail(content, khModel.TenKH, khModel.SoDienThoai, khModel.Email, khModel.DiaChi, lstCart.Sum(x => x.ThanhTien).ToString("N0"));

                    new MailHelper().SendMail(khModel.Email, "Đơn hàng mới từ Hoài Vương Shop", content);
                    new MailHelper().SendMail(toEmail, "Đơn hàng mới từ Hoài Vương Shop", content);
                }
                else
                {
                    SetAlert("Bạn đặt hàng không thành công, vui lòng cho chúng tôi thêm chi tiết để liên hệ với bạn", "warning");
                    return RedirectToAction("ViewCart");
                }
            }

            Session[CommonConstants.CART_SESSION] = null;

            cartModel.LstCart = null;           

            return View("ViewCart", cartModel);
        }

        [HttpPost]
        public ActionResult CapNhat(List<ItemGioHangViewModel> _lstCart)
        {
            Session[CommonConstants.CART_SESSION] = null;
            if (_lstCart == null)
            {
                return RedirectToAction("ViewCart", "Cart");
            }
            else
            {
                Session.Add(CommonConstants.CART_SESSION, _lstCart);
                SetAlert("Cập nhật thành công", "success");
                return RedirectToAction("ViewCart", "Cart");
            }
        }
    }
}