﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Mail;

namespace ShopDienTu.Common
{
    public class MailHelper
    {
        public string ConfigMail(string content, string tenKH, string soDienThoai, string email, string diaChi, string thanhTien)
        {
            //string content = System.IO.File.ReadAllText(Server.MapPath("~/assets/client/template/neworder.html"));
            content = content.Replace("{{CustomerName}}", tenKH);
            content = content.Replace("{{Phone}}", soDienThoai);
            content = content.Replace("{{Email}}", email);
            content = content.Replace("{{Address}}", diaChi);
            content = content.Replace("{{Total}}", thanhTien);
            return content;
        }

        public void SendMail(string toEmailAddress, string subject, string content)
        {
            var fromEmailAddress = ConfigurationManager.AppSettings["FromEmailAddress"].ToString();
            var fromEmailDisplayName = ConfigurationManager.AppSettings["FromEmailDisplayName"].ToString();
            var fromEmailPassword = ConfigurationManager.AppSettings["FromEmailPassword"].ToString();
            var smtpHost = ConfigurationManager.AppSettings["SMTPHost"].ToString();
            var smtpPort = ConfigurationManager.AppSettings["SMTPPort"].ToString();

            bool enabledSsl = bool.Parse(ConfigurationManager.AppSettings["EnabledSSL"].ToString());

            string body = content;
            MailMessage message = new MailMessage(new MailAddress(fromEmailAddress, fromEmailDisplayName), new MailAddress(toEmailAddress));
            message.Subject = subject;
            message.IsBodyHtml = true;
            message.Body = body;

            var client = new SmtpClient();
            client.Credentials = new NetworkCredential(fromEmailAddress, fromEmailPassword);
            client.Host = smtpHost;
            client.EnableSsl = enabledSsl;
            client.Port = !string.IsNullOrEmpty(smtpPort) ? Convert.ToInt32(smtpPort) : 0;
            client.Send(message);
        }
    }
}