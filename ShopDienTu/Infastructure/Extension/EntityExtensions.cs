﻿using ShopDienTu.Common;
using ShopDienTu.Model.Model;
using ShopDienTu.Models;

namespace ShopDienTu.Infastructure.Extension
{
    public static class EntityExtensions
    {
        #region LoaiSanPham
        public static void UpdateLoaiSanPham(this LoaiSanPham loaisanpham, LoaiSanPhamViewModel loaisanphamVM)
        {
            loaisanpham.MaLoaiSP = loaisanphamVM.MaLoaiSP;
            loaisanpham.TenLoai = loaisanphamVM.TenLoai;
            loaisanpham.Icon = loaisanphamVM.Icon;
            loaisanpham.BiDanh = loaisanphamVM.BiDanh;
        }

        public static void UpdateLoaiSanPham(this LoaiSanPhamViewModel loaisanphamVM, LoaiSanPham loaisanpham)
        {
            loaisanphamVM.MaLoaiSP = loaisanpham.MaLoaiSP;
            loaisanphamVM.TenLoai = loaisanpham.TenLoai;
            loaisanphamVM.Icon = loaisanpham.Icon;
            loaisanphamVM.BiDanh = loaisanpham.BiDanh;
        }
        #endregion

        #region NhaCungCap
        public static void UpdateNhaCungCap(this NhaCungCap nhaCC, NhaCungCapViewModel nhaCCVM)
        {
            nhaCC.MaNCC = nhaCCVM.MaNCC;
            nhaCC.TenNCC = nhaCCVM.TenNCC;
            nhaCC.DiaChi = nhaCCVM.DiaChi;
            nhaCC.Email = nhaCCVM.Email;
            nhaCC.SoDienThoai = nhaCCVM.SoDienThoai;
            nhaCC.Fax = nhaCCVM.Fax;
        }

        public static void UpdateNhaCungCap(this NhaCungCapViewModel nhaCCVM, NhaCungCap nhaCC)
        {
            nhaCCVM.MaNCC = nhaCC.MaNCC;
            nhaCCVM.TenNCC = nhaCC.TenNCC;
            nhaCCVM.DiaChi = nhaCC.DiaChi;
            nhaCCVM.Email = nhaCC.Email;
            nhaCCVM.SoDienThoai = nhaCC.SoDienThoai;
            nhaCCVM.Fax = nhaCC.Fax;
        }
        #endregion

        #region NhaSanXuat
        public static void UpdateNhaSanXuat(this NhaSanXuat nhaSX, NhaSanXuatViewModel nhaSXVM)
        {
            nhaSX.MaNSX = nhaSXVM.MaNSX;
            nhaSX.TenNSX = nhaSXVM.TenNSX;
            nhaSX.ThongTin = nhaSXVM.ThongTin;
            nhaSX.Logo = nhaSXVM.Logo;
        }

        public static void UpdateNhaSanXuat(this NhaSanXuatViewModel nhaSXVM, NhaSanXuat nhaSX)
        {
            nhaSXVM.MaNSX = nhaSX.MaNSX;
            nhaSXVM.TenNSX = nhaSX.TenNSX;
            nhaSXVM.ThongTin = nhaSX.ThongTin;
            nhaSXVM.Logo = nhaSX.Logo;
        }
        #endregion

        #region SanPham
        public static void UpdateSanPham(this SanPham sanpham, SanPhamViewModel sanphamVM)
        {
            sanpham.MaSP = sanphamVM.MaSP;
            sanpham.TenSP = sanphamVM.TenSP;
            sanpham.DonGia = sanphamVM.DonGia;
            sanpham.NgayCapNhap = sanphamVM.NgayCapNhap;
            sanpham.CauHinh = sanphamVM.CauHinh;
            sanpham.MoTa = sanphamVM.MoTa;
            sanpham.HinhAnh = sanphamVM.HinhAnh;
            sanpham.SoLuongTon = sanphamVM.SoLuongTon;
            sanpham.LuotXem = sanphamVM.LuotXem;
            sanpham.LuotBinhChon = sanphamVM.LuotBinhChon;
            sanpham.LuotBinhLuan = sanphamVM.LuotBinhLuan;
            sanpham.SoLanMua = sanphamVM.SoLanMua;
            sanpham.Moi = sanphamVM.Moi;
            sanpham.MaNCC = sanphamVM.MaNCC;
            sanpham.MaNSX = sanphamVM.MaNSX;
            sanpham.MaLoaiSP = sanphamVM.MaLoaiSP;
            sanpham.DaXoa = sanphamVM.DaXoa;
            sanpham.HinhAnh1 = sanphamVM.HinhAnh1;
            sanpham.NhaSanXuat = sanphamVM.NhaSanXuat;
            sanpham.LoaiSanPham = sanphamVM.LoaiSanPham;
            sanpham.Home = sanphamVM.Home;
        }

        public static void UpdateSanPham(this SanPhamViewModel sanphamVM, SanPham sanpham)
        {
            sanphamVM.MaSP = sanpham.MaSP;
            sanphamVM.TenSP = sanpham.TenSP;
            sanphamVM.DonGia = sanpham.DonGia;
            sanphamVM.NgayCapNhap = sanpham.NgayCapNhap;
            sanphamVM.CauHinh = sanpham.CauHinh;
            sanphamVM.MoTa = sanpham.MoTa;
            sanphamVM.HinhAnh = sanpham.HinhAnh;
            sanphamVM.SoLuongTon = sanpham.SoLuongTon;
            sanphamVM.LuotXem = sanpham.LuotXem;
            sanphamVM.LuotBinhChon = sanpham.LuotBinhChon;
            sanphamVM.LuotBinhLuan = sanpham.LuotBinhLuan;
            sanphamVM.SoLanMua = sanpham.SoLanMua;
            sanphamVM.Moi = sanpham.Moi;
            sanphamVM.MaNCC = sanpham.MaNCC;
            sanphamVM.MaNSX = sanpham.MaNSX;
            sanphamVM.MaLoaiSP = sanpham.MaLoaiSP;
            sanphamVM.DaXoa = sanpham.DaXoa;
            sanphamVM.HinhAnh1 = sanpham.HinhAnh1;
            sanphamVM.NhaSanXuat = sanpham.NhaSanXuat;
            sanphamVM.LoaiSanPham = sanpham.LoaiSanPham;
            sanphamVM.Home = sanpham.Home;
        }
        #endregion

        #region PhieuNhap
        public static void UpdatePhieuNhap(this PhieuNhap phieunhap, PhieuNhapViewModel phieunhapVM)
        {
            phieunhap.MaPN = phieunhapVM.MaPN;
            phieunhap.MaNCC = phieunhapVM.MaNCC;
            phieunhap.NgayNhap = phieunhapVM.NgayNhap;
            phieunhap.DaXoa = phieunhapVM.DaXoa;
            phieunhap.NgaySua = phieunhapVM.NgaySua;
            phieunhap.MaTV = phieunhapVM.MaTV;
        }

        public static void UpdatePhieuNhap(this PhieuNhapViewModel phieunhapVM, PhieuNhap phieunhap)
        {
            phieunhapVM.MaPN = phieunhap.MaPN;
            phieunhapVM.MaNCC = phieunhap.MaNCC;
            phieunhapVM.NgayNhap = phieunhap.NgayNhap;
            phieunhapVM.NgaySua = phieunhap.NgaySua;
            phieunhapVM.DaXoa = phieunhap.DaXoa;
        }
        #endregion

        #region ChiTietPhieuNhap
        public static void UpdateCTPN(this ChiTietPhieuNhap ctpn, ChiTietPhieuNhapViewModel ctpnVM)
        {
            ctpn.MaChiTietPN = ctpnVM.MaChiTietPN;
            ctpn.MaPN = ctpnVM.MaPN;
            ctpn.MaSP = ctpnVM.MaSP;
            ctpn.DonGiaNhap = ctpnVM.DonGiaNhap;
            ctpn.SoLuongNhap = ctpnVM.SoLuongNhap;
        }

        public static void UpdateCTPN(this ChiTietPhieuNhapViewModel ctpnVM, ChiTietPhieuNhap ctpn)
        {
            ctpnVM.MaChiTietPN = ctpn.MaChiTietPN;
            ctpnVM.MaPN = ctpn.MaPN;
            ctpnVM.MaSP = ctpn.MaSP;
            ctpnVM.DonGiaNhap = ctpn.DonGiaNhap;
            ctpnVM.SoLuongNhap = ctpn.SoLuongNhap;
        }
        #endregion

        #region ThanhVien
        public static void UpdateThanhVien(this ThanhVien tv, MemberViewModel tvVM)
        {
            tv.MaThanhVien = tvVM.MaThanhVien;
            tv.TaiKhoan = tvVM.TaiKhoan;
            tv.MatKhau = Encryptor.MD5Hash(tvVM.MatKhau);
            tv.HoTen = tvVM.HoTen;
            tv.DiaChi = tvVM.DiaChi;
            tv.Email = tvVM.Email;
            tv.SoDienThoai = tvVM.SoDienThoai;
            tv.CauHoi = tvVM.CauHoi;
            tv.CauTraLoi = tvVM.CauTraLoi;
        }

        public static void UpdateThanhVien(this MemberViewModel tvVM, ThanhVien tv)
        {
            tvVM.MaThanhVien = tv.MaThanhVien;
            tvVM.TaiKhoan = tv.TaiKhoan;
            tvVM.MatKhau = tv.MatKhau;
            tvVM.HoTen = tv.HoTen;
            tvVM.DiaChi = tv.DiaChi;
            tvVM.Email = tv.Email;
            tvVM.SoDienThoai = tv.SoDienThoai;
            tvVM.CauHoi = tv.CauHoi;
            tvVM.CauTraLoi = tv.CauTraLoi;
        }
        #endregion

        #region GioHang
        public static void UpdateGioHang(this ItemGioHangViewModel itVM, ItemGioHang it)
        {
            itVM.MaSP = it.MaSP;
            itVM.TenSP = it.TenSP;
            itVM.SoLuong = it.SoLuong;
            itVM.HinhAnh = it.HinhAnh;
            itVM.DonGia = it.DonGia;
            itVM.ThanhTien = it.ThanhTien;
        }
        #endregion

        #region KhachHang
        public static void UpdateKhachHang(this KhachHang kh, KhachHangViewModel khVM)
        {
            kh.MaKH = khVM.MaKH;
            kh.TenKH = khVM.TenKH;
            kh.DiaChi = khVM.DiaChi;
            kh.Email = khVM.Email;
            kh.SoDienThoai = khVM.SoDienThoai;
            kh.MaThanhVien = khVM.MaThanhVien;
        }

        public static void UpdateKhachHang(this KhachHangViewModel khVM, KhachHang kh)
        {
            khVM.MaKH = kh.MaKH;
            khVM.TenKH = kh.TenKH;
            khVM.DiaChi = kh.DiaChi;
            khVM.Email = kh.Email;
            khVM.SoDienThoai = kh.SoDienThoai;
            khVM.MaThanhVien = kh.MaThanhVien;
        }
        #endregion

        #region DonDatHang
        public static void UpdateDonDatHang(this DonDatHangViewModel ddhVM, DonDatHang ddh)
        {
            ddhVM.MaDDH = ddh.MaDDH;
            ddhVM.NgayDat = ddh.NgayDat;
            ddhVM.TinhTrangGiaoHang = ddh.TinhTrangGiaoHang;
            ddhVM.NgayGiao = ddh.NgayGiao;
            ddhVM.DaThanhToan = ddh.DaThanhToan;
            ddhVM.MaKH = ddh.MaKH;
            ddhVM.UuDai = ddh.UuDai;
            ddhVM.DaHuy = ddh.DaHuy;
            ddhVM.DaXoa = ddh.DaXoa;
            ddhVM.ChiTietDonDatHangs = ddh.ChiTietDonDatHangs;
            ddhVM.KhachHang = ddh.KhachHang;
        }
        #endregion

        #region ChiTietDonHang
        public static void UpdateChiTietDonHang(this ChiTietDonDatHangViewModel ctdhVM, ChiTietDonDatHang ctdh)
        {
            ctdhVM.MaChiTietDDH = ctdh.MaChiTietDDH;
            ctdhVM.MaDDH = ctdh.MaDDH;
            ctdhVM.MaSP = ctdh.MaSP;
            ctdhVM.TenSP = ctdh.TenSP;
            ctdhVM.SoLuong = ctdh.SoLuong;
            ctdhVM.DonGia = ctdh.DonGia;
        }
        #endregion

        #region LoaiThanhVien
        public static void UpdateLoaiThanhVien(this LoaiThanhVienViewModel ltvVM, LoaiThanhVien ltv)
        {
            ltvVM.MaLoaiThanhVien = ltv.MaLoaiThanhVien;
            ltvVM.TenLoaiThanhVien = ltv.TenLoaiThanhVien;
            ltvVM.UuDai = ltv.UuDai;
        }
        #endregion

    }
}