﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ShopDienTu
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.IgnoreRoute("{*botdetect}",
            new { botdetect = @"(.*)BotDetectCaptcha\.ashx" });

            routes.MapRoute(
                name: "dangky",
                url: "dang-ky",
                defaults: new { controller = "ThanhVien", action = "DangKy", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "giohang",
                url: "gio-hang",
                defaults: new { controller = "Cart", action = "ViewCart", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "timkiem",
                url: "tim-kiem",
                defaults: new { controller = "SanPham", action = "Search", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "sanpham",
                url: "san-pham-{id}",
                defaults: new { controller = "SanPham", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "chitietsanpham",
                url: "chi-tiet-san-pham-{tennsx}-{id}",
                defaults: new { controller = "SanPham", action = "Detail", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "HomePage", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
