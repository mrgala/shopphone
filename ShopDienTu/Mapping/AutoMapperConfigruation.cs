﻿using AutoMapper;
using ShopDienTu.Model.Model;
using ShopDienTu.Models;

namespace ShopDienTu.Mapping
{
    public class AutoMapperConfigruation
    {
        public static void Configure()
        {
            Mapper.Initialize(cg =>
            {
                cg.CreateMap<LoaiSanPham, LoaiSanPhamViewModel>();

                cg.CreateMap<NhaCungCap, NhaCungCapViewModel>();

                cg.CreateMap<NhaSanXuat, NhaSanXuatViewModel>();

                cg.CreateMap<SanPham, SanPhamViewModel>();

                cg.CreateMap<ChiTietPhieuNhapViewModel, ChiTietPhieuNhap>();

                cg.CreateMap<ChiTietPhieuNhap, ChiTietPhieuNhapViewModel>();

                cg.CreateMap<Slide, SlideViewModel>();

                cg.CreateMap<ItemGioHangViewModel, ItemGioHang>();

                cg.CreateMap<DonDatHang, DonDatHangViewModel>();

                cg.CreateMap<ChiTietDonDatHang, ChiTietDonDatHangViewModel>();

                cg.CreateMap<LoaiThanhVien, LoaiThanhVienViewModel>();

                cg.CreateMap<PhieuNhap, PhieuNhapViewModel>();
            });
        }
    }
}