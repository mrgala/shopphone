﻿using ShopDienTu.Infastructure.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopDienTu.Areas.Admin.Controllers
{
    public class HomeController : BaseController
    {
        // GET: Admin/Home
        public ActionResult Index()
        {
            if (Session[Common.CommonConstants.ADMIN_SESSION] != null)
                return View();
            return RedirectToAction("Index", "DangNhap");
        }
    }
}