﻿using AutoMapper;
using ShopDienTu.Infastructure.Core;
using ShopDienTu.Infastructure.Extension;
using ShopDienTu.Model.DAO;
using ShopDienTu.Model.Model;
using ShopDienTu.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopDienTu.Areas.Admin.Controllers
{
    [AuthorizeBase(Roles = "QuanTri")]
    public class QuanLyDanhMucController : BaseController
    {
        // GET: Admin/QuanLyDanhMuc
        #region QuanLyDanhMuc
        [Authorize(Roles = "QuanTri")]
        [HttpGet]
        public ActionResult Index()
        {
            var loaiSPDao = new LoaiSanPhamDao();

            var model = loaiSPDao.GetAll();

            var viewModel = Mapper.Map<IEnumerable<LoaiSanPham>, IEnumerable<LoaiSanPhamViewModel>>(model);

            return View(viewModel);
        }

        [HttpGet]
        public ActionResult ThemDanhMuc()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ThemDanhMuc(LoaiSanPhamViewModel loaiSPViewModel)
        {
            if (ModelState.IsValid)
            {
                var loaiSPDao = new LoaiSanPhamDao();
                var loaiSP = new LoaiSanPham();
                loaiSP.UpdateLoaiSanPham(loaiSPViewModel);
                loaiSPDao.Add(loaiSP);
                return RedirectToAction("Index");
            }
            return View(loaiSPViewModel);
        }

        [HttpGet]
        public ActionResult SuaDanhMuc(int id)
        {
            var loaiSPDao = new LoaiSanPhamDao();

            var loaiSP = loaiSPDao.GetByID(id);

            var loaiSPViewModel = new LoaiSanPhamViewModel();

            loaiSPViewModel.UpdateLoaiSanPham(loaiSP);

            return View(loaiSPViewModel);
        }

        [HttpPost]
        public ActionResult SuaDanhMuc(LoaiSanPhamViewModel loaiSPViewModel)
        {
            if (ModelState.IsValid)
            {
                var loaiSPDao = new LoaiSanPhamDao();

                var loaiSP = new LoaiSanPham();

                loaiSP.UpdateLoaiSanPham(loaiSPViewModel);

                loaiSPDao.Update(loaiSP);

                return RedirectToAction("Index");
            }
            return View(loaiSPViewModel);
        }

        [HttpGet]
        public ActionResult XoaDanhMuc(int id)
        {
            var loaiSPDao = new LoaiSanPhamDao();

            loaiSPDao.Delete(id);

            return RedirectToAction("Index");
        }
        #endregion
    }
}