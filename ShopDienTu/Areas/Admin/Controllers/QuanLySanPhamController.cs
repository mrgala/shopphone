﻿using AutoMapper;
using ShopDienTu.Infastructure.Extension;
using ShopDienTu.Model.DAO;
using ShopDienTu.Model.Model;
using ShopDienTu.Models;
using System.Collections.Generic;
using System.Web.Mvc;
using PagedList;
using System;
using ShopDienTu.Infastructure.Core;

namespace ShopDienTu.Areas.Admin.Controllers
{
    [AuthorizeBase(Roles = "QuanTri,QuanLySanPham")]
    public class QuanLySanPhamController : BaseController
    {
        // GET: Admin/QuanLySanPham
        SanPhamDao sanphamDao;
        NhaCungCapDao nhaCCDao;
        LoaiSanPhamDao loaiSPDao;
        NhaSanXuatDao nhaSXDao;


        public QuanLySanPhamController()
        {
            sanphamDao = new SanPhamDao();
            nhaCCDao = new NhaCungCapDao();
            loaiSPDao = new LoaiSanPhamDao();
            nhaSXDao = new NhaSanXuatDao();
        }

        #region QuanLySanPham
        [Authorize(Roles = "QuanTri,QuanLySanPham")]
        public ActionResult Index(int? page)
        {
            int PageSize = 10;

            int PageNumber = (page ?? 1) - 1;

            int totalCount = 0;

            var model = sanphamDao.GetAll(out totalCount).ToPagedList(PageNumber + 1, PageSize);

            var viewModel = Mapper.Map<IEnumerable<SanPham>, IEnumerable<SanPhamViewModel>>(model);

            IPagedList<SanPhamViewModel> spView = new StaticPagedList<SanPhamViewModel>(viewModel, PageNumber + 1, PageSize, totalCount);

            return View(spView);
        }

        [HttpGet]
        public ActionResult TimKiemSanPham(string sTuKhoa, int? page)
        {

            int PageSize = 10;

            int PageNumber = (page ?? 1) - 1;

            int totalCount = 0;

            if (String.IsNullOrEmpty(sTuKhoa))
            {
                var model = sanphamDao.GetAll(out totalCount).ToPagedList(PageNumber + 1, PageSize);

                var viewModel = Mapper.Map<IEnumerable<SanPham>, IEnumerable<SanPhamViewModel>>(model);

                IPagedList<SanPhamViewModel> spView = new StaticPagedList<SanPhamViewModel>(viewModel, PageNumber + 1, PageSize, totalCount);

                return PartialView("IndexPartial", spView);
            }
            else
            {
                var model = sanphamDao.Search(sTuKhoa, out totalCount).ToPagedList(PageNumber + 1, PageSize);

                var viewModel = Mapper.Map<IEnumerable<SanPham>, IEnumerable<SanPhamViewModel>>(model);

                IPagedList<SanPhamViewModel> spView = new StaticPagedList<SanPhamViewModel>(viewModel, PageNumber + 1, PageSize, totalCount);

                ViewBag.TuKhoa = sTuKhoa;

                return PartialView("TimKiemSanPham", spView);
            }

        }

        [HttpGet]
        public ActionResult ThemSanPham()
        {
            ViewBag.MaNCC = new SelectList(nhaCCDao.GetAll(), "MaNCC", "TenNCC");
            ViewBag.MaLoaiSP = new SelectList(loaiSPDao.GetAll(), "MaLoaiSP", "TenLoai");
            ViewBag.MaNSX = new SelectList(nhaSXDao.GetAll(), "MaNSX", "TenNSX");
            return View();
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult ThemSanPham(SanPhamViewModel spViewModel)
        {

            ViewBag.MaNCC = new SelectList(nhaCCDao.GetAll(), "MaNCC", "TenNCC");
            ViewBag.MaLoaiSP = new SelectList(loaiSPDao.GetAll(), "MaLoaiSP", "TenLoai");
            ViewBag.MaNSX = new SelectList(nhaSXDao.GetAll(), "MaNSX", "TenNSX");

            if (ModelState.IsValid)
            {
                spViewModel.NgayCapNhap = DateTime.Now;
                var sanpham = new SanPham();
                sanpham.UpdateSanPham(spViewModel);
                sanphamDao.Add(sanpham);
                SetAlert("Thêm sản phẩm thành công", "success");
                return RedirectToAction("Index");
            }
            SetAlert("Thêm sản phẩm không thành công", "error");
            return View(spViewModel);
        }

        [HttpGet]
        public ActionResult SuaSanPham(int id)
        {
            var sanpham = sanphamDao.GetByID(id);

            var sanphamViewModel = new SanPhamViewModel();

            sanphamViewModel.UpdateSanPham(sanpham);

            ViewBag.MaNCC = new SelectList(nhaCCDao.GetAll(), "MaNCC", "TenNCC", sanpham.MaNCC);
            ViewBag.MaLoaiSP = new SelectList(loaiSPDao.GetAll(), "MaLoaiSP", "TenLoai", sanpham.MaLoaiSP);
            ViewBag.MaNSX = new SelectList(nhaSXDao.GetAll(), "MaNSX", "TenNSX", sanpham.MaNSX);

            return View(sanphamViewModel);
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult SuaSanPham(SanPhamViewModel spVM)
        {
            ViewBag.MaNCC = new SelectList(nhaCCDao.GetAll(), "MaNCC", "TenNCC", spVM.MaNCC);
            ViewBag.MaLoaiSP = new SelectList(loaiSPDao.GetAll(), "MaLoaiSP", "TenLoai", spVM.MaLoaiSP);
            ViewBag.MaNSX = new SelectList(nhaSXDao.GetAll(), "MaNSX", "TenNSX", spVM.MaNSX);

            if (ModelState.IsValid)
            {
                spVM.NgayCapNhap = DateTime.Now;

                var sanpham = new SanPham();

                sanpham.UpdateSanPham(spVM);

                sanphamDao.Update(sanpham);

                SetAlert("Sửa sản phẩm thành công", "success");

                return RedirectToAction("Index");
            }
            SetAlert("Sửa sản phẩm không thành công", "error");
            return View(spVM);
        }

        [HttpGet]
        public ActionResult SapHetHang(int ?page)
        {
            int PageSize = 5;

            int PageNumber = (page ?? 1) - 1;

            int totalCount = 0;

            var model = sanphamDao.GetOutOfStock(out totalCount).ToPagedList(PageNumber + 1, PageSize);

            var viewModel = Mapper.Map<IEnumerable<SanPham>, IEnumerable<SanPhamViewModel>>(model);

            IPagedList<SanPhamViewModel> spView = new StaticPagedList<SanPhamViewModel>(viewModel, PageNumber + 1, PageSize, totalCount);

            return PartialView("SapHetHang", spView);

        }

        [HttpGet]
        public ActionResult XoaSanPham(int id)
        {
            var sanphamDao = new SanPhamDao();

            sanphamDao.Delete(id);

            return RedirectToAction("Index");
        }

        #endregion QuanLySanPham
    }
}