﻿using AutoMapper;
using ShopDienTu.Infastructure.Core;
using ShopDienTu.Infastructure.Extension;
using ShopDienTu.Model.DAO;
using ShopDienTu.Model.Model;
using ShopDienTu.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopDienTu.Areas.Admin.Controllers
{
    [AuthorizeBase(Roles = "QuanTri")]
    public class QuanLyNhaSanXuatController : BaseController
    {
        private NhaSanXuatDao nhaSXDao;

        public QuanLyNhaSanXuatController()
        {
            nhaSXDao = new NhaSanXuatDao();
        }

        #region QuanLyNhaCungCap
        [Authorize(Roles = "QuanTri")]
        [HttpGet]
        public ActionResult Index()
        {

            var model = nhaSXDao.GetAll();

            var viewModel = Mapper.Map<IEnumerable<NhaSanXuat>, IEnumerable<NhaSanXuatViewModel>>(model);

            return View(viewModel);
        }

        [HttpGet]
        public ActionResult ThemNhaSX()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ThemNhaSX(NhaSanXuatViewModel nhaSXViewModel)
        {
            if (ModelState.IsValid)
            {
                var nhaSX = new NhaSanXuat();
                nhaSX.UpdateNhaSanXuat(nhaSXViewModel);
                nhaSXDao.Add(nhaSX);
                return RedirectToAction("Index");
            }
            return View(nhaSXViewModel);
        }

        [HttpGet]
        public ActionResult SuaNhaSX(int id)
        {
            var nhaSX = nhaSXDao.GetByID(id);

            var nhaSXViewModel = new NhaSanXuatViewModel();

            nhaSXViewModel.UpdateNhaSanXuat(nhaSX);

            return View(nhaSXViewModel);
        }

        [HttpPost]
        public ActionResult SuaNhaSX(NhaSanXuatViewModel nhaSXViewModel)
        {
            if (ModelState.IsValid)
            {
                var nhaSX = new NhaSanXuat();

                nhaSX.UpdateNhaSanXuat(nhaSXViewModel);

                nhaSXDao.Update(nhaSX);

                return RedirectToAction("Index");
            }
            return View(nhaSXViewModel);
        }

        [HttpGet]
        public ActionResult XoaNhaSX(int id)
        {
            nhaSXDao.Delete(id);

            return RedirectToAction("Index");
        }
        #endregion
    }
}