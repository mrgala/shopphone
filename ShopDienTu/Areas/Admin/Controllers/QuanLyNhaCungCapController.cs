﻿using AutoMapper;
using ShopDienTu.Infastructure.Core;
using ShopDienTu.Infastructure.Extension;
using ShopDienTu.Model.DAO;
using ShopDienTu.Model.Model;
using ShopDienTu.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopDienTu.Areas.Admin.Controllers
{
    [AuthorizeBase(Roles = "QuanTri")]
    public class QuanLyNhaCungCapController : BaseController
    {
        // GET: Admin/QuanLyNhaCungCap
        #region QuanLyNhaCungCap
        [Authorize(Roles = "QuanTri")]
        [HttpGet]
        public ActionResult Index()
        {
            var nhaCCDao = new NhaCungCapDao();

            var model = nhaCCDao.GetAll();

            var viewModel = Mapper.Map<IEnumerable<NhaCungCap>, IEnumerable<NhaCungCapViewModel>>(model);

            return View(viewModel);
        }

        [HttpGet]
        public ActionResult ThemNhaCC()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ThemNhaCC(NhaCungCapViewModel nhaCCViewModel)
        {
            if (ModelState.IsValid)
            {
                var nhaCCDao = new NhaCungCapDao();
                var nhaCC = new NhaCungCap();
                nhaCC.UpdateNhaCungCap(nhaCCViewModel);
                nhaCCDao.Add(nhaCC);
                return RedirectToAction("Index");
            }
            return View(nhaCCViewModel);
        }

        [HttpGet]
        public ActionResult SuaNhaCC(int id)
        {
            var nhaCCDao = new NhaCungCapDao();

            var nhaCC = nhaCCDao.GetByID(id);

            var nhaCCViewModel = new NhaCungCapViewModel();

            nhaCCViewModel.UpdateNhaCungCap(nhaCC);

            return View(nhaCCViewModel);
        }

        [HttpPost]
        public ActionResult SuaNhaCC(NhaCungCapViewModel nhaCCViewModel)
        {
            if (ModelState.IsValid)
            {
                var nhaCCDao = new NhaCungCapDao();

                var nhaCC = new NhaCungCap();

                nhaCC.UpdateNhaCungCap(nhaCCViewModel);

                nhaCCDao.Update(nhaCC);

                return RedirectToAction("Index");
            }
            return View(nhaCCViewModel);
        }

        [HttpGet]
        public ActionResult XoaNhaCC(int id)
        {
            var nhaCCDao = new NhaCungCapDao();

            nhaCCDao.Delete(id);

            return RedirectToAction("Index");
        }
        #endregion
    }
}