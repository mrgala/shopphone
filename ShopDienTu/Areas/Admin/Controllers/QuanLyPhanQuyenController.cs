﻿using AutoMapper;
using ShopDienTu.Infastructure.Core;
using ShopDienTu.Model.DAO;
using ShopDienTu.Model.Model;
using ShopDienTu.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ShopDienTu.Infastructure.Extension;

namespace ShopDienTu.Areas.Admin.Controllers
{
    [AuthorizeBase(Roles = "QuanTri")]
    public class QuanLyPhanQuyenController : BaseController
    {
        // GET: Admin/QuanLyPhanQuyen
        private LoaiThanhVienDao loaiTvDao;

        private QuyenDao quyenDao;

        private LoaiThanhVienQuyenDao loaiTVQuyenDao;

        public QuanLyPhanQuyenController()
        {
            loaiTvDao = new LoaiThanhVienDao();
            quyenDao = new QuyenDao();
            loaiTVQuyenDao = new LoaiThanhVienQuyenDao();
        }
        [Authorize(Roles = "QuanTri")]
        public ActionResult Index()
        {
            var lstLoaiTV = loaiTvDao.GetListByCondition(6);
            var lstLoaiTVVM = Mapper.Map<IEnumerable<LoaiThanhVien>, IEnumerable<LoaiThanhVienViewModel>>(lstLoaiTV);
            return View(lstLoaiTVVM);
        }

        [HttpGet]
        public ActionResult PhanQuyen(int? id)
        {
            if (id == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            var ltv = loaiTvDao.GetByID(id);
            if (ltv == null)
            {
                return HttpNotFound();
            }
            var ltvVM = new LoaiThanhVienViewModel();
            ltvVM.UpdateLoaiThanhVien(ltv);
            //Lấy danh sách quyền
            ViewBag.MaQuyen = quyenDao.GetAll();
            //Lấy danh sách quyền của thành viên đó
            ViewBag.LoaiTVQuyen = loaiTVQuyenDao.GetListByID(id);
            return View(ltvVM);
        }

        [HttpPost]
        public ActionResult PhanQuyen(int? id, IEnumerable<LoaiThanhVien_Quyen> lstPhanQuyen)
        {
            if (lstPhanQuyen == null)
            {
                return RedirectToAction("Index");
            }
            else
            {
                var lstDaPhanQuyen = loaiTVQuyenDao.GetListByID(id);
                if (lstDaPhanQuyen.Count() != 0)
                {
                    loaiTVQuyenDao.DeleteList(lstDaPhanQuyen);
                }
                foreach (var item in lstPhanQuyen)
                {
                    item.MaLoaiTV = int.Parse(id.ToString());
                    loaiTVQuyenDao.Add(item);
                }
                loaiTVQuyenDao.Save();

            }
            SetAlert("Cập nhật thành công", "success");    
            return RedirectToAction("Index");
        }
    }
}