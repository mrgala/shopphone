﻿using ShopDienTu.Areas.Admin.Models;
using ShopDienTu.Common;
using ShopDienTu.Infastructure.Core;
using ShopDienTu.Model.DAO;
using ShopDienTu.Model.Model;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ShopDienTu.Areas.Admin.Controllers
{
    public class DangNhapController : Controller
    {
        private ThanhVienDao thanhVienDao;
        private LoaiThanhVienQuyenDao loatTVQuyenDao;

        public DangNhapController()
        {
            thanhVienDao = new ThanhVienDao();
            loatTVQuyenDao = new LoaiThanhVienQuyenDao();
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(LoginModel loginModel)
        {
            if (ModelState.IsValid)
            {
                var result = thanhVienDao.DangNhap(loginModel.TaiKhoan,Encryptor.MD5Hash(loginModel.MatKhau), true);
                if (result == 1)
                {
                    var tv = thanhVienDao.GetUserByTK(loginModel.TaiKhoan, Encryptor.MD5Hash(loginModel.MatKhau));
                    var lstQuyen = loatTVQuyenDao.GetListByID(tv.MaLoaiTV);

                    string quyen = "";
                    foreach (var item in lstQuyen)
                    {
                        quyen += item.Quyen.MaQuyen + ",";
                    }
                    if (!string.IsNullOrEmpty(quyen))
                    {
                        quyen = quyen.Substring(0, quyen.Length - 1);
                    }
                    PhanQuyen(tv.TaiKhoan.ToString(), quyen);
                    var adminSession = new UserLogin();
                    adminSession.UserID = tv.MaThanhVien;
                    adminSession.UserName = tv.TaiKhoan;
                    adminSession.Name = tv.HoTen;
                    Session.Add(CommonConstants.ADMIN_SESSION, adminSession);
                    return RedirectToAction("Index", "Home");
                }
                else if (result == 0)
                {
                    ModelState.AddModelError("", "Tài khoản không tồn tại.");
                }
                else if (result == -1)
                {
                    ModelState.AddModelError("", "Mật khẩu không đúng.");
                }
                else if (result == -2)
                {
                    ModelState.AddModelError("", "Bạn không có quyền đăng nhập.");
                }
            }

            return View(loginModel);
        }

        public void PhanQuyen(string TaiKhoan, string Quyen)
        {
            FormsAuthentication.Initialize();

            var ticket = new FormsAuthenticationTicket(1,
                TaiKhoan, //user
                DateTime.Now, //timeput
                DateTime.Now.AddHours(3),
                false, //remember
                Quyen, //permission
                FormsAuthentication.FormsCookiePath);

            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(ticket));
            if (ticket.IsPersistent) cookie.Expires = ticket.Expiration;
            Response.Cookies.Add(cookie);
        }

        public ActionResult DangXuat()
        {
            Session[CommonConstants.ADMIN_SESSION] = null;
            FormsAuthentication.SignOut();
            return RedirectToAction("Index");
        }
    }
}