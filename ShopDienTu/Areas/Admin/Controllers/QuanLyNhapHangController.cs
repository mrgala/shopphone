﻿using ShopDienTu.Model.DAO;
using ShopDienTu.Model.Model;
using System.Web.Mvc;
using ShopDienTu.Infastructure.Extension;
using ShopDienTu.Models;
using System;
using AutoMapper;
using System.Collections.Generic;
using ShopDienTu.Infastructure.Core;
using ShopDienTu.Common;

namespace ShopDienTu.Areas.Admin.Controllers
{
    [AuthorizeBase(Roles = "QuanTri,QuanLyNhapHang")]
    public class QuanLyNhapHangController : BaseController
    {
        private SanPhamDao sanphamDao;

        private NhaCungCapDao nhaCCDao;

        private PhieuNhapDao phieuNhapDao;

        private ChiTietPhieuNhapDao ctpnDao;

        private ThanhVienDao tvDao;

        public QuanLyNhapHangController()
        {
            sanphamDao = new SanPhamDao();
            nhaCCDao = new NhaCungCapDao();
            phieuNhapDao = new PhieuNhapDao();
            ctpnDao = new ChiTietPhieuNhapDao();
            tvDao = new ThanhVienDao();
        }

        [Authorize(Roles = "QuanTri,QuanLyNhapHang")]
        [HttpGet]
        public ActionResult Index()
        {
            var nhaCCModel = nhaCCDao.GetAll();
            var nhaCCViewModel = Mapper.Map<IEnumerable<NhaCungCap>, IEnumerable<NhaCungCapViewModel>>(nhaCCModel);

            var sanphamModel = sanphamDao.GetAlls();
            var sanphamViewModel = Mapper.Map<IEnumerable<SanPham>, IEnumerable<SanPhamViewModel>>(sanphamModel);
            ViewBag.MaNCC = nhaCCViewModel;
            ViewBag.ListSanPham = sanphamViewModel;
            return View();
        }

        [HttpPost]
        public ActionResult NhapHang(PhieuNhapViewModel pnViewModel, IEnumerable<ChiTietPhieuNhapViewModel> lstViewModel)
        {
            var nhaCCModel = nhaCCDao.GetAll();
            var nhaCCViewModel = Mapper.Map<IEnumerable<NhaCungCap>, IEnumerable<NhaCungCapViewModel>>(nhaCCModel);

            var sanphamModel = sanphamDao.GetAlls();
            var sanphamViewModel = Mapper.Map<IEnumerable<SanPham>, IEnumerable<SanPhamViewModel>>(sanphamModel);
            ViewBag.MaNCC = nhaCCViewModel;
            ViewBag.ListSanPham = sanphamViewModel;

            pnViewModel.DaXoa = false;
            pnViewModel.NgayNhap = DateTime.Now;
            var user = (UserLogin)Session[Common.CommonConstants.ADMIN_SESSION];
            pnViewModel.MaTV = user.UserID;
            var pnModel = new PhieuNhap();
            pnModel.UpdatePhieuNhap(pnViewModel);
            phieuNhapDao.Add(pnModel);

            SanPham sp = new SanPham();
            foreach (var item in lstViewModel)
            {
                sp = sanphamDao.GetByID(item.MaSP);
                sp.SoLuongTon += item.SoLuongNhap;
                sanphamDao.Update(sp);
                item.MaPN = pnModel.MaPN;
            }

            var lstCTPNModel = Mapper.Map<IEnumerable<ChiTietPhieuNhapViewModel>, IEnumerable<ChiTietPhieuNhap>>(lstViewModel);
            ctpnDao.AddRange(lstCTPNModel);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult NhapHangDon(int id)
        {
            var model = sanphamDao.GetByID(id);

            var viewModel = new SanPhamViewModel();

            viewModel.UpdateSanPham(model);

            ViewBag.NCC = new SelectList(nhaCCDao.GetAll(), "MaNCC", "TenNCC", model.MaNCC);

            return View(viewModel);

        }

        [HttpPost]
        public ActionResult NhapHangDon(PhieuNhapViewModel pnViewModel, ChiTietPhieuNhapViewModel ctpnViewModel)
        {
            if(ModelState.IsValid)
            {
                ViewBag.NCC = new SelectList(nhaCCDao.GetAll(), "MaNCC", "TenNCC", pnViewModel.MaNCC);
                pnViewModel.DaXoa = false;
                pnViewModel.NgayNhap = DateTime.Now;

                var pnModel = new PhieuNhap();
                pnModel.UpdatePhieuNhap(pnViewModel);
                phieuNhapDao.Add(pnModel);

                ctpnViewModel.MaPN = pnModel.MaPN;
                var ctpnModel = new ChiTietPhieuNhap();
                ctpnModel.UpdateCTPN(ctpnViewModel);
                ctpnDao.Add(ctpnModel);

                var sanphamModel = sanphamDao.GetByID(ctpnViewModel.MaSP);
                sanphamModel.SoLuongTon += ctpnViewModel.SoLuongNhap;
                sanphamDao.Update(sanphamModel);
                return RedirectToAction("Index", "QuanLySanPham");
            }
            return HttpNotFound();
        }

        [HttpGet]
        public ActionResult ViewListImport()
        {
            var listImportModel = phieuNhapDao.GetAll();

            var listImportViewModel = Mapper.Map<IEnumerable<PhieuNhap>, IEnumerable<PhieuNhapViewModel>>(listImportModel);

            return View(listImportViewModel);
        }

        [HttpGet]
        public ActionResult DeleteImport(int id)
        {
            var lstCTPN = ctpnDao.GetListByID(id);
            foreach (var item in lstCTPN)
            {
                var sanpham = sanphamDao.GetByID(item.MaSP);
                sanpham.SoLuongTon -= item.SoLuongNhap;
                sanphamDao.Update(sanpham);
            }
            ctpnDao.DeletList(lstCTPN);
            phieuNhapDao.Delete(id);
            SetAlert("Xoa thanh cong", "success");
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult EditImport(int id)
        {
            var lstCTPNModel = ctpnDao.GetListByID(id);

            var phieuNhapModel = phieuNhapDao.GetByID(id);

            var phieuNhapViewModel = new PhieuNhapViewModel();

            phieuNhapViewModel.UpdatePhieuNhap(phieuNhapModel);

            var thanhVien = tvDao.GetByID(phieuNhapModel.MaTV);
            ViewBag.Ten = thanhVien.HoTen;
            var lstCTPNViewModel = Mapper.Map<IEnumerable<ChiTietPhieuNhap>, IEnumerable<ChiTietPhieuNhapViewModel>>(lstCTPNModel);
            ViewBag.ListCTPN = lstCTPNViewModel;

            int k = 0;
            foreach (var item in lstCTPNViewModel)
            {
                TempData["soluongnhap" + k] = item.SoLuongNhap;
                k++;
            }

            return View(phieuNhapViewModel);
        }

        [HttpPost]
        public ActionResult EditImport(PhieuNhapViewModel pnVM,List<ChiTietPhieuNhapViewModel> lstCTPN)
        {
            int[] mangNew = new int[lstCTPN.Count];
            int[] mangOld = new int[lstCTPN.Count];
            int k = 0;
            foreach (var i in lstCTPN)
            {
                mangNew[k] = i.SoLuongNhap;
                mangOld[k] = (int)TempData["soluongnhap" + k];
                TempData["soluongnhap" + k] = null;
                k++;
            }
            ctpnDao.UpdateRange(pnVM.MaPN, mangNew, mangOld);
            var phieuNhapModel = phieuNhapDao.GetByID(pnVM.MaPN);
            var phieuNhapViewModel = new PhieuNhapViewModel();
            phieuNhapViewModel.UpdatePhieuNhap(phieuNhapModel);

            var lstCTPNModel = ctpnDao.GetListByID(pnVM.MaPN);

            var thanhVien = tvDao.GetByID(phieuNhapModel.MaTV);
            ViewBag.Ten = thanhVien.HoTen;
            var lstCTPNViewModel = Mapper.Map<IEnumerable<ChiTietPhieuNhap>, IEnumerable<ChiTietPhieuNhapViewModel>>(lstCTPNModel);
            ViewBag.ListCTPN = lstCTPNViewModel;

            SetAlert("Cap nhat thanh cong", "success");
            return View(phieuNhapViewModel);
        }
    }
}