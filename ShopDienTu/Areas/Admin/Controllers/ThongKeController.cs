﻿using ShopDienTu.Infastructure.Core;
using ShopDienTu.Model.DAO;
using ShopDienTu.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopDienTu.Areas.Admin.Controllers
{
    [AuthorizeBase(Roles = "QuanTri")]
    public class ThongKeController : BaseController
    {
        private ThongKeDao thongKeDao;

        public ThongKeController()
        {
            thongKeDao = new ThongKeDao();
        }
        [Authorize(Roles = "QuanTri")]
        public ActionResult Index()
        {
            ViewBag.Visitor = HttpContext.Application["PageView"].ToString(); //Lấy số lượng người truy cần từ Application đã được tạo
            //ViewBag.Online = HttpContext.Application["Online"].ToString();
            ViewBag.TongThanhVien = thongKeDao.ThongKeUser();
            return View();
        }

        [HttpGet]
        public ActionResult TotalPrefit(string fromDate,string toDate, int flag = 0)
        {
            if(flag == 0)
            {
                return View();
            }
            else
            {
                var lstBenefit = thongKeDao.GetBenefit(fromDate,toDate);
                ViewBag.FromDate = fromDate;
                ViewBag.ToDate = toDate;
                return PartialView("TotalPrefitPartial", lstBenefit);
            }

        }

        [HttpGet]
        public ActionResult TotalExpenditure(string fromDate, string toDate, int flag = 0)
        {
            if (flag == 0)
            {
                return View();
            }
            else
            {
                DateTime from = DateTime.Parse(fromDate);
                DateTime to = DateTime.Parse(toDate);
                ViewBag.FromDate = fromDate;
                ViewBag.ToDate = toDate;
                var model = thongKeDao.GetListPhieuNhap(from, to);
                return PartialView("TotalExpenditurePartial",model);
            }
        }

        //public decimal ThongKeTongDoanhThuThang(int thang, int nam)
        //{
        //    //Thống kê theo tất cả doanh thu web theo tháng
        //    var lstDDH = db.DonDatHangs.Where(n => n.NgayDat.Value.Month == thang && n.NgayDat.Value.Year == nam);
        //    decimal TongTien = 0;
        //    foreach (var item in lstDDH)
        //    {
        //        TongTien += item.ChiTietDonDatHangs.Sum(n => n.DonGia * n.SoLuong).Value;
        //    }
        //    return TongTien;
        //}
    }
}