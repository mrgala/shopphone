﻿using ShopDienTu.Infastructure.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ShopDienTu.Model.DAO;
using ShopDienTu.Models;
using AutoMapper;
using ShopDienTu.Model.Model;
using System.Net;
using ShopDienTu.Infastructure.Extension;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
using System.Data.Entity.SqlServer;

namespace ShopDienTu.Areas.Admin.Controllers
{
    
    [AuthorizeBase(Roles = "QuanTri,QuanLyDonHang")]
    public class QuanLyDonHangController : BaseController
    {

        private QuanLyDonHangDao qldhDao;
        private DonDatHangDao ddhDao;

        public QuanLyDonHangController()
        {
            qldhDao = new QuanLyDonHangDao();
            ddhDao = new DonDatHangDao();
        }

        [Authorize(Roles = "QuanTri,QuanLyDonHang")]
        [HttpGet]
        public ActionResult Index()
        {
            var lstDonHangViewModel = new ListDonHangViewModel();

            var lstUnPaidModel = qldhDao.GetAllUnPaid();
            var lstUnDeliveryModel = qldhDao.GetAllDelivery();
            var lstAllSuccessModel = qldhDao.GetAllSuccess();

            lstDonHangViewModel.LstUnPaid = Mapper.Map<IEnumerable<DonDatHang>, IEnumerable<DonDatHangViewModel>>(lstUnPaidModel);
            lstDonHangViewModel.LstUnDelivery = Mapper.Map<IEnumerable<DonDatHang>, IEnumerable<DonDatHangViewModel>>(lstUnDeliveryModel);
            lstDonHangViewModel.LstAllSuccess = Mapper.Map<IEnumerable<DonDatHang>, IEnumerable<DonDatHangViewModel>>(lstAllSuccessModel);

            return View(lstDonHangViewModel);
        }

        [HttpGet]
        public ActionResult DuyetDonHang(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var ddh = ddhDao.GetByID(id);
            if (ddh == null)
            {
                return HttpNotFound();
            }

            var ddhViewModel = new DonDatHangViewModel();

            ddhViewModel.UpdateDonDatHang(ddh);

            //Lấy list các ctdh           
            var listctdh = ddh.ChiTietDonDatHangs.Where(n => n.MaDDH == id);
            var lstCtdhViewModel = Mapper.Map<IEnumerable<ChiTietDonDatHang>, IEnumerable<ChiTietDonDatHangViewModel>>(listctdh);

            ViewBag.ListCTDH = lstCtdhViewModel;
            return View(ddhViewModel);
        }

        [HttpPost]
        public ActionResult DuyetDonHang(DonDatHangViewModel ddhVM)
        {
            var ddhModel = ddhDao.GetByID(ddhVM.MaDDH);
            ddhModel.DaThanhToan = ddhVM.DaThanhToan;
            ddhModel.TinhTrangGiaoHang = ddhVM.TinhTrangGiaoHang;         
              
            ddhDao.Update(ddhModel);

            SetAlert("Cập nhật đơn hàng thành công", "success");
            return RedirectToAction("Index","QuanLyDonHang");
        }

        public static byte[] ImageToBinary(string imagePath)
        {
            FileStream fS = new FileStream(imagePath, FileMode.Open, FileAccess.Read);
            byte[] b = new byte[fS.Length];
            fS.Read(b, 0, (int)fS.Length);
            fS.Close();
            return b;
        }

        public ActionResult Crystal(int id)
        {
            ShopDienTuEntities db = new ShopDienTuEntities();
            var ctddh = db.ChiTietDonDatHangs.SqlQuery("Select * From ChiTietDonDatHang Where MaDDH = " + id).ToList();
            ReportDocument rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Report/crpHopDong.rpt")));
            rd.SetDataSource(ctddh.Select(p=>new
            {
                ProductName = p.TenSP,
                Quantity = p.SoLuong.Value,
                Photo = ImageToBinary(Path.Combine(Server.MapPath(p.SanPham.HinhAnh.ToString()))),
                Price = p.DonGia.Value
            }).ToList());
            //rd.SetParameterValue("totalPrice", 150000);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream
                (CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "SaleOrder.pdf");
        }
    }
}