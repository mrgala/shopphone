﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ShopDienTu.Models
{
    public class MemberViewModel
    {
        [Key]
        public int MaThanhVien { set; get; }

        [Display(Name = "Tài khoản")]
        [Required(ErrorMessage = "Bạn phải nhập tài khoản")]
        public string TaiKhoan { set; get; }

        [Required(ErrorMessage = "Bạn phải nhập mật khẩu")]
        [Display(Name = "Mật khẩu")]
        [StringLength(20, MinimumLength = 6, ErrorMessage = "Độ dài mật khẩu ít nhất 6 ký tự.")]
        public string MatKhau { set; get; }

        [Required(ErrorMessage = "Bạn phải nhập mật khẩu")]
        [Display(Name = "Xác Nhận Mật khẩu")]
        [Compare("MatKhau", ErrorMessage = "Mật khẩu không đúng")]
        public string XacNhanMatKhau { set; get; }

        [Display(Name = "Họ Tên")]
        [Required(ErrorMessage = "Bạn phải nhập họ tên")]
        public string HoTen { set; get; }

        [Display(Name = "Đia chỉ")]
        [Required(ErrorMessage = "Bạn phải nhập địa chỉ")]
        public string DiaChi { set; get; }

        [Display(Name = "Email")]
        [Required(ErrorMessage = "Bạn phải nhập email")]
        [RegularExpression(@"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z09!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z09-]*[a-z0-9])?)\Z", ErrorMessage = "Email không hợp lệ!")]
        public string Email { set; get; }

        [Display(Name = "Số điện thoại")]
        [Required(ErrorMessage = "Bạn phải nhập số điện thoại")]
        [StringLength(20, MinimumLength = 9, ErrorMessage = "Độ dài mật khẩu ít nhất 6 ký tự.")]
        public string SoDienThoai { set; get; }

        [Display(Name = "Câu hỏi")]
        public string CauHoi { set; get; }

        [Display(Name = "Câu trả lời")]
        public string CauTraLoi { set; get; }

        public int? MaLoaiTV { set; get; }
    }
}