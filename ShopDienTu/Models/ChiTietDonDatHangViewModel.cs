﻿using ShopDienTu.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopDienTu.Models
{
    public class ChiTietDonDatHangViewModel
    {
        public int MaChiTietDDH { set; get; }

        public int? MaDDH { set; get; }

        public int? MaSP { set; get; }

        public string TenSP { set; get; }

        public int? SoLuong { set; get; }

        public decimal? DonGia { set; get; }

        public virtual DonDatHang DonDatHang { get; set; }

        public virtual SanPham SanPham { get; set; }

    }
}