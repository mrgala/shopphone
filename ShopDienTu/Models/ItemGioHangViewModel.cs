﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopDienTu.Models
{
    public class ItemGioHangViewModel
    {
        public int MaSP { set; get; }

        public string TenSP { set; get; }

        public int SoLuong { set; get; }

        public decimal DonGia { set; get; }

        public decimal ThanhTien { set; get; }

        public string HinhAnh { set; get; }

    }
}