﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopDienTu.Models
{
    public class CartViewModel
    {
        public List<ItemGioHangViewModel> LstCart { set; get; }

        public KhachHangViewModel KhachHangViewModel { set; get; }
    }
}