﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ShopDienTu.Models
{
    public class LoaiSanPhamViewModel
    {
        [Key]
        public int MaLoaiSP { get; set; }

        [DisplayName("Loại sản phẩm")]
        [Required(ErrorMessage = "Hãy nhập vào tên {0}")]
        public string TenLoai { get; set; }

        [DisplayName("Icon")]
        public string Icon { get; set; }

        [DisplayName("Bí danh")]
        [Required(ErrorMessage = "Hãy nhập vào tên {0}")]
        public string BiDanh { get; set; }

    }
}