﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ShopDienTu.Models
{
    public class NhaCungCapViewModel
    {
        [Key]
        public int MaNCC { get; set; }

        [DisplayName("Tên nhà cung cấp")]
        [Required(ErrorMessage = "Hãy nhập vào tên {0}")]
        public string TenNCC { get; set; }

        [DisplayName("Địa chỉ")]
        [Required(ErrorMessage = "Hãy nhập vào tên {0}")]
        public string DiaChi { get; set; }

        [DisplayName("Email")]
        [Required(ErrorMessage = "Hãy nhập vào tên {0}")]
        [RegularExpression(@"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z09!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z09-]*[a-z0-9])?)\Z", ErrorMessage = "Email không hợp lệ!")]
        public string Email { get; set; }

        [DisplayName("Số điện thoại")]
        [Required(ErrorMessage = "Hãy nhập vào tên {0}")]
        //[Range(9, 11, ErrorMessage = "Số điện thoài phải từ {1} đến {2}")]
        public string SoDienThoai { get; set; }

        public string Fax { get; set; }
    }
}