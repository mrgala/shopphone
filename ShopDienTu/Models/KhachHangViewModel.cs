﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ShopDienTu.Models
{
    public class KhachHangViewModel
    {
        [Key]
        public int MaKH { set; get; }

        [Required(ErrorMessage ="Bạn phải nhập vào tên khách hàng")]
        public string TenKH { set; get; }

        [Required(ErrorMessage = "Bạn phải nhập vào địa chỉ")]
        public string DiaChi { set; get; }

        [Required(ErrorMessage = "Bạn phải nhập vào địa chỉ email")]
        [RegularExpression(@"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z09!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z09-]*[a-z0-9])?)\Z", ErrorMessage = "Email không hợp lệ!")]
        public string Email { set; get; }

        [Required(ErrorMessage = "Bạn phải nhập số điện thoại")]
        [StringLength(20, MinimumLength = 9, ErrorMessage = "Độ dài số điện thoại ít nhất 9 ký tự.")]
        public string SoDienThoai { set; get; }

        public int? MaThanhVien { set; get; }
    }
}