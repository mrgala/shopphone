﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ShopDienTu.Models
{
    public class PhieuNhapViewModel
    {
        [Key]
        public int MaPN { get; set; }

        public int? MaNCC { get; set; }

        public DateTime? NgayNhap { get; set; }

        public bool? DaXoa { get; set; }

        public DateTime? NgaySua { get; set; }

        public int? MaTV { get; set; }
    }
}