﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ShopDienTu.Models
{
    public class NhaSanXuatViewModel
    {
        [Key]
        public int MaNSX { get; set; }

        [DisplayName("Tên nhà sản xuất")]
        [Required(ErrorMessage = "Hãy nhập vào tên {0}")]
        public string TenNSX { get; set; }

        [DisplayName("Thông Tin")]
        public string ThongTin { get; set; }

        [DisplayName("Logo")]
        public string Logo { get; set; }
    }
}