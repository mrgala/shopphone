﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ShopDienTu.Models
{
    public class SlideViewModel
    {
        [Key]
        public int ID { get; set; }

        public string Ten { get; set; }

        public string MoTa { get; set; }

        public string HinhAnh { get; set; }

        public string Url { get; set; }

        public int? ThuTu { get; set; }

        public bool DaXoa { get; set; }

    }
}