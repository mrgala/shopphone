﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopDienTu.Models
{
    public class ListDonHangViewModel
    {
        public IEnumerable<DonDatHangViewModel> LstUnPaid { set; get; }
        public IEnumerable<DonDatHangViewModel> LstUnDelivery { set; get; }
        public IEnumerable<DonDatHangViewModel> LstAllSuccess { set; get; }
    }
}