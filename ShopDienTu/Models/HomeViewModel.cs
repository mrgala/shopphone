﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopDienTu.Models
{
    public class HomeViewModel
    {
        public IEnumerable<SanPhamViewModel> HotPhone { set; get; }
        public IEnumerable<SanPhamViewModel> HotLapTop { set; get; }
        public IEnumerable<SanPhamViewModel> HotTablet { set; get; }
        public IEnumerable<SlideViewModel> HotSlide { set; get; }

    }
}