﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopDienTu.Models
{
    public class LoaiThanhVienViewModel
    {
        public int MaLoaiThanhVien { set; get; }

        public string TenLoaiThanhVien { set; get; }

        public int? UuDai { set; get; }
    }
}