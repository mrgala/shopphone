﻿using ShopDienTu.Model.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ShopDienTu.Models
{
    public class ChiTietPhieuNhapViewModel
    {
        [Key]
        public int MaChiTietPN { get; set; }

        public int? MaPN { get; set; }

        public int? MaSP { get; set; }

        public decimal DonGiaNhap { get; set; }

        public int SoLuongNhap { get; set; }

        public ChiTietPhieuNhap ChiTietPhieuNhap { get; set; }

        public SanPham SanPham { get; set; }

    }
}