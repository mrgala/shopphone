﻿using ShopDienTu.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopDienTu.Models
{
    public class DonDatHangViewModel
    {
        public int MaDDH { set; get; }

        public DateTime NgayDat { set; get; }

        public bool? TinhTrangGiaoHang { set; get; }

        public DateTime? NgayGiao { set; get; }

        public bool? DaThanhToan { set; get; }

        public int? MaKH { set; get; }

        public int? UuDai { set; get; }

        public bool? DaHuy { set; get; }

        public bool? DaXoa { set; get; }

        public virtual IEnumerable<ChiTietDonDatHang> ChiTietDonDatHangs { set; get; }

        public virtual KhachHang KhachHang { set; get; }
    }
}