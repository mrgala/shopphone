﻿using ShopDienTu.Model.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ShopDienTu.Models
{
    public class SanPhamViewModel
    {
        [Key]
        [DisplayName("Mã sản phẩm")]
        public int MaSP { get; set; }

        [DisplayName("Tên sản phẩm")]
        public string TenSP { get; set; }

        [DisplayName("Đơn giá")]
        public decimal DonGia { get; set; }

        [DisplayName("Ngày cập nhập")]
        public DateTime? NgayCapNhap { get; set; }

        [DisplayName("Cấu hình")]
        public string CauHinh { get; set; }

        [DisplayName("Mô tả")]
        public string MoTa { get; set; }

        [DisplayName("Hình ảnh")]
        public string HinhAnh { get; set; }

        [DisplayName("Số lượng tồn")]
        public int? SoLuongTon { get; set; }

        [DisplayName("Lượt xem")]
        public int? LuotXem { get; set; }

        [DisplayName("Lượt bình chọn")]
        public int? LuotBinhChon { get; set; }

        [DisplayName("Lượt bình luận")]
        public int? LuotBinhLuan { get; set; }

        [DisplayName("Số lần mua")]
        public int? SoLanMua { get; set; }

        [DisplayName("Mới")]
        public bool Moi { get; set; }

        [DisplayName("Tên nhà cung cấp")]
        public int? MaNCC { get; set; }

        [DisplayName("Tên nhà sản xuất")]
        public int? MaNSX { get; set; }

        [DisplayName("Tên loại sản phẩm")]
        public int? MaLoaiSP { get; set; }

        [DisplayName("Ẩn")]
        public bool DaXoa { get; set; }

        [DisplayName("Hiển thị trang chủ")]
        public bool Home { get; set; }

        public string HinhAnh1 { get; set; }

        public virtual NhaSanXuat NhaSanXuat { get; set; }

        public virtual LoaiSanPham LoaiSanPham { get; set; }

    }
}